@ECHO OFF

:: --------------------
:CHECK_ARGS
:: --------------------
:: Console output only.
IF [%1] == [] GOTO START

IF [%~nx1] NEQ [] (
  :: %1 is a file
  SET HEADER_OUT_FILE=%~fs1
)

GOTO START

REM ===================
REM Entry Point
REM ===================
:START
CALL :WRITE_OUT
GOTO END

:: --------------------
:WRITE_OUT
:: --------------------
:: HEADER_OUT falls through to CON_OUT which checks for the QUIET flag.
IF DEFINED HEADER_OUT_FILE (
  CALL :OUT_HEADER
) ELSE (
  ECHO No header file specified!
  CALL :END
)
IF NOT DEFINED STRVERSION (
  ECHO STRVERSION not defined.  Call gitdescribe.bat!
  CALL :END
)

:: --------------------
:OUT_HEADER
:: --------------------
ECHO //gitversion.bat generated resource header.>"%HEADER_OUT_FILE%"
ECHO #define STRVERSION  "%STRVERSION%\0" >> "%HEADER_OUT_FILE%"
ECHO #define BYTEVERSION %BYTEVERSION% >> "%HEADER_OUT_FILE%"

:: --------------------
:END
:: --------------------
