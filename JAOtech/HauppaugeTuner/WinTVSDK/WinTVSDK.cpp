/*
** LTF
** WinTVSDK.cpp
*/

#include "stdafx.h"
#include "WinTVSDK.h"


#if 0
/*
** These Casts were needed at one time when we automatically included
** 'using namespace DataModel' and 'using namespace WinTVPublic'
** in WinTVSDK.h for everyone's global consumption.
** Now that they are gone, so is the need for these.
*/
struct WinTVPublic::IChannel * CastAsWinTVPublicIChannel(IChannelPtr p)
{
	return (WinTVPublic::IChannel *) (DataModel::IChannel *) p;
}

struct WinTVPublic::IBoard * CastAsWinTVPublicIBoard(IBoardPtr p)
{
	return (WinTVPublic::IBoard *) (DataModel::IBoard *) p;
}
#endif
