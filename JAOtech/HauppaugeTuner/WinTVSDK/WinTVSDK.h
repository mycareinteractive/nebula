/*
** LTF
** WinTVSDK.h
*/
#pragma once

#ifdef DEBUG
#import "..\WinTVSDKComponents\Debug\DataModel.tlb" raw_interfaces_only
#import "..\WinTVSDKComponents\Debug\WinTVPublic.tlb" raw_interfaces_only
#else
#import "..\WinTVSDKComponents\Release\DataModel.tlb" raw_interfaces_only
#import "..\WinTVSDKComponents\Release\WinTVPublic.tlb" raw_interfaces_only
#endif

/*
** LTF
** While specifying these here would seem to make things more convenient,
** I feel it lends a little confusion.  I believe this Sample App is actually
** more clear and explicit by NOT including these here.  If forces us to
** to think about where all the Types are defined by having to refere to the
** proper namespaces.
**
** Eliminating them from here also eliminates the need for some confusing
** casts (which, frankly, I don't quite understand why are needed).
**
using namespace DataModel;
using namespace WinTVPublic;
**
*/


/*
** LTF
** SAFEARRAY helper classes
*/
class CHcwArray : public SAFEARRAY
{
public:

	CHcwArray()
	: pSafeArray(NULL)
	{}
	~CHcwArray()
	{
		Destroy();
	}

	SAFEARRAY *pSafeArray;

	virtual int Validate(USHORT cDims, ULONG cbElements)
	{
		if( !pSafeArray )
			return 0;
		if( cDims != pSafeArray->cDims )
			return 0;
		if( cbElements != pSafeArray->cbElements )
			return 0;
		return 1;
	}
	
	virtual operator SAFEARRAY**() { return &pSafeArray; }

	virtual ULONG Count(USHORT cDim=0)
	{
		if( !pSafeArray )
			return 0;
		if( cDim < pSafeArray->cDims )
			return pSafeArray->rgsabound[cDim].cElements;
		return 0;
	}

	virtual HRESULT Lock()
	{
		if( !pSafeArray )
			return E_INVALIDARG;
		return SafeArrayLock(pSafeArray);
	}
	virtual HRESULT Unlock()
	{
		if( !pSafeArray )
			return E_INVALIDARG;
		return SafeArrayUnlock(pSafeArray);
	}

	virtual void Destroy()
	{
		if( pSafeArray ) {
			SafeArrayDestroy(pSafeArray);
			pSafeArray = NULL;
		}
	}
};


template <class T> class tCHcwArray : public CHcwArray {
public:

	tCHcwArray(USHORT cDims, ULONG cbElementSize)
	: CHcwArray()
	, cMyDimCount(cDims)
	, cbMyElementSize(cbElementSize)
	{
	}

	~tCHcwArray()
	{
	}

	T operator[](ULONG ul)
	{
		if( !pSafeArray )
			return NULL;

		if( ul > Count() )
			return NULL;

		return ((T*)pSafeArray->pvData)[ul];
	}

	virtual int Validate()
	{
		return CHcwArray::Validate( cMyDimCount, cbMyElementSize );
	}

protected:
	USHORT cMyDimCount;
	ULONG  cbMyElementSize;

};


class CHcwAutoLockArray {
public:
	CHcwAutoLockArray( CHcwArray &theArray )
	: myArray(theArray)
	, hr(-1)
	{
		hr = myArray.Lock();
	}

	~CHcwAutoLockArray()
	{
		if( hr == 0 ) {
			myArray.Unlock();
		}
	}

	operator HRESULT() { return hr; }

protected:
	CHcwArray &myArray;
	HRESULT hr;
};




/*
** LTF
** CHcwArray_IChannel array of IChannelPtr
*/
typedef tCHcwArray<DataModel::IChannelPtr> CHcwArray_IChannel;
/*
** This is a Cast which was needed at one time when we automatically included
** 'using namespace DataModel' and 'using namespace WinTVPublic'
** in this header file for everyone's global consumption.
** Now that they are gone, so is the need for this Cast.
**
struct WinTVPublic::IChannel * CastAsWinTVPublicIChannel(IChannelPtr p);
**
*/




/*
** LTF
** CHcwArray_IBoard array of IBoardPtr
*/
typedef tCHcwArray<DataModel::IBoardPtr> CHcwArray_IBoard;
/*
** This is a Cast which was needed at one time when we automatically included
** 'using namespace DataModel' and 'using namespace WinTVPublic'
** in this header file for everyone's global consumption.
** Now that they are gone, so is the need for this Cast.
**
struct WinTVPublic::IBoard * CastAsWinTVPublicIBoard(IBoardPtr p);
**
*/




/*
** LTF
** CHcwArray_BSTR array of BSTR
**
** NOTE that whoever instantiates this array is responsible for calling
** SysFreeString on any BSTRs as necessary.  This array class DOES NOT
** concern itself with that sort of thing.
*/
typedef tCHcwArray<BSTR> CHcwArray_BSTR;
