#include "StdAfx.h"
#include "HauppaugeTuner.h"

using namespace TVTuner;

HauppaugeTuner::HauppaugeTuner(void)
	: _pILiveTVServices(NULL)
, _pIServiceFactory(NULL)
, _pIPlaybackServices(NULL)
, _allChannels(1, sizeof(DataModel::IChannelPtr))
, _currentChannelIndex((ULONG)-1)
, _renderWindow(NULL)
{
	HRESULT hrCoInit = CoInitialize(NULL);
}


HauppaugeTuner::~HauppaugeTuner(void)
{
}

int HauppaugeTuner::Initialize(std::string device, std::string options)
{
	/*
	** Get the ServiceFactory.
	*/
	_pIServiceFactory = new WinTVPublic::IServiceFactoryPtr(__uuidof(WinTVPublic::ServiceFactory));
	if( !_pIServiceFactory ) 
	{
		//TODO: logging and return an error code
		return -1;
	}
	
	/*
	** Update the list of channels
	*/
	_allChannels.Destroy();
	HRESULT hr = _pIServiceFactory->GetAllChannels_C(_allChannels);
	if( hr != S_OK )
	{
		//TODO: logging and return an error code
		return -1;
	}

	if( !_allChannels.Validate() )
	{
		//TODO: logging and return an error code
		return -1;
	}

    if (_allChannels.Count() == 0)
    {
		//TODO: logging and return an error code
		return -1;
    }
	// create the channel UID map for fast access
	GenerateChannelMap();
	return 0;
}

void HauppaugeTuner::GenerateChannelMap()
{
	_channelUIDMap.clear();

	{
		CHcwAutoLockArray autoLock(_allChannels);
		HRESULT hr = autoLock;
		if( hr != S_OK )
			return;

		for( ULONG ul = 0; ul < _allChannels.Count(); ul++ )
		{
			DataModel::IChannelPtr pChannel = _allChannels[ul];

			if( pChannel != NULL ) {
				BSTR pB = NULL;
				HRESULT hr = pChannel->GetPreferredNumber_Alloc_C((long *)(&pB)); 
				if( (hr == S_OK) && (pB != NULL) ) {
					std::wstring ws(pB, ::SysStringLen(pB));
					_channelUIDMap[ws] = ul;
					SysFreeString(pB);
				}
				else {
					//logging and skip
				}
			}
			else {
				//logging and skip
			}
		}
	}
}

int HauppaugeTuner::Terminate()
{
	return 0;
}

int HauppaugeTuner::GetChannelCount()
{
	return _allChannels.Count();
}

/// Test function, dumping all channels in the format tha each tuner decided
std::wstring HauppaugeTuner::GetChannels()
{
	std::wstring channelStr = L"";
	if( _allChannels.Count() == 0 )
	{
		return channelStr;
	}

	{
		CHcwAutoLockArray autoLock(_allChannels);
		HRESULT hr = autoLock;
		if( hr != S_OK )
			return channelStr;

		for( ULONG ul = 0; ul < _allChannels.Count(); ul++ )
		{
			DataModel::IChannelPtr pChannel = _allChannels[ul];

			if( pChannel != NULL ) {
				BSTR pB = NULL;
				HRESULT hr = pChannel->GetPreferredNumberName_Alloc_C((long *)(&pB));
				if( (hr == S_OK) && (pB != NULL) ) {
					std::wstring ws(pB, ::SysStringLen(pB));
					if(ul>0)
 						channelStr.append(L"\n");
					channelStr.append(ws);
					SysFreeString(pB);
				}
				else {
					//logging and skip
					wchar_t buffer [100];
					swprintf(buffer, 100, L"Error Getting Channel Index %04d", ul);
					channelStr.append(buffer);
				}
			}
			else {
				wchar_t buffer [100];
				swprintf(buffer, 100, L"Error Getting IChannelPtr for iteration %04d", ul);
				channelStr.append(buffer);
			}
		}
	}
	return channelStr;
}

int HauppaugeTuner::SetChannels()
{
	return 0;
}

int HauppaugeTuner::SetWindow(int handle)
{
	_renderWindow = (HWND)handle;
	return 0;
}

int HauppaugeTuner::GetWindow()
{
	return (int)_renderWindow;
}

int HauppaugeTuner::Play(std::wstring uid, int& idx)
{
	idx = -1;
	std::map<std::wstring,int>::iterator it = _channelUIDMap.find(uid);
	if(it != _channelUIDMap.end()) {
		idx = it->second;
		return PlayAt(idx);
	}
	return -1;
}

int HauppaugeTuner::PlayAt(int idx)
{
	if( _pIServiceFactory && (_allChannels.Count() > 0) ) {

		_currentChannelIndex = (ULONG)idx;
		if( _currentChannelIndex >= _allChannels.Count() )
			_currentChannelIndex = _allChannels.Count() - 1;

		DataModel::IChannelPtr pSelectedChannel = _allChannels[_currentChannelIndex];
		
		if( _pILiveTVServices ) {
			long i = 0;
			_pILiveTVServices->SwitchToChannel_C( pSelectedChannel, &i );
			WinTVPublic::AspectRatioMode arm;
			_pILiveTVServices->GetAspectRatioMode_C(&arm);
			_pILiveTVServices->SetAspectRatioMode_C(arm);
		}

		else {

			HWND hWndVideo = _renderWindow;
			
			_pIServiceFactory->StartLiveTV_C((long)hWndVideo, pSelectedChannel, &_pILiveTVServices);

			if( _pILiveTVServices ) {
				_pILiveTVServices->SetVolume_C(100);
				WinTVPublic::AspectRatioMode arm;
				_pILiveTVServices->GetAspectRatioMode_C(&arm);
				_pILiveTVServices->SetAspectRatioMode_C(arm);
			}
			else {
				//Trace(L"OnBnClickedButtonwatch", L"pILiveTVServices == NULL, _NOT_ Calling SetTimer()");
				return -1; //error
			}
		}
	}
	return 0;
}

int HauppaugeTuner::Stop(bool shutdownHardware)
{
	return 0;
}