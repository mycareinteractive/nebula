#pragma once

#include <map>
#include "..\nebula\TVTuner.h"
#include ".\WinTVSDK\WinTVSDK.h"

namespace TVTuner
{
	class HauppaugeTuner : public ITuner
	{
	public:
		HauppaugeTuner(void);
		~HauppaugeTuner(void);

		int Initialize(std::string device, std::string options);
		int Terminate();

		int GetChannelCount();
		std::wstring GetChannels();
		int SetChannels();

		int SetWindow(int handle);
		int GetWindow();

		int PlayAt(int idx);
		int Play(std::wstring uid, int& idx);
		int Stop(bool shutdownHardware);

		static ITuner * __stdcall Create() { return new HauppaugeTuner(); }

	protected:
		void GenerateChannelMap();

	private:
		WinTVPublic::IServiceFactoryPtr _pIServiceFactory;
		WinTVPublic::ILiveTVServicesPtr _pILiveTVServices;
		WinTVPublic::IPlaybackServicesPtr _pIPlaybackServices;

		CHcwArray_IChannel _allChannels;
		std::map<std::wstring, int> _channelUIDMap;

		ULONG _currentChannelIndex;
		HWND _renderWindow;
	};
}

