#include <QApplication>
#include <QDesktopWidget>
#include <QLayout>
#include <QList>
#include "OSDWidget.h"

OSDWidget* OSDWidget::_instance = NULL;

OSDWidget* OSDWidget::GetInstance()
{
	return _instance;
}

OSDWidget::OSDWidget(QWidget *parent)
	: QWidget(parent)
	, _volumeBg(NULL)
	, _volumeBar(NULL)
	, _timer(NULL)
	, _textLabel(NULL)
{
	_instance = this;
	Init();
	hide();
}

OSDWidget::~OSDWidget()
{
	if(_timer) {
		_timer->stop();
		delete _timer;
	}
	if(_volumeBg)
		delete _volumeBg;
	if(_volumeBar)
		delete _volumeBar;
}

void OSDWidget::Init()
{
	// full screen transparent
	move(0,0);
	resize(QApplication::desktop()->size());

	// Volume bar
	_volumeBg = new QLabel(this);
	QImage imageVolBg(QString::fromUtf8(":/nebula/Resources/vol_bg.png"));
	_volumeBg->setBackgroundRole(QPalette::Base);
	_volumeBg->setSizePolicy(QSizePolicy::Ignored, QSizePolicy::Ignored);
    _volumeBg->setScaledContents(true);
	_volumeBg->setPixmap(QPixmap::fromImage(imageVolBg));
	_volumeBg->resize(imageVolBg.size());
	
	_volumeBar = new QLabel(this);
	QImage imageVolBar(QString::fromUtf8(":/nebula/Resources/vol_bar.png"));
	_volumeBar->setBackgroundRole(QPalette::Base);
	_volumeBar->setSizePolicy(QSizePolicy::Ignored, QSizePolicy::Ignored);
    _volumeBar->setScaledContents(true);
	_volumeBar->setPixmap(QPixmap::fromImage(imageVolBar));
	_volumeBar->resize(imageVolBar.size());
	_volumeBarWidth = imageVolBar.size().width();

	_volumeBg->hide();
	_volumeBar->hide();

	// Text label
	_textLabel = new QLabel(this);
	_textLabel->setText("");
	_textLabel->setStyleSheet("QLabel { background-color:#353535; color:#ffffff; font-family:Arial; font-size:30px; font-weight:bold; padding:10px }");	// default style
	_textLabel->hide();

	_timer = new QTimer(this);
	connect(_timer, SIGNAL(timeout()), this, SLOT(OSDTimeout()));
}

void OSDWidget::AdjustLayer(int x, int y, int w, int h)
{
	// When we render OSD widget over a directshow window such as VLC TV tuner,
	// the entire widget will block the TV tuner even some of the widget is transparent.
	// Hence we have to hack to shrink and move OSD widget to only overlay portion of the screen.
	resize(w, h);
	move(x, y);
	show();
}

void OSDWidget::Clear()
{
	foreach(QWidget *obj, findChildren<QWidget*>())
    {
        obj->hide();
    }
}

void OSDWidget::OSDTimeout()
{
	_timer->stop();
	Clear();
	hide();
}

void OSDWidget::ShowVolumeBar(int volume, int delay)
{
	OSDTimeout();
	
	QSize screen = QApplication::desktop()->size();
	int bgX = (screen.width() - _volumeBg->width()) / 2;
	int bgY = screen.height() * 0.8;
	
	int barW = _volumeBarWidth * volume / 100;
	int barH = _volumeBar->height();

	_volumeBg->show();
	_volumeBg->raise();
	_volumeBar->show();
	_volumeBar->raise();

	_volumeBar->resize(barW, barH);
	_volumeBg->move(0, 0);
	_volumeBar->move(76, 14);

	// adjust the entire OSD layer to right position
	AdjustLayer(bgX, bgY, _volumeBg->width(), _volumeBg->height());
	_timer->start(delay);
}

void OSDWidget::ShowText(QString text, QPoint point, int delay)
{
	OSDTimeout();

	_textLabel->setText(text);
	_textLabel->adjustSize();
	_textLabel->show();
	_textLabel->move(0, 0);

	AdjustLayer(point.x(), point.y(), _textLabel->width(), _textLabel->height());
	_timer->start(delay);
}