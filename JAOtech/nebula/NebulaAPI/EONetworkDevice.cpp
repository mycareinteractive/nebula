/******************************************************************************/
/** @file
 *
 * @brief	Implementation for the Nebula Network Device object
 *
 *		This file implements the Nebula network device object, which provides
 *		functionality to the application via JavaScript
 *
 * @author
 *		Michael K. Jones\n
 *		Stone Hill Consulting\n
 *		http://www.stonehill.com\n
 *
 * @par Environment
 *		Windows Embedded Standard 7
 *		Microsoft Visual Studio 2010
 *		Qt Framework 4.8.3
 *****************************************************************************/
#include "EONetworkDevice.h"


/******************************************************************************/
/**
 * Return a list of the available network interfaces.
 *
 * @return
 *		String list containing names of "real" adapters
 *
 ******************************************************************************/
QStringList GetNetworkDeviceList()
{
	QStringList devList;		// List to return (initially empty)

	// Iterate the list of all network interfaces
	foreach(QNetworkInterface intf, QNetworkInterface::allInterfaces())
    {
		// Fetch the hardware address.  For Ethernet interfaces, this will
		// be the MAC address of the form XX:XX:XX:XX:XX:XX
		QString hardware = intf.hardwareAddress();

		// Filter out interfaces which aren't real hardware
		QNetworkInterface::InterfaceFlags flags = intf.flags();
		if (!(flags & QNetworkInterface::IsLoopBack)	// NOT a loop-back connection
			&& (flags & QNetworkInterface::IsUp)			// only get connected adapter
			&& (flags & QNetworkInterface::IsRunning)		// only get adapter that has meaningful IP
			&& (hardware != "")								// Has a physical address
			&& (hardware != "00:00:00:00:00:00")			// Is not a virtual adapter
			&& (hardware != "00:00:00:00:00:00:00:E0"))		// Is not an IPv6 tunnel adapter
		{
			// We'll take it!  
			devList.append(intf.name());
		}
    }
    return devList;
}


/******************************************************************************/
/**
 * Constructor; initialize a new instance of the object
 *
 * @return
 *		None
 *
 ******************************************************************************/
EONebulaNetworkDevice::EONebulaNetworkDevice(QString name)
	: _dev(QNetworkInterface::interfaceFromName(name))
{
	return;
}


/******************************************************************************/
/**
 * Destructor; clean up the object as it is going away
 *
 * @return
 *		None
 *
 ******************************************************************************/
EONebulaNetworkDevice::~EONebulaNetworkDevice()
{
	// Nothing to do (yet, anyway)
	return;
}


/******************************************************************************/
/**
 * Return human readable name of the device
 *
 * @return
 *		String
 *
 ******************************************************************************/
QString EONebulaNetworkDevice::getHumanReadableName()
{
	return _dev.humanReadableName();
}


/******************************************************************************/
/**
 * Return low-level hardware address of the device (on Ethernet interfaces,
 * this will be the MAC address, formatted as a string "00:11:22:33:44:55").
 *
 * @return
 *		String
 *
 ******************************************************************************/
QString EONebulaNetworkDevice::getMAC()
{
	return _dev.hardwareAddress();
}


/******************************************************************************/
/**
 * Return the first IPV4 IP address of this interface.
 *
 * @return
 *		String
 *
 ******************************************************************************/
QString EONebulaNetworkDevice::getIP()
{
	QString ip = "";
	QList<QNetworkAddressEntry> iplist = _dev.addressEntries();
	for (int i = 0; i < iplist.size(); ++i) {
		QNetworkAddressEntry entry = iplist.at(i);
		QHostAddress address = entry.ip();
		if (address.protocol() == QAbstractSocket::IPv4Protocol 
			&& address != QHostAddress(QHostAddress::LocalHost)) {
				ip = address.toString();
				break;
		}
	}
	return ip;
}


/******************************************************************************/
/**
 * Return whether or not this interface contains valid information
 *
 * @return
 *		True if interface is valid, false if not.
 *
 ******************************************************************************/
bool EONebulaNetworkDevice::isValid()
{
	return _dev.isValid();
}


/******************************************************************************/
/**
 * Return whether or not the interface is active
 *
 * @return
 *		True if interface is active, false if not.
 *
 ******************************************************************************/
bool EONebulaNetworkDevice::isActive()
{
	QNetworkInterface::InterfaceFlags flags = _dev.flags();

	return (flags & QNetworkInterface::IsUp);
}


/******************************************************************************/
/**
 * Assigns a new network interface
 *
 * @return
 *		True if new interface is valid, false if not
 *
 ******************************************************************************/
bool EONebulaNetworkDevice::assignInterface(const QString name)
{
	_dev = QNetworkInterface::interfaceFromName(name);
	return _dev.isValid();
}
