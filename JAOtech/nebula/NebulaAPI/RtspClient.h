/******************************************************************************/
/** @file 
 *
 * @brief	Definitions and declarations for the RTSP client object
 *
 * @author
 *		Michael K. Jones\n
 *		Stone Hill Consulting\n
 *		http://www.stonehill.com\n
 *
 * @par Environment
 *		Windows Embedded Standard 7
 *		Microsoft Visual Studio 2010
 *		Qt Framework 4.8.3
 *****************************************************************************/
#ifndef RTSPCLIENT_H
#define RTSPCLIENT_H

#include <QObject>
#include <QUrl>
#include <QTcpSocket>
#include <QMap>
#include <QTimer>
#include "logger.h"


/******************************************************************************/
/**
 * Class which represents an RTSP client session
 *
 ******************************************************************************/
class RtspClient : public QObject
{
	Q_OBJECT
	LOG4QT_DECLARE_QCLASS_LOGGER

public:

	// Construction/destruction
	RtspClient(QObject *parent=NULL);
	virtual ~RtspClient();

	// -----------------
	// Invokable Methods
	// -----------------

	// Set the RTSP URL to access
	Q_INVOKABLE bool SetUrl(QString url);

	// Initiate a connection to the server.
	Q_INVOKABLE bool Connect();

	// Initiate a disconnect from the server.
	Q_INVOKABLE bool Disconnect();

	// Check if is connected to server
	Q_INVOKABLE bool IsConnected();

	// Initiate a SETUP to create a session for the specified URL.
	Q_INVOKABLE bool Setup();

	// Initiate a TEAROWN of the session
	Q_INVOKABLE bool Teardown();

	// Send a PLAY request
	Q_INVOKABLE bool Play(float speed=1.0, QString range="");

	// Send a PAUSE request
	Q_INVOKABLE bool Pause();

	// Get stream position (NPT)
	Q_INVOKABLE bool GetPosition();

	// Stop any ongoing session and close connection
	Q_INVOKABLE bool Stop();


	// --------------
	// Public signals
	// --------------
signals:

	// Indicates that we are now connected to the server
	void Connected();

	// Indicates that we are now disconnected from the server
	void Disconnected();

	// Indicates when a socket error occurs.  Supplied the error code (see the
	// enum QAbstractSocket::SocketError), and a string describing the error.
	void SocketError(int errCode, QString errString);

	// Indicates a response from the RTSP server
	void RtspResponse(QVariantMap response);

	// Indicate an announcement from the RTSP server
	void RtspAnnounce(QVariantMap announce);


	// -------------
	// Private Slots
	// -------------
private slots:

	// Called whenever a socket error occurs
	void onSocketError(QAbstractSocket::SocketError err);

	// Called when the socket is connected/disconnected to/from the host
	void onSocketConnected(void);
	void onSocketDisconnected(void);

	// Called when new data is available to read
	void onSocketReadyRead(void);

	// Called when the session keep-alive timer goes off
	void onSessionKeepAlive(void);


	// ------------------------
	// Private Member Functions
	// ------------------------
private:

	// We pass header values around as a map
	typedef QMap<QString, QString>	RtspHeaders;

	// Helper to convert a header map to a string
	QString ToString(const RtspHeaders &h);

	// Called to set a GET_PARAMETER request
	bool GetParameter(QString parm="");

	// Called to send an OPTIONS request
	bool SendOptions();

	// Called to send a DESCRIBE request for the current URL
	bool SendDescribe();

	// Called to send various requests to the server.
	bool SendRequest(const QString method, const RtspHeaders &headers, const QString body="");

	// Parse a received response buffer
	bool parseResponse(const QString &response, int &code, QString &phrase, RtspHeaders &hdrs);

	// Parse a received request buffer
	bool parseRequest(const QString &request, QString &method, QString &url, RtspHeaders &hdrs);

	// Process a response received from the server
	bool ProcessResponse (int code, QString phrase, RtspHeaders hdrs, QString body);

	// Process an announcement received from the server
	bool ProcessAnnounce(RtspHeaders hdrs);



	// ------------------------
	// Private Member Variables
	// ------------------------
private:

	// URL of the requested presentation
	QUrl _url;
	
	// Socket used to communicate with server
	QTcpSocket _socket;

	// Request sequence number
	int _cseq;

	// Data received so far
	QString _received;

	// Session ID
	QString _session;

	// Session timeout, in seconds
	uint _timeout;

	QTimer _sessionKeepAliveTimer;
};

#endif // RTSPCLIENT_H
