/******************************************************************************/
/** @file
 *
 * @brief	Implementation for the TimeZones class
 *
 *		This file implements the TimeZones object, which wraps getting and
 *		settings the Windows time zone.
 *
 * @author
 *		Michael K. Jones\n
 *		Stone Hill Consulting\n
 *		http://www.stonehill.com\n
 *
 * @par Environment
 *		Windows Embedded Standard 7
 *		Microsoft Visual Studio 2010
 *		Qt Framework 4.8.3
 *****************************************************************************/
#include "Events.h"
#include <QVariantList>


/******************************************************************************/
/**
 * Constructor; initialize a new instance of the object
 *
 * @return
 *		None
 *
 ******************************************************************************/
NebulaCommandEvent::NebulaCommandEvent(int keyCode, QString message, int modifiers)
    : QVariantMap() {
    // Save key code
    this->insert("Code", keyCode);

    // Save the message
    this->insert("Message", message);

    // Save the key modifiers (CTRL, Shift, etc)
    this->insert("Modifiers", modifiers);

#if 0
    // TEMP: add a data array, just to make sure it comes through
    QVariantList data;
    data.push_back(42);
    data.push_back(24);
    this->insert("Data", data);
#endif

    return;
}

NebulaCommandEvent::NebulaCommandEvent(const QVariantMap &map)
    : QVariantMap(map) {

}


/******************************************************************************/
/**
 * Destructor; clean up the object as it is going away
 *
 * @return
 *		None
 *
 ******************************************************************************/
NebulaCommandEvent::~NebulaCommandEvent() {
    // Nothing to do (yet)
}


/******************************************************************************/
/**
 * Constructor; initialize a new instance of the object
 *
 * @return
 *		None
 *
 ******************************************************************************/
NebulaNotificationEvent::NebulaNotificationEvent(QString category, QString action, QString message)
    : QVariantMap() {
    // Save key code
    this->insert("Category", category);

    // Save the message
    this->insert("Action", action);

    // Save the key modifiers (CTRL, Shift, etc)
    this->insert("Message", message);

    return;
}

NebulaNotificationEvent::NebulaNotificationEvent(const QVariantMap &map)
    : QVariantMap(map) {
}


/******************************************************************************/
/**
 * Destructor; clean up the object as it is going away
 *
 * @return
 *		None
 *
 ******************************************************************************/
NebulaNotificationEvent::~NebulaNotificationEvent() {
    // Nothing to do (yet)
}


/******************************************************************************/
/**
 * Constructor; initialize a new instance of the object
 *
 * @return
 *		None
 *
 ******************************************************************************/
RtspResponseEvent::RtspResponseEvent(QString method, int code, QString message)
    : QVariantMap() {
    this->insert("Method", method);
    this->insert("Code", code);
    this->insert("Message", message);

    return;
}

RtspResponseEvent::RtspResponseEvent(const QVariantMap &map)
    : QVariantMap(map) {

}


/******************************************************************************/
/**
 * Destructor; clean up the object as it is going away
 *
 * @return
 *		None
 *
 ******************************************************************************/
RtspResponseEvent::~RtspResponseEvent() {
    // Nothing to do (yet)
}
