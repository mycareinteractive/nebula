/******************************************************************************/
/** @file
 *
 * @brief	Implementation for the TimeZones class
 *
 *		This file implements the TimeZones object, which wraps getting and
 *		settings the Windows time zone.
 *
 * @author
 *		Michael K. Jones\n
 *		Stone Hill Consulting\n
 *		http://www.stonehill.com\n
 *
 * @par Environment
 *		Windows Embedded Standard 7
 *		Microsoft Visual Studio 2010
 *		Qt Framework 4.8.3
 *****************************************************************************/
#include "TimeZones.h"
#include "util.h"
#include <qt_windows.h>
#include <QSettings>
#include <QStringList>
#include <memory>

// Disable the stupid warning "C4351: new behavior: elements of array will be default
// initialized", which is the standard behavior (and what we want)
#pragma warning(disable:4351)

/// Registry path (under HKEY_LOCAL_MACHINE) for the time zone information entries.
#define TIME_ZONE_KEY	"SOFTWARE\\Microsoft\\Windows NT\\CurrentVersion\\Time Zones"

/// Use a unique pointer to implement a Windows HKEY value, so that it automatically
/// gets closed when it goes out of scope.
struct hkey_deleter
{
	void operator()(HKEY hkey) { ::RegCloseKey(hkey); }
};
typedef std::unique_ptr<HKEY__, hkey_deleter> ScopedHKEY;

/// Opens the specified registry key and returns a scoped HKEY for it
ScopedHKEY MyRegOpenKeyEx(HKEY hKey, LPCWSTR lpSubKey, DWORD ulOptions, REGSAM samDesired)
{
	HKEY hOpenedKey = NULL;
	::RegOpenKeyEx(hKey, lpSubKey, ulOptions, samDesired, &hOpenedKey);

	return ScopedHKEY(hOpenedKey);
}


// Local helper class which wraps the Windows dynamic time zone info structure
// (with helpers for reading the information from a time zone entry in the
// registry).
class TimeZoneEntry : public DYNAMIC_TIME_ZONE_INFORMATION
{
public:

	// Default constructor (empty/invalid time zone)
	TimeZoneEntry()
		: DYNAMIC_TIME_ZONE_INFORMATION(), _displayName(), _valid(false)
	{
	}

	// Constructor initializes an entry by reading it from the registry
	TimeZoneEntry(QString name)
		: DYNAMIC_TIME_ZONE_INFORMATION(), _displayName(), _valid(false)
	{
		// Generate the full path to the registry key for the specified time
		// zone
		QString keyName = TIME_ZONE_KEY;
		keyName += "\\";
		keyName += name;

		// Open the registry key for the specified time zone
		ScopedHKEY key = MyRegOpenKeyEx (HKEY_LOCAL_MACHINE, keyName.utf16(), 0, KEY_READ);
		if (!key)
			return;

		// Save the key name
		if (name.length() >= _countof(TimeZoneKeyName))
			return;
		name.toWCharArray(TimeZoneKeyName);

		// Read the registry values into their respective buffers.
		if (!ReadValue (key.get(), L"Display", _displayName, sizeof(_displayName)))
			return;
		if (!ReadValue (key.get(), L"Std", StandardName, sizeof(StandardName)))
			return;
		if (!ReadValue (key.get(), L"Dlt", DaylightName, sizeof(DaylightName)))
			return;

		// Structure to hold binary data from registry key (initialized to 0)
		TZI tzi;
		if (!ReadValue (key.get(), L"TZI", &tzi, sizeof(tzi)))
			return;
		Bias			= tzi.Bias;
		StandardBias	= tzi.StandardBias;
		DaylightBias	= tzi.DaylightBias;
		StandardDate	= tzi.StandardDate;
		DaylightDate	= tzi.DaylightDate;

		// Success!
		_valid = true;
		return;
	}

	// Clean up before going away
	~TimeZoneEntry()
	{
	}

	// Find the specified time zone name (i.e. the display string),
	// returning the full TimeZoneEntry for it
	static TimeZoneEntry Find(QString name)
	{
		// For convenience, we'll use a native settings object to enumerate
		// the registry keys
		QString tzKey = "HKEY_LOCAL_MACHINE\\";
		tzKey += TIME_ZONE_KEY;
		QSettings reg(tzKey, QSettings::NativeFormat);

		// Enumerate all of the subkeys (which are the time zone keys)
		QStringList keyList = reg.childGroups();

		// Build a map to translate the time zone name (i.e. the display name)
		// to the time zone key name
		QMap<QString, QString> keyMap;
		foreach (QString key, keyList)
		{
			reg.beginGroup(key);
			if (reg.contains("Display"))
			{
				QString displayName = reg.value("Display").toString();
				keyMap[displayName] = key;
			}
			reg.endGroup();
		}

		// If the requested time zone name exists, return the time zone entry
		// for it
		if (keyMap.contains(name))
			return TimeZoneEntry(keyMap[name]);

		// Otherwise, return an empty time zone entry
		return TimeZoneEntry();
	}

	bool isValid() const { return _valid; }
	LPCWSTR DisplayName() const { return _displayName; }

private:
	// Buffer to hold the display name from the registry
	WCHAR _displayName[128];

	// Flag to tell if the fields on the time zone info are all valid.
	bool _valid;

	// Each time zone has a binary value which contains the following:
	typedef struct
	{
		LONG Bias;
		LONG StandardBias;
		LONG DaylightBias;
		SYSTEMTIME StandardDate;
		SYSTEMTIME DaylightDate;

	} TZI;

	// Read the specified value from the registry
	bool ReadValue(const HKEY key, LPCWSTR name, void *buf, DWORD size)
	{
		bool result = false;
		DWORD type = 0;
		return (RegQueryValueEx(key, name, NULL, &type, (LPBYTE)buf, &size) == ERROR_SUCCESS);
	}

};


/******************************************************************************/
/**
 * Constructor; initialize a new instance of the object
 *
 * @return
 *		None
 *
 ******************************************************************************/
TimeZones::TimeZones()
{
}


/******************************************************************************/
/**
 * Destructor; clean up the object as it is going away
 *
 * @return
 *		None
 *
 ******************************************************************************/
TimeZones::~TimeZones()
{
}


/******************************************************************************/
/**
 * Return a string containing the current time zone.  This will be the same
 * string shown in the control panel applet for changing the time zone.
 *
 * @return
 *		String containing current time zone (empty if an error occurs)
 *
 ******************************************************************************/
QString TimeZones::GetCurrent()
{
	QString result = "";
	DYNAMIC_TIME_ZONE_INFORMATION tz;

	// Fetch the current time zone info.
	if (GetDynamicTimeZoneInformation(&tz) == TIME_ZONE_ID_INVALID)
		logger()->error("GetDynamicTimeZoneInformation failed; error=%1", GetLastError());
	else
	{
		// Use the key name to access the display name for the time zone
		// from the registry (this is what the user actually sees in the
		// GUI)
		TimeZoneEntry tz (QString::fromWCharArray(tz.TimeZoneKeyName));
		result = QString::fromWCharArray (tz.DisplayName());
	}
	return result;
}


/******************************************************************************/
/**
 * Change the current time zone.  Argument is a string containing one of the
 * values shown in the control panel applet for changing the time zone.
 *
 * @return
 *		True if time zone changed; false if an error occurs.
 *
 ******************************************************************************/
bool TimeZones::SetCurrent (QString name)
{
	bool result = false;

	// Find the time zone entry for the specified time zone in the registry
	TimeZoneEntry tz = TimeZoneEntry::Find(name);
	if (tz.isValid())
	{
		// User is permitted to change the time zone, but by default the privilege
		// is not enabled.  Enable it now
		if (Permit(SE_TIME_ZONE_NAME, true))
		{
			// Set the new time zone
			if (!SetDynamicTimeZoneInformation(&tz))
				logger()->error("SetDynamicTimeZoneInformation failed, error=%1", GetLastError());
			else
				result = true;
		}

		// When done, disable the "change time zone" privilege
		Permit(SE_TIME_ZONE_NAME, false);
	}
	return result;
}
