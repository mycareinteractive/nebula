/******************************************************************************/
/** @file
 *
 * @brief	Declaration for the Nebula events
 *
 *		In order to fire an event to JavaScript and pass structured data, we
 *		send QVariantMaps.  These get converted to Javascript objects.  We
 *		define subclasses for the various types of event arguments to make
 *		them easier to construct from native code.
 *
 * @author
 *		Michael K. Jones\n
 *		Stone Hill Consulting\n
 *		http://www.stonehill.com\n
 *
 * @par Environment
 *		Windows Embedded Standard 7
 *		Microsoft Visual Studio 2010
 *		Qt Framework 4.8.3
 *****************************************************************************/

#include <QVariantMap>
#include <QString>


/******************************************************************************/
/**
 * Class for COMMAND events (mainly key presses of interest to the app)
 *
 ******************************************************************************/
class NebulaCommandEvent : public QVariantMap {
  public:
    // Construction/destruction
    NebulaCommandEvent(int keyCode, QString message, int modifiers);
    NebulaCommandEvent(const QVariantMap &map);
    ~NebulaCommandEvent();
};


/******************************************************************************/
/**
 * Class for NOTIFICATION events (events of interest to the app)
 *
 ******************************************************************************/
class NebulaNotificationEvent : public QVariantMap {
  public:
    // Construction/destruction
    NebulaNotificationEvent(QString category, QString action, QString message);
    NebulaNotificationEvent(const QVariantMap &map);
    ~NebulaNotificationEvent();
};


/******************************************************************************/
/**
 * Class for RtspResponse events
 *
 ******************************************************************************/
class RtspResponseEvent : public QVariantMap {
  public:
    // Construction/destruction
    RtspResponseEvent(QString method="", int code=0, QString message="");
    RtspResponseEvent(const QVariantMap &map);
    ~RtspResponseEvent();
};
