#ifndef COMMON_H
#define COMMON_H

#define IMPL_PROPERTY(varType, varName)\
protected: varType m_##varName;\
public: inline varType get##varName(void) const { return m_##varName; }\
public: inline void set##varName(varType var){ m_##varName = var; }



#endif // COMMON_H