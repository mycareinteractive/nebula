/******************************************************************************/
/** @file
 *
 * @brief	Implementation for the Nebula API object
 *
 *		This file implements the Nebula API object, which provides functionality
 *		to the application via JavaScript
 *
 * @author
 *		Michael K. Jones\n
 *		Stone Hill Consulting\n
 *		http://www.stonehill.com\n
 *
 * @par Environment
 *		Windows Embedded Standard 7
 *		Microsoft Visual Studio 2010
 *		Qt Framework 4.8.3
 *****************************************************************************/
#include <qt_windows.h>
#include <QDateTime>
#include <QWebInspector>
#include <QNetworkInterface>
#include <QSettings>
#include <QCoreApplication>
#include <QMutexLocker>
#include <QFile>
#include <mmdeviceapi.h>
#include <endpointvolume.h>
#include "eonebula.h"
#include "EONetworkDevice.h"
#include "TimeZones.h"
#include "../commonlib/commonlib.h"
#include "util.h"
#include "mainwindow.h"
#include "../version.info"

/******************************************************************************/
/**
 * Constructor; initialize a new instance of the object
 *
 * @return
 *		None
 *
 ******************************************************************************/
EONebula::EONebula(MainWindow &mainWindow)	///< Reference to main web view window
    : _mainWindow		(mainWindow)// Save a reference to the main window
    , _webInspector		(NULL)		// Defer creating the web debugger until it's asked for
    , _watchdog			()			// Watchdog timer for handshake
    , _logLevel			()			// Logging levels
    , _proIdiomEnabled	(false), _proIdiomPrimaryChan(0), _proIdiomSecondaryChan(0)
    , _scheduledCommandMessages(), _scheduledNotificationMessages()
    , _userBrowser		(NULL) 	// Parent the browser process to this object
    , _userServer(NULL)
    , _userChannel(NULL) {
    // Set revision numbers (read-only properties)
    m_NativeRevision = STRVERSION;
    m_ApiRevision = STRVERSION;

    // Seed the random number generator (used by getNetworkDevice method)
    qsrand((uint)QDateTime::currentMSecsSinceEpoch());

    // Initialize the watchdog timer interval to 90 seconds
    _watchdog.setSingleShot (false);
    _watchdog.setInterval(90 * 1000);
    connect (&_watchdog, SIGNAL(timeout()), this, SLOT(handshakeTimeout()));

    // Get current system volume
    _volume = GetVolume();

    // Start auto update timer
    _autoUpdateServerPath = "ftp://kpnwsupport:KPNW%40support1@ftp.aceso.com/Nebula/updates/production/";
    _autoUpdateTimer.setSingleShot(false);
    _autoUpdateTimer.setInterval(8 * 3600 * 1000); // every 8 hours
    connect(&_autoUpdateTimer, SIGNAL(timeout()), this, SLOT(autoUpdateTimeout()));
    _autoUpdateTimer.start();
}


/******************************************************************************/
/**
 * Destructor; clean up the object as it is going away
 *
 * @return
 *		None
 *
 ******************************************************************************/
EONebula::~EONebula() {
    if (_webInspector) {
        // Save the window geometry
        QSettings settings;
        settings.setValue("DebugGeometry", _webInspector->saveGeometry());

        // Destroy the web debugger
        delete _webInspector;
    }

    if(_userBrowser) {
        _userBrowser->kill();
        delete _userBrowser;
    }
    if(_userChannel)
        delete _userChannel;
    if(_userServer)
        delete _userServer;
}

/******************************************************************************/
/**
 * Return a list of the available network interfaces.
 *
 * @return
 *		List of strings (QStringList in Qt; converted to array of strings in
 *		Javascript)
 *
 ******************************************************************************/
QStringList EONebula::GetNetworkDeviceList() {
    // Call the helper function to get the NIC list; Qt will auotmatically
    // convert the QStringList to an array of string
    return ::GetNetworkDeviceList();
}


/******************************************************************************/
/**
 * Create an object for the specified network device
 *
 * @return
 *		EONebulaNetworkDevice object
 *
 ******************************************************************************/
QObject * EONebula::GetNetworkDevice(QString name) {
    // Create the network device
    QObject *object = new EONebulaNetworkDevice(name);

#if 0	/// @TODO	Not doing this for now; simply returing the object seems to work.
    ///			Question: when does the returned object get destroyed?

    // Generate a unique name for it
    static int num;
    num = qrand();
    QString objName = "EONebulaNetworkDevice" + QString::number(num);

    // Make the object available to Javascript (Javascript will henceforth
    // own the object, and control its lifetime)
    emit addJavaScriptObject(objName, object);
#endif

    // Return the object name (so the script knows how to access it)
    return object;
}


/******************************************************************************/
/**
 * Log the specified debug message.
 *
 * @return
 *		None
 *
 ******************************************************************************/
void EONebula::LogDebugMessage(
    int type,		///< Type of message
    QString msg) {	///< String to log
    logger()->debug("%1 %2", type, msg);
    return;
}


/******************************************************************************/
/**
 * Reload the initial starting page
 *
 * @return
 *		None
 *
 ******************************************************************************/
bool EONebula::Reload(bool bWait) {	///< If false, reload immediately; otherwise, wait for page load delay time
    // For now, we restart instead of reload to clear all hanging issues
    return Restart(false);

    // Tell the page to reload (but not to use any cached information)
    //_mainWindow.page()->triggerAction(QWebPage::ReloadAndBypassCache);
    //_mainWindow.loadMainApplication(bWait);
    //_watchdog.start();

    //return true;
}

/******************************************************************************/
/**
 * Return current state of web debugger window.
 *
 * @return
 *		True if the window is enabled, false if not.
 *
 ******************************************************************************/
bool EONebula::Debug() const {
    bool result = _webInspector && _webInspector->isVisible();
    return result;
}


/******************************************************************************/
/**
 * Enable or disable the web debugger window.
 *
 * @return
 *		None.
 *
 ******************************************************************************/
void EONebula::setDebug(bool bEnable) {
    // If enabling the web debugger...
    if (bEnable) {
        // Create the web debugger if needed
        if (!_webInspector) {
            _webInspector = new QWebInspector;

            // Restore the saved window geometry
            QSettings settings;
            _webInspector->restoreGeometry(settings.value("DebugGeometry").toByteArray());

#if !QT_DEBUG
            // For Release builds only, set the window as "always on top", or
            // it will forever remain behind the thing you're trying to debug!
            _webInspector->setWindowFlags(Qt::WindowStaysOnTopHint);
#endif

            // Must enable the developer extras in order for the web inspector to
            // show anything
            _mainWindow.page()->settings()->setAttribute(QWebSettings::DeveloperExtrasEnabled, true);
        }

        // If necessary, set our page as the page to debug
        if (!_webInspector->page())
            _webInspector->setPage(_mainWindow.page());

        // Show the debugger window
        _webInspector->setVisible(true);
    }

    // Otherwise, disable the web debugger (just hide it if it exists)
    else if (_webInspector)
        _webInspector->setVisible(false);

    return;
}


/******************************************************************************/
/**
 * Start the specified EXE name as a separate process
 *
 * @return
 *		None
 *
 ******************************************************************************/
void EONebula::StartProcess(QString exec) {	///< Name of app to exec
    QProcess *process = new QProcess(this);
    process->start(exec, QStringList() << "");
    return;
}


/******************************************************************************/
/**
 * The JavaScript application must handshake with us periodically so we know
 * it's still running.  These functions get and set the handshake period (in
 * seconds), and actually perform the handshake
 *
 ******************************************************************************/
int EONebula::GetHandshakeTimeout() const {
    // Return timer period in seconds (watchdog is in milliseconds)
    return (_watchdog.interval() / 1000);
}
void EONebula::SetHandshakeTimeout(int seconds) {
    logger()->trace("Changing handshake timeout from %1 to %2 seconds",
                    _watchdog.interval()/1000, seconds);
    _watchdog.setInterval(seconds * 1000);

    // If new interval is non-zero, start the timer; otherwise, stop it.
    if (seconds > 0)
        _watchdog.start();
    else
        _watchdog.stop();
}
bool EONebula::Handshake(QString name) {
    logger()->trace("Handshake: %1", name);
    _watchdog.start();
    return true;
}
void EONebula::handshakeTimeout() {
    logger()->warn("Handshake Timeout!");

    _watchdog.stop();
    return;
    // Reload/Restart the main page immediately
    QSettings settings;
    QString action = settings.value("HandshakeTimeoutAction", "Restart").toString();
    if(action.compare("Reload", Qt::CaseInsensitive) == 0) {
        logger()->info("Watchdog kicks in, reloading original URL...");
        Reload(false);
    } else if(action.compare("Ignore", Qt::CaseInsensitive) == 0) {
        logger()->info("Watchdog kicks in, ignore it according to settings...");
        // do nothing
    } else {
        logger()->info("Watchdog kicks in, restarting Nebula...");
        Restart(false);
    }

    return;
}


/******************************************************************************/
/**
 * Functions to allow the JavaScript application to get and set the current
 * time zone.  Note: we don't actually let them change the time zone; we
 * just return the current system setting.
 *
 ******************************************************************************/
QString EONebula::GetTimeZoneName() {
    TimeZones tz;
    QString result = tz.GetCurrent();
    return result;
}
bool EONebula::SetTimeZoneName(QString name) {
    bool result = false;
    TimeZones tz;
    result = tz.SetCurrent(name);
    return result;
}


/******************************************************************************/
/**
 * Get/Set the logging level for the specified component.
 *
 * Note: while this function is implemented, the logging level itself is not
 * currently used anywhere.
 *
 ******************************************************************************/
bool EONebula::SetLogLevel(QString category, int level) {
    // Save logging level for the specified component.
    _logLevel[category] = level;
    return _logLevel.contains(category);
}
int EONebula::GetLogLevel(QString category) {
    // Return logging level for the specified component.  If the level has never
    // been set, return -1.
    return _logLevel.value(category, -1);
}


/******************************************************************************/
/**
 * Set the ProIdiom authorization parameters
 *
 * Note: This is just stubbed for now; it is not actually used anywhere.
 *
 ******************************************************************************/
bool EONebula::SetAuthMode(bool bEnabled, int primaryChan, int secondaryChan) {
    _proIdiomEnabled = bEnabled;
    _proIdiomPrimaryChan = primaryChan;
    _proIdiomSecondaryChan = secondaryChan;
    return true;
}
bool EONebula::GetAuthModeEnabled() {
    return _proIdiomEnabled;
}
int EONebula::GetAuthModePrimaryChannel() {
    return _proIdiomPrimaryChan;
}
int EONebula::GetAuthModeSecondaryChannel() {
    return _proIdiomSecondaryChan;
}


/******************************************************************************/
/**
 * For firing events (primary intended for testing, but it might be useful
 * for the script)
 ******************************************************************************/
void EONebula::FireCommandEvent(int keyCode, QString msg, int modifiers) {
    emit CommandEvent(NebulaCommandEvent(keyCode, msg, modifiers));
}
void EONebula::FireNotificationEvent(QString type, QString action, QString msg) {
    emit NotificationEvent(NebulaNotificationEvent(type, action, msg));
}
void EONebula::ScheduleCommandEvent(int timeMS, QString msg) {
    _scheduledCommandMessages.append(msg);
    QTimer::singleShot(timeMS, this, SLOT(fireScheduledCommandEvent()));
}
void EONebula::ScheduleNotificationEvent(int timeMS, QString msg) {
    _scheduledNotificationMessages.append(msg);
    QTimer::singleShot(timeMS, this, SLOT(fireScheduledNotificationEvent()));
}
void EONebula::fireScheduledCommandEvent() {
    QString msg;
    if (!_scheduledCommandMessages.isEmpty())
        msg = _scheduledCommandMessages.takeFirst();
    NebulaNotificationEvent event(Json::decode(msg));
    emit CommandEvent(event);
}
void EONebula::fireScheduledNotificationEvent() {
    QString msg;
    if (!_scheduledNotificationMessages.isEmpty())
        msg = _scheduledNotificationMessages.takeFirst();
    NebulaCommandEvent event(Json::decode(msg));
    emit NotificationEvent(event);
}


/******************************************************************************/
/**
 * Restart the terminal.
 *
 * @return
 *		True if shutdown initiated; false if unable to shutdown.
 *
 ******************************************************************************/
bool EONebula::Restart(bool coldboot) {
    bool result = false;

    /// @TODO Implement bKioskMode parameter when we know what Kiosk mode entails.
    if(!coldboot) {
        // Restart Nebula instead of OS
        _mainWindow.getRtspClient()->Stop();
        _mainWindow.getTVPlayer()->Stop(false);
        qApp->exit(1);
        QStringList args;
        args.append("-nodelay");
        QProcess::startDetached(qApp->arguments()[0], args);
        result = true;
    } else if (Permit(SE_SHUTDOWN_NAME, true)) {
        // Must have the required permission to restart
        // Restart Windows; event log will say, "Application: Maintenance (Planned)"
        if (!ExitWindowsEx(EWX_REBOOT | EWX_FORCEIFHUNG, SHTDN_REASON_MAJOR_APPLICATION |
                           SHTDN_REASON_MINOR_MAINTENANCE | SHTDN_REASON_FLAG_PLANNED)) {
            logger()->error("ExitWindowsEx failed; error=%1", GetLastError());
        }

        // We've successfully INITIATED a restart (but it proceeds asynchronously, so
        // this doesn't necessarily means it is actually going to happen).
        else {
            logger()->info("Restart requested by application");
            result = true;
        }
    }

    return result;
}


/******************************************************************************/
/**
 * Open the user browser window.  The initial URL can be specified, as well as
 * the size and position of the window.  The window is not moveable or resizable
 * by the user (so it can be positioned relative to the main application UI
 * underneath).
 *
 * If the window is already open, nothing happens (i.e. the specified URL is
 * not loaded, nor is the window moved or sized).
 *
 * @return
 *		True if window was opened successfully; false if not.
 *
 ******************************************************************************/
bool EONebula::OpenUserBrowser(
    QString url,			///< Initial URL to load (can be empty)
    int x, int y,			///< Position of window (0,0 means default)
    int width, int height) {	///< Size of window (0,0 means default);
    bool result = false;

    // We will lock the following operation for multi-thread environment
    QMutexLocker locker(&_userMutex);

    try {

        // If browser is already running simply bring it to top
        if(_userBrowser && _userBrowser->state()!=QProcess::NotRunning) {
#if defined(Q_OS_WIN)	// Windows-specific code
            HWND hWnd = ::FindWindow(0, L"UserBrowser");
            ::ShowWindowAsync(hWnd,SW_SHOW);
            ::SetForegroundWindow(hWnd);
#endif
            return true;
        }

        // Check if browser executable is there before launching it
        QString browserpath = QCoreApplication::applicationDirPath() + "/userbrowser.exe";
        if(!QFile::exists(browserpath)) {
            logger()->error("Could not locate UserBrowser.exe. Failed to launch browser.");
            return false;
        }

        // Start event channel
        _userServer = new QLocalServer(this);
        connect(_userServer, SIGNAL(newConnection()), this, SLOT(userBrowserConnected()));

        _channelName = QString(NBIPC_NAME) + "." + QString::number(qApp->applicationPid()) + ".1";
        _userServer->removeServer(_channelName);
        bool listening = _userServer->listen(_channelName);
        if(!listening) {
            logger()->error("Unable to create event channel to user browser, error=%1", _userServer->errorString());
            delete _userServer;
            _userServer = NULL;
        } else {
            logger()->info("Event channel %1 created for user browser", _userServer->fullServerName());
        }

        // Build the argument list for the user browser
        QStringList argList;

        argList.append("-channel=" + _channelName);
        if (x || y)
            argList.append("-pos=" + QString::number(x) + "," + QString::number(y));
        if (width || height)
            argList.append("-size=" + QString::number(width) + "," + QString::number(height));
        if (!url.isEmpty())
            argList.append(url);

        // Start the user browser as a separated process
        _userBrowser = new QProcess(this);

        connect(_userBrowser, SIGNAL(finished(int, QProcess::ExitStatus)), this, SLOT(userBrowserFinished()));

        logger()->info("Starting user browser...");

        _userBrowser->start("UserBrowser.exe", argList);

        // Wait for the browser to start.
        result = _userBrowser->waitForStarted(5000);

        if(!result) {
            logger()->error("Unable to start user browser process");
            locker.unlock();
            CloseUserBrowser();
            return result;
        }

        logger()->info("User browser started, pid=%1", (qint64)_userBrowser->pid());

    } catch(...) {
        logger()->fatal("Fatal exception when trying to launch user browser!");
    }
    return result;
}


/******************************************************************************/
/**
 * Close the user browser window, if it is open.  It is not considered an error
 * to close the window if it's not currently open.
 *
 * @return
 *		True if window was closed successfully; false if not.
 *
 ******************************************************************************/
bool EONebula::CloseUserBrowser() {
    bool result = false;

    // We will lock the following operation for multi-thread environment
    QMutexLocker locker(&_userMutex);

    try {
        if(_userChannel && _userChannel->isOpen()) {
            logger()->info("Closing user browser...");

            // Send exit message to browser first
            QMap<QString, QVariant> message;
            message[NBIPC_TYPE] = NBIPC_TYPE_COMMAND;
            message[NBIPC_CATEGORY] = NBIPC_CATEGORY_BROWSER;
            message[NBIPC_ACTION] = NBIPC_ACTION_EXIT;

            QByteArray block;
            QDataStream out(&block, QIODevice::WriteOnly);
            out.setVersion(QDataStream::Qt_4_8);
            out << Json::encode(message);
            _userChannel->write(block);
        }

        // We wait for a while before killing it by force
        if(_userBrowser) {
            //_userBrowser->terminate();
            result = _userBrowser->waitForFinished(5000);

            if (!result) {
                // Graceful close timed out (or failed for some other reason); hit it with
                // a large hammer instead.
                _userBrowser->kill();
                result = _userBrowser->waitForFinished(5000);
            }
        } else {
            logger()->debug("Browser not running, ignore close command");
        }

    } catch(...) {
        logger()->fatal("Fatal exception when trying to close user browser!");
    }

    // Call finish slot to clean up in case the slot is not emitted
    userBrowserFinished();
    return result;
}

bool EONebula::IsUserBrowserOpen() {
    return (_userBrowser!=NULL);
}

void EONebula::userBrowserStarted() {

}

void EONebula::userBrowserFinished() {
    try {
        // Close event channel
        if(_userChannel) {
            _userChannel->close();
            delete _userChannel;
            _userChannel = NULL;
        }
        if(_userServer) {
            _userServer->close();
            delete _userServer;
            _userServer = NULL;
        }
        if(_userBrowser) {
            delete _userBrowser;
            _userBrowser = NULL;
            logger()->info("User browser closed");
        }
    } catch (...) {
        logger()->fatal("Fatal exception when trying to dispose user browser!");
        _userBrowser = NULL;
    }
}

void EONebula::userBrowserConnected() {
    logger()->info("User browser channel connected");
    if(!_userServer) {
        logger()->error("User browser channel server error");
        return;
    }

    _userChannel = _userServer->nextPendingConnection();

    if(!_userChannel) {
        logger()->error("Failed to connect to user browser channel");
        return;
    }

    connect(_userChannel, SIGNAL(readyRead()), this, SLOT(userBrowserRead()));
    connect(_userChannel, SIGNAL(error(QLocalSocket::LocalSocketError)), this, SLOT(userBrowserError(QLocalSocket::LocalSocketError)));

}

void EONebula::userBrowserRead() {
    if(!_userChannel) {
        logger()->error("Invalid message channel to user browser");
        return;
    }

    QDataStream in(_userChannel);
    in.setVersion(QDataStream::Qt_4_8);

    if (_userChannel->bytesAvailable() < 2) {
        logger()->error("Invalid message received from User Browser");
        return;
    }

    QString jsonString;
    in >> jsonString;

    logger()->debug("Message received from user browser: %1", jsonString);

    QMap<QString, QVariant> message = Json::decode(jsonString);
    if(message.isEmpty() || message[NBIPC_CATEGORY] != "browser") {
        return;
    }

    if(message[NBIPC_TYPE] == NBIPC_TYPE_NOTIFICATION) {
        QString action = message[NBIPC_ACTION].toString();
        if(action == NBIPC_ACTION_OPENED) {
            FireNotificationEvent("browser", "navigate", message[NBIPC_PAYLOAD].toString());
        }
    }
}

void EONebula::userBrowserError(QLocalSocket::LocalSocketError error) {
    logger()->error("User browser error: %1", error);
}

/******************************************************************************/
/**
 * Get master volume
 *
 * @return
 *		0 - 100
 *
 ******************************************************************************/
int EONebula::GetVolume() {
#if defined(Q_OS_WIN)	// Windows-specific code
    HRESULT hr=NULL;

    CoInitialize(NULL);
    IMMDeviceEnumerator *deviceEnumerator = NULL;
    hr = CoCreateInstance(__uuidof(MMDeviceEnumerator), NULL, CLSCTX_INPROC_SERVER,
                          __uuidof(IMMDeviceEnumerator), (LPVOID *)&deviceEnumerator);
    IMMDevice *defaultDevice = NULL;

    hr = deviceEnumerator->GetDefaultAudioEndpoint(eRender, eConsole, &defaultDevice);
    deviceEnumerator->Release();
    deviceEnumerator = NULL;

    IAudioEndpointVolume *endpointVolume = NULL;
    hr = defaultDevice->Activate(__uuidof(IAudioEndpointVolume),
                                 CLSCTX_INPROC_SERVER, NULL, (LPVOID *)&endpointVolume);
    defaultDevice->Release();
    defaultDevice = NULL;

    float currentVolume = 0;
    hr = endpointVolume->GetMasterVolumeLevelScalar(&currentVolume);

    if(hr == S_OK) {
        logger()->debug("Getting volume %1", qRound(currentVolume*100.0));
        _volume = qRound(currentVolume * 100.0);
    } else {
        logger()->debug("Getting volume failed");
    }

    endpointVolume->Release();

    CoUninitialize();
#endif

    return _volume;
}

/******************************************************************************/
/**
 * Set master volume
 *
 * @vol
 *		0 - 100
 * @return
 *		True if successful
 *
 ******************************************************************************/
bool EONebula::SetVolume(int vol) {
    bool ret = false;

#if defined(Q_OS_WIN)	// Windows-specific code
    HRESULT hr=NULL;
    double newVolume=vol/100.0;
    if(newVolume>1.0)
        newVolume = 1.0;
    else if(newVolume<0.0)
        newVolume = 0.0;

    CoInitialize(NULL);
    IMMDeviceEnumerator *deviceEnumerator = NULL;
    hr = CoCreateInstance(__uuidof(MMDeviceEnumerator), NULL, CLSCTX_INPROC_SERVER,
                          __uuidof(IMMDeviceEnumerator), (LPVOID *)&deviceEnumerator);
    IMMDevice *defaultDevice = NULL;

    hr = deviceEnumerator->GetDefaultAudioEndpoint(eRender, eConsole, &defaultDevice);
    deviceEnumerator->Release();
    deviceEnumerator = NULL;

    IAudioEndpointVolume *endpointVolume = NULL;
    hr = defaultDevice->Activate(__uuidof(IAudioEndpointVolume),
                                 CLSCTX_INPROC_SERVER, NULL, (LPVOID *)&endpointVolume);
    defaultDevice->Release();
    defaultDevice = NULL;

    float currentVolume = 0;
    hr = endpointVolume->GetMasterVolumeLevelScalar(&currentVolume);
    hr = endpointVolume->SetMute(FALSE, NULL);
    hr = endpointVolume->SetMasterVolumeLevelScalar((float)newVolume, NULL);
    if(hr == S_OK) {
        _volume = vol;
        logger()->debug("Setting volume from %1 to %2", qRound(currentVolume*100.0), qRound(newVolume*100.0));
        _mainWindow.getOSDWidget()->ShowVolumeBar(_volume, 5000);
        ret = true;
    } else {
        logger()->debug("Setting volume failed");
    }

    endpointVolume->Release();

    CoUninitialize();
#endif

    return ret;
}

bool EONebula::GetMute() {
    bool ret = false;

#if defined(Q_OS_WIN)	// Windows-specific code
    HRESULT hr=NULL;

    CoInitialize(NULL);
    IMMDeviceEnumerator *deviceEnumerator = NULL;
    hr = CoCreateInstance(__uuidof(MMDeviceEnumerator), NULL, CLSCTX_INPROC_SERVER,
                          __uuidof(IMMDeviceEnumerator), (LPVOID *)&deviceEnumerator);
    IMMDevice *defaultDevice = NULL;

    hr = deviceEnumerator->GetDefaultAudioEndpoint(eRender, eConsole, &defaultDevice);
    deviceEnumerator->Release();
    deviceEnumerator = NULL;

    IAudioEndpointVolume *endpointVolume = NULL;
    hr = defaultDevice->Activate(__uuidof(IAudioEndpointVolume),
                                 CLSCTX_INPROC_SERVER, NULL, (LPVOID *)&endpointVolume);
    defaultDevice->Release();
    defaultDevice = NULL;

    BOOL mute = FALSE;
    hr = endpointVolume->GetMute(&mute);

    if(hr == S_OK) {
        ret = (bool)mute;
        logger()->debug("Getting mute: %1", ret?"true":"false");
    } else {
        logger()->debug("Getting mute failed");
    }

    endpointVolume->Release();

    CoUninitialize();
#endif

    return ret;
}

bool EONebula::SetMute(bool bMute) {
    bool ret = false;

#if defined(Q_OS_WIN)	// Windows-specific code
    HRESULT hr=NULL;

    CoInitialize(NULL);
    IMMDeviceEnumerator *deviceEnumerator = NULL;
    hr = CoCreateInstance(__uuidof(MMDeviceEnumerator), NULL, CLSCTX_INPROC_SERVER,
                          __uuidof(IMMDeviceEnumerator), (LPVOID *)&deviceEnumerator);
    IMMDevice *defaultDevice = NULL;

    hr = deviceEnumerator->GetDefaultAudioEndpoint(eRender, eConsole, &defaultDevice);
    deviceEnumerator->Release();
    deviceEnumerator = NULL;

    IAudioEndpointVolume *endpointVolume = NULL;
    hr = defaultDevice->Activate(__uuidof(IAudioEndpointVolume),
                                 CLSCTX_INPROC_SERVER, NULL, (LPVOID *)&endpointVolume);
    defaultDevice->Release();
    defaultDevice = NULL;

    hr = endpointVolume->SetMute(bMute?1:0, NULL);
    if(hr == S_OK) {
        logger()->debug("Setting mute to %1", bMute?"true":"false");
        if(bMute)
            _mainWindow.getOSDWidget()->ShowVolumeBar(0, 5000);
        else
            _mainWindow.getOSDWidget()->ShowVolumeBar(_volume, 5000);
        ret = true;
    } else {
        logger()->debug("Setting volume failed");
    }

    endpointVolume->Release();

    CoUninitialize();
#endif

    return ret;
}

// Get/Set display on/off status
bool EONebula::GetDisplayEnabled() {
    bool on = _mainWindow.getJaoController()->GetScreenStatus();
    logger()->debug("Getting display status: %1", on?"on":"off");
    return on;
}

bool EONebula::SetDisplayEnabled(bool on) {
    logger()->debug("Setting display status to %1", on?"on":"off");
    _mainWindow.getJaoController()->SetScreenStatus(on);
    return true;
}

void EONebula::autoUpdateTimeout() {
    autoUpdateSelf();
}

bool EONebula::autoUpdateSelf(bool force/* =false */) {
    if(!autoUpdateAvailable()) {
        logger()->info("No auto update found at this moment");
        return false;
    }
    if(!force && GetDisplayEnabled()) {
        logger()->info("Terminal is on, skip this update");
        return false;
    }
    return autoUpdateStart();
}

bool EONebula::autoUpdateAvailable() {
    QProcess check(this);
    QStringList args;
    QString wysPath = _autoUpdateServerPath + "wyserver.wys";
    QString wycPath = QCoreApplication::applicationDirPath() + "/client.wyc";
    QString wyUpdateFile = QCoreApplication::applicationDirPath() + "/wyUpdate.exe";

    args << "/quickcheck" << "/justcheck" << "/noerr";
    args << QString("/server=\"%1\"").arg(wysPath);

    if(!QFile::exists(wyUpdateFile) || !QFile::exists(wycPath)) {
        logger()->error("Could not locate wyUpdate.exe or client.wyc files. Update cancelled.");
        return false;
    }

    logger()->info("Checking auto update on %1", wysPath);

    check.start(wyUpdateFile, args);
    // If it doesn't return in 10 seconds, we call it fail
    if (!check.waitForFinished(10000)) {
        logger()->info("Auto update checking taking too long, cancel this time");
        return false;
    }
    int ret =  check.exitCode();	// wyUpdate will return 2 if there is an update
    logger()->info("Auto update checking returned %1", ret);
    return (ret == 2);
}

bool EONebula::autoUpdateStart() {
    QStringList args;
    QString wysPath = _autoUpdateServerPath + "wyserver.wys";
    args << "/skipinfo";
    args << "-startonerr=\"nebula.exe\"";
    args << QString("/server=\"%1\"").arg(wysPath);

    logger()->info("Close Nebula and perform auto update now...");

    bool started = QProcess::startDetached("wyUpdate.exe", args);

    if(started) {
        QCoreApplication::exit(0);
        return true;
    } else {
        logger()->error("Unable to start update process!");
        return false;
    }
}

bool EONebula::GetAutoUpdateEnabled() {
    return _autoUpdateTimer.isActive();
}

void EONebula::SetAutoUpdateEnabled(bool bEnabled, QString path/* ="" */) {
    if(!path.isEmpty()) {
        _autoUpdateServerPath = path;
        if(!_autoUpdateServerPath.endsWith("/")) {
            _autoUpdateServerPath += "/";
        }
    }

    if(!bEnabled) {
        _autoUpdateTimer.stop();
    } else if(!GetAutoUpdateEnabled()) {
        _autoUpdateTimer.start();
    }
}

bool EONebula::AutoUpdate() {
    // Calling this API will force the update even if display is on
    return autoUpdateSelf(true);
}

