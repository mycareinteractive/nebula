/******************************************************************************/
/** @file
 *
 * @brief	Definitions and declarations for the Nebula API
 *
 * @author
 *		Michael K. Jones\n
 *		Stone Hill Consulting\n
 *		http://www.stonehill.com\n
 *
 * @par Environment
 *		Windows Embedded Standard 7
 *		Microsoft Visual Studio 2010
 *		Qt Framework 4.8.3
 *****************************************************************************/
#ifndef EONEBULA_H
#define EONEBULA_H

#include <QObject>
#include <QTimer>
#include <QStringList>
#include <QMap>
#include <QString>
#include <QProcess>
#include <QWebView>
#include <QLocalServer>
#include <QLocalSocket>
#include <QMutex>
#include "common.h"
#include "Events.h"
#include "logger.h"

class MainWindow;
/******************************************************************************/
/**
 * Class which contains the Nebula API for Javascript
 *
 ******************************************************************************/
class EONebula : public QObject {
    Q_OBJECT
    LOG4QT_DECLARE_QCLASS_LOGGER

  public:
    // Construction/destruction
    EONebula(MainWindow &mainWindow);
    ~EONebula();

    // ----------
    // Properties
    // ----------

    // Current version of the API (major.minor)
    Q_PROPERTY(QString ApiRevision READ getApiRevision);
    IMPL_PROPERTY(QString, ApiRevision);

    // Current version of the Nebula executable
    Q_PROPERTY(QString NativeRevision READ getNativeRevision);
    IMPL_PROPERTY(QString, NativeRevision);

    // Web debugger enable/disable
    Q_PROPERTY(bool Debug READ Debug WRITE setDebug);
    bool Debug() const;
    void setDebug(bool bEnabled);

    // -----------------
    // Invokable Methods
    // -----------------

    // Return list of network devices (interfaces)
    Q_INVOKABLE QStringList GetNetworkDeviceList();

    // Return a specific network device (interface)
    Q_INVOKABLE QObject * GetNetworkDevice(QString name);

    // Log the specified debug message
    Q_INVOKABLE void LogDebugMessage(int type, QString msg);

    // Reload the main application page
    Q_INVOKABLE bool Reload(bool bWait);

    // Start the specified executable as a separate process
    Q_INVOKABLE void StartProcess(QString exec);

    // Get/set handshake timeout, and perform the handshake
    Q_INVOKABLE int GetHandshakeTimeout() const;
    Q_INVOKABLE void SetHandshakeTimeout(int seconds);
    Q_INVOKABLE bool Handshake(QString name);

    // Get/set the timezone name and mode
    Q_INVOKABLE QString GetTimeZoneName();
    Q_INVOKABLE bool SetTimeZoneName(QString name);

    // Get/Set the logging level for the specified component
    Q_INVOKABLE bool SetLogLevel(QString category, int level);
    Q_INVOKABLE int GetLogLevel(QString category);

    // Get/Set the ProIdiom authorization parameters
    Q_INVOKABLE bool SetAuthMode(bool bEnabled, int primaryChan, int secondaryChan);
    Q_INVOKABLE bool GetAuthModeEnabled();
    Q_INVOKABLE int GetAuthModePrimaryChannel();
    Q_INVOKABLE int GetAuthModeSecondaryChannel();

    // For firing events (primary intended for testing, but it might be useful
    // for the script)
    Q_INVOKABLE void FireCommandEvent(int keyCode, QString msg, int modifiers);
    Q_INVOKABLE void FireNotificationEvent(QString type, QString action, QString msg);
    Q_INVOKABLE void ScheduleCommandEvent(int timeMS, QString msg);
    Q_INVOKABLE void ScheduleNotificationEvent(int timeMS, QString msg);

    // Restart the terminal
    Q_INVOKABLE bool Restart(bool coldboot=false);

    // User browser window
    Q_INVOKABLE bool OpenUserBrowser(QString url="", int x=0, int y=0, int width=0, int height=0);
    Q_INVOKABLE bool CloseUserBrowser();
    Q_INVOKABLE bool IsUserBrowserOpen();

    // Volume control
    Q_INVOKABLE int GetVolume();
    Q_INVOKABLE bool SetVolume(int vol);
    Q_INVOKABLE bool GetMute();
    Q_INVOKABLE bool SetMute(bool bMute);

    // Display control
    Q_INVOKABLE bool GetDisplayEnabled();
    Q_INVOKABLE bool SetDisplayEnabled(bool on);

    // Auto Update related
    Q_INVOKABLE bool GetAutoUpdateEnabled();
    Q_INVOKABLE void SetAutoUpdateEnabled(bool bEnabled, QString path="");
    Q_INVOKABLE bool AutoUpdate();

    // --------------
    // Public signals
    // --------------
  signals:

    // A command event is signaled
    void CommandEvent(QVariantMap ev);

    // A notification event is signaled
    void NotificationEvent(QVariantMap ev);


    // ---------------
    // Private Methods
    // ---------------
  private slots:

    // Handle a timeout of the handshake watchdog timer
    void handshakeTimeout();

    void fireScheduledCommandEvent();
    void fireScheduledNotificationEvent();

    void userBrowserConnected();
    void userBrowserRead();
    void userBrowserError(QLocalSocket::LocalSocketError error);
    void userBrowserStarted();
    void userBrowserFinished();


    // Timer to kick in for auto update
    void autoUpdateTimeout();

    // Perform one auto update check cycle
    bool autoUpdateSelf(bool force=false);

    // Check if Nebula update is available
    bool autoUpdateAvailable();

    // Manually close Nebula and start update
    bool autoUpdateStart();

    // ------------------------
    // Private Member Variables
    // ------------------------
  private:
    // Reference to the main web view window
    MainWindow &_mainWindow;

    // Web page debugger
    QWebInspector *_webInspector;

    // Handshake timer
    QTimer _watchdog;

    // Logging levels for components
    QMap<QString, int> _logLevel;

    // ProIdiom authorization parameters (just stubbed for now)
    bool _proIdiomEnabled;
    int _proIdiomPrimaryChan;
    int _proIdiomSecondaryChan;

    // For scheduling events
    QStringList _scheduledCommandMessages;
    QStringList _scheduledNotificationMessages;

    // For starting/stopping the user browser
    QString _channelName;
    QProcess *_userBrowser;
    QLocalServer *_userServer;
    QLocalSocket *_userChannel;
    QMutex _userMutex;

    // current volume
    int _volume;

    /// Auto update server
    QString _autoUpdateServerPath;

    /// Auto update timer
    QTimer _autoUpdateTimer;
};


#endif // EONEBULA_H
