/******************************************************************************/
/** @file 
 *
 * @brief	Definitions and declarations for the Nebula network device object
 *
 * @author
 *		Michael K. Jones\n
 *		Stone Hill Consulting\n
 *		http://www.stonehill.com\n
 *
 * @par Environment
 *		Windows Embedded Standard 7
 *		Microsoft Visual Studio 2010
 *		Qt Framework 4.8.3
 *****************************************************************************/
#ifndef EONETWORKDEVICE_H
#define EONETWORKDEVICE_H

#include <QNetworkInterface>
#include <QStringList>
#include "common.h"

// Return a list of the available network interfaces.
QStringList GetNetworkDeviceList();

	
/******************************************************************************/
/**
 * Class which represents a specific network device for Javascript
 *
 ******************************************************************************/
class EONebulaNetworkDevice : public QObject
{
	Q_OBJECT

	// The underlying interface we are proving access to
	QNetworkInterface _dev;

public:

	// Construction/destruction
	EONebulaNetworkDevice(QString name="");
	~EONebulaNetworkDevice();

	// -----------------
	// Invokable Methods
	// -----------------

	// Return a human-readable name for the interface
	Q_INVOKABLE QString getHumanReadableName();

	// Return the low-level physical address of the interface
	Q_INVOKABLE QString getMAC();

	// Return the first ip address of the interface
	Q_INVOKABLE QString getIP();

	// Returns whether or not this interface contains valid information
	Q_INVOKABLE bool isValid();

	// Return whether or not the interface is active
	Q_INVOKABLE bool isActive();

	// Assign the specified interface name
	Q_INVOKABLE bool assignInterface(const QString name);
};


#endif // EONETWORKDEVICE_H
