/******************************************************************************/
/** @file
 *
 * @brief	Declaration for the TimeZones class
 *
 *		This file declare the TimeZones object, which wraps getting and
 *		setting the Windows time zone.
 *
 * @author
 *		Michael K. Jones\n
 *		Stone Hill Consulting\n
 *		http://www.stonehill.com\n
 *
 * @par Environment
 *		Windows Embedded Standard 7
 *		Microsoft Visual Studio 2010
 *		Qt Framework 4.8.3
 *****************************************************************************/

#include <QObject>
#include <QString>
#include "logger.h"


/******************************************************************************/
/**
 * Class which contains the wraps getting/setting the time zone in Windows.
 *
 ******************************************************************************/
class TimeZones : public QObject
{
	Q_OBJECT
	LOG4QT_DECLARE_QCLASS_LOGGER

public:
	// Constructor (initialize the settings object)
	TimeZones();

	// Destructor cleans up when the object is going away
	~TimeZones();

	// Return the current time zone.
	QString GetCurrent();

	// Set the specified time zone as current
	bool SetCurrent (QString name);
};
