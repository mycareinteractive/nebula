<!-- Module to test UI related API functions -->
module("Test UI Functions")

test("reload", 1, function() {
    <!-- Can't call this one, since it reloads the page! -->
    equal(typeof Nebula.reload, "function", "Wrapper function is present");
});

test("startNativeProcess", 3, function() {
    equal(typeof Nebula.startNativeProcess, "function", "Wrapper function is present");
    var result = Nebula.startNativeProcess("Blarg");
    equal(result, null, "Attempt to start invalid process");
    result = Nebula.startNativeProcess("taskmgr.exe");
    equal(result, null, "Attempt to start valid process");
});

test("User Browser", 8, function() {
    ok(Nebula.openUserBrowser(), "Open user browser with default arguments");
    ok(Nebula.closeUserBrowser(), "Close user browser");

    ok(Nebula.openUserBrowser("google.com"), "Open user browser with partial URL");
    ok(Nebula.closeUserBrowser(), "Close user browser");

    ok(Nebula.openUserBrowser("", 256, 128), "Open user browser at position");
    ok(Nebula.closeUserBrowser(), "Close user browser");

    ok(Nebula.openUserBrowser("", 128,12, 800, 600), "Open user browser with position and size");
    ok(Nebula.closeUserBrowser(), "Close user browser");
});