<!-- Module to validate the environment in which the tests are running -->
module("Validate Test Environment")

test("Verify Nebula API version matches tests", 1, function() {
    equal(Nebula.getAPIRevision(), "0.1");
});

test("Debug property is present", 2, function() {
    equal(typeof Nebula.NebulaObj.Debug, "boolean", "Native-code property exists");
    equal(typeof Nebula.Debug, "function", "Wrapper function exists");
});

test("Can turn Debug on and off", 2, function() {
    var originalValue = Nebula.Debug();
    Nebula.Debug(!originalValue);
    equal(Nebula.Debug(), !originalValue, "Can toggle value of Debug");
    Nebula.Debug(originalValue);
    equal(Nebula.Debug(), originalValue, "Set Debug back to original value");
});

test("Log Levels", 8, function() {
    equal(typeof Nebula.getLogLevel, "function", "getLogLevel wrapper function exists");
    var level = Nebula.getLogLevel("Test");
    equal(typeof level, "number", "getLogLevel returns a number");
    equal(level, -1, "getLogLevel returns -1 if category never set");

    equal(typeof Nebula.setLogLevel, "function", "setLogLevel wrapper function exists");
    ok(Nebula.setLogLevel("Test", 42), "setLogLevel for new category succeeds");
    equal(Nebula.getLogLevel("Test"), 42, "setLogLevel value is correct");
    ok(Nebula.setLogLevel("Test", level), "setLogLevel for existing category succeeds");
    equal(Nebula.getLogLevel("Test"), level, "setLogLevel value is correct");
});

test("logMessage", 2, function() {
    equal(typeof Nebula.logMessage, "function", "Wrapper function exists");
    var result = Nebula.logMessage("Test message");
    equal(result, null, "Can successfully log a test message");
});

test("Handshake", 6, function() {
    equal(typeof Nebula.getHandshakeTimeout, "function", "getHandshakeTimeout wrapper function exists");
    equal(typeof Nebula.getHandshakeTimeout(), "number", "getHandshakeTimeout returns a number");
    equal(typeof Nebula.setHandshakeTimeout, "function", "setHandshakeTimeout wrapper function exists");
    equal(typeof Nebula.handshake, "function", "handshake wrapper function exists");

    // Async test for handshaking
    stop();

    // Set the handshake timeout to expire after 2 seconds
    Nebula.setHandshakeTimeout(2);
    equal(Nebula.getHandshakeTimeout(), 2, "Can set handshake timeout");

    // Set a timer to go off 2 times per second, and do the handshake
    var timerCount = 0;
    var timer = window.setInterval(function ()
    {
        timerCount++;
        Nebula.handshake("Test");
        if (timerCount > 5)
        {
            // After more than 2 seconds, disable the handshake and resume the test
            Nebula.setHandshakeTimeout(0);
            clearInterval(timer);
            start();
            ok(true, "Handshake prevents browser restart");
        }
    }, 500);
});

test("Time Zone Functions", 9, function() {
    equal(typeof Nebula.getTimeZoneName, "function", "getTimeZoneName function exists");
    var originalTimeZone = Nebula.getTimeZoneName();
    equal(typeof originalTimeZone, "string", "getTimeZoneName returns a string");
    ok(originalTimeZone.length > 0, "getTimeZoneName value is non-empty");

    equal(typeof Nebula.setTimeZoneName, "function", "setTimeZoneName function exists");
    ok(!Nebula.setTimeZoneName("Foo"), "setTimeZoneName fails with invalid time zone");
    equal(Nebula.getTimeZoneName(), originalTimeZone, "Time zone is unchanged");

    var newTimeZone = "(UTC) Coordinated Universal Time";
    ok(Nebula.setTimeZoneName(newTimeZone), "Attempt to change time zone to " + newTimeZone);
    equal(Nebula.getTimeZoneName(), newTimeZone, "Time zone should now be " + newTimeZone);

    Nebula.setTimeZoneName(originalTimeZone);
    equal(Nebula.getTimeZoneName(), originalTimeZone, "Time zone is restored to original value");
});

