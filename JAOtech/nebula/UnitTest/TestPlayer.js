<!-- Module to test video player and TV tuner API functions -->
module("Test Player and Tuner Functions")

test("Auth Mode", 15, function() {
    equal(typeof Nebula.getAuthModeEnabled, "function", "getAuthModeEnabled wrapper function exists");
    var originalEnabled = Nebula.getAuthModeEnabled();
    equal(typeof originalEnabled, "boolean", "getAuthModeEnabled returns a boolean");
    equal(typeof Nebula.getAuthModePrimaryChannel, "function", "getAuthModePrimaryChannel wrapper function exists");
    var originalPrimaryChan = Nebula.getAuthModePrimaryChannel();
    equal(typeof originalPrimaryChan, "number", "getAuthModePrimaryChannel returns a number");
    equal(typeof Nebula.getAuthModeSecondaryChannel, "function", "getAuthModeSecondaryChannel wrapper function exists");
    var originalSecondaryChan = Nebula.getAuthModeSecondaryChannel();
    equal(typeof originalSecondaryChan, "number", "getAuthModeSecondaryChannel returns a number");
    equal(typeof Nebula.setAuthMode, "function", "setAuthMode wrapper function exists");
    ok(Nebula.setAuthMode(true, 42, 24), "setAuthMode succeeds");
    equal(Nebula.getAuthModeEnabled(), true, "New auth mode enabled");
    equal(Nebula.getAuthModePrimaryChannel(), 42, "New auth mode primary channel");
    equal(Nebula.getAuthModeSecondaryChannel(), 24, "New auth mode secondary channel");
    ok(Nebula.setAuthMode(originalEnabled, originalPrimaryChan, originalSecondaryChan), "Restore original auth mode succeeds");
    equal(Nebula.getAuthModeEnabled(), originalEnabled, "Restore auth mode enabled");
    equal(Nebula.getAuthModePrimaryChannel(), originalPrimaryChan, "Restore auth mode primary channel");
    equal(Nebula.getAuthModeSecondaryChannel(), originalSecondaryChan, "Restore auth mode secondary channel");
});

