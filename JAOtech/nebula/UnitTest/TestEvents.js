<!-- Module to test event handling API functions -->
module("Test Event Functions");

test("Command Events", 21, function() {
    equal(typeof Nebula.addCommandHandler, "function", "addCommandHandler wrapper function exists");

    // Add an event handler, and fire a test event.  Makes sure the event comes through.
    function handler1(event)
    {
        equal(typeof event, "object", "Event delivered");
        equal(event.Code, 1, "Event code present");
        equal(event.Message, "Test Message 1", "Event Message present");
        equal(event.Modifiers, 1, "Code modifiers present");
    }
    equal(Nebula.addCommandHandler(handler1), undefined, "Can successfully add a command handler");
    Nebula.NebulaObj.FireCommandEvent(1, "Test Message 1", 1);
    equal(Nebula.removeCommandHandler(handler1), undefined, "Can successfully remove a command handler");

    // Fire the event with no handler loaded (if previous handler IS called, it
    // will fail the tests)
    Nebula.NebulaObj.FireCommandEvent(2, "Test Message 2", 2);

    // Add multiple handlers, and make sure they all get called
    function multiHandler1(event) { equal(typeof event, "object", "Multi-handler1: Event delivered"); }
    function multiHandler2(event) { equal(typeof event, "object", "Multi-handler2: Event delivered"); }
    function multiHandler3(event) { equal(typeof event, "object", "Multi-handler3: Event delivered"); }
    equal(Nebula.addCommandHandler(multiHandler1), undefined, "Can successfully add multiple command handlers: 1");
    equal(Nebula.addCommandHandler(multiHandler2), undefined, "Can successfully add multiple command handlers: 2");
    equal(Nebula.addCommandHandler(multiHandler3), undefined, "Can successfully add multiple command handlers: 3");
    Nebula.NebulaObj.FireCommandEvent(3, "Test multiple message", 3);

    // Remove all handlers, and fire an event to make sure none get called again
    equal(Nebula.removeCommandHandler(multiHandler1), undefined, "Can successfully remove multiple command handlers: 1");
    equal(Nebula.removeCommandHandler(multiHandler2), undefined, "Can successfully remove multiple command handlers: 2");
    equal(Nebula.removeCommandHandler(multiHandler3), undefined, "Can successfully remove multiple command handlers: 3");
    Nebula.NebulaObj.FireCommandEvent(4, "Test multiple remove", 4);

    // Test an async event
    function asyncHandler(event)
    {
        equal(typeof event, "object", "Async event delivered");
        equal(event.Code, 100, "Async Event Code present");
        equal(event.Message, "100", "Async Event Message present");
        equal(Nebula.removeCommandHandler(asyncHandler), undefined, "Async handler removed");
        start();
    }
    equal(Nebula.addCommandHandler(asyncHandler), undefined, "Async event handler added");
    stop();
    Nebula.NebulaObj.ScheduleCommandEvent(1000, "100");
});

test("Notification Events", 22, function() {
    equal(typeof Nebula.addNotificationHandler, "function", "addNotificationHandler wrapper function exists");

    // Add an event handler, and fire a test event.  Makes sure the event comes through.
    function handler1(event)
    {
        equal(typeof event, "object", "Event delivered");
        equal(event.Message, "Test Message 1", "Event Message present");
        equal(event.Data.length, 3, "Event Data present");
    }
    equal(Nebula.addNotificationHandler(handler1), undefined, "Can successfully add a command handler");
    Nebula.NebulaObj.FireNotificationEvent({ Message:"Test Message 1", Data:[1,2,3] });
    equal(Nebula.removeNotificationHandler(handler1), undefined, "Can successfully remove a command handler");

    // Fire the event with no handler loaded (if previous handler IS called, it
    // will fail the tests)
    Nebula.NebulaObj.FireNotificationEvent({ Message:"Test Message 2", Data:[2] });

    // Add multiple handlers, and make sure they all get called
    function multiHandler1(event) { equal(typeof event, "object", "Multi-handler1: Event delivered"); }
    function multiHandler2(event) { equal(typeof event, "object", "Multi-handler2: Event delivered"); }
    function multiHandler3(event) { equal(typeof event, "object", "Multi-handler3: Event delivered"); }
    equal(Nebula.addNotificationHandler(multiHandler1), undefined, "Can successfully add multiple command handlers: 1");
    equal(Nebula.addNotificationHandler(multiHandler2), undefined, "Can successfully add multiple command handlers: 2");
    equal(Nebula.addNotificationHandler(multiHandler3), undefined, "Can successfully add multiple command handlers: 3");
    Nebula.NebulaObj.FireNotificationEvent({ Message:"Test multiple message", Data:[10,9,8,7] });

    // Remove all handlers, and fire an event to make sure none get called again
    equal(Nebula.removeNotificationHandler(multiHandler1), undefined, "Can successfully remove multiple command handlers: 1");
    equal(Nebula.removeNotificationHandler(multiHandler2), undefined, "Can successfully remove multiple command handlers: 2");
    equal(Nebula.removeNotificationHandler(multiHandler3), undefined, "Can successfully remove multiple command handlers: 3");
    Nebula.NebulaObj.FireNotificationEvent({ Message:"Test multiple remove", Data:[6,5] });

    // Test an aync event
    function asyncHandler(event)
    {
        equal(typeof event, "object", "Async event delivered");
        equal(event.Message, "Test Async Message", "Async Event Message present");
        equal(event.Data.length, 2, "Async Event Data present");
        equal(event.Data[0], 42, "Async Event Data[0] is correct");
        equal(event.Data[1], 24, "Async Event Data[1] is correct");
        equal(Nebula.removeNotificationHandler(asyncHandler), undefined, "Async handler removed");
        start();
    }
    equal(Nebula.addNotificationHandler(asyncHandler), undefined, "Async event handler added");
    stop();
    Nebula.NebulaObj.ScheduleNotificationEvent(1000, "Test Async Message");
});

