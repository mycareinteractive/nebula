<!-- Module to the network device API -->
module("Network Device API")

test("getNetworkDeviceList function", 2, function() {
    equal(typeof Nebula.getNetworkDeviceList, "function", "Wrapper function is present");
    equal(typeof Nebula.NebulaObj.GetNetworkDeviceList, "function", "Native code function is present");
});

test("getNetworkDevice function", 2, function() {
    equal(typeof Nebula.getNetworkDevice, "function", "Wrapper function is present");
    equal(typeof Nebula.NebulaObj.GetNetworkDevice, "function", "Native code function is present");
});

test("getNetworkDeviceList returns at least one device", 4, function() {
    var devList = Nebula.getNetworkDeviceList();
    ok(devList instanceof Array, "List is an array");
    ok(devList.length > 0, "List has at least one entry");
    equal(typeof devList[0], "string", "Entry is a string");
    ok(devList[0].length > 0, "Entry is non-empty");
});

test("getNetworkDevice returns a valid device", 7, function() {
    var devList = Nebula.getNetworkDeviceList();
    var dev = Nebula.getNetworkDevice(devList[0]);
    equal(typeof dev, "object", "NetworkDevice is an object");
    equal(typeof dev.getHumanReadableName, "function", "getHumanReadableName function is present");
    equal(typeof dev.getHumanReadableName(), "string", "getHumanReadableName returns a string");
    ok(dev.getHumanReadableName().length > 0, "getHumanReadableName value is non-empty");
    equal(typeof dev.getMAC, "function", "getMAC function is present");
    equal(typeof dev.getMAC(), "string", "getMAC returns a string");
    ok(dev.getMAC().length > 0, "getMAC value is non-empty");
});

