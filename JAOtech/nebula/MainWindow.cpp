/******************************************************************************/
/** @file
 *
 * @brief	Implementation for main UI window
 *
 *		This file implements the main UI window.  It's a full-screen browser
 *		window without any decoration -- no window frame, title bar, menu bar,
 *		status line, tool bar, etc. (just the browser pane).
 *
 * @author
 *		Michael K. Jones\n
 *		Stone Hill Consulting\n
 *		http://www.stonehill.com\n
 *
 * @par Environment
 *		Windows Embedded Standard 7
 *		Microsoft Visual Studio 2010
 *		Qt Framework 4.8.3
 *****************************************************************************/
#include <Qt>
#include <QMessageBox>
#include <qt_windows.h>
#include <QWebFrame>
#include <QUrl>
#include <QCloseEvent>
#include <QSettings>
#include <QNetworkReply>
#include <QtDebug>
#include <QSslError>
#include <QCoreApplication>
#include <QDateTime>
#include <QTcpSocket>
#include <QHostAddress>
#include <QResource>
#include "MainWindow.h"
#include "EONetworkDevice.h"
#include "config.h"


#define STATIC_HTML "<!DOCTYPE HTML> <html> <head> <style> html {width:100%; height:100%;} body {background:#000000; font-family: Arial,sans-serif;} #loading-wrapper {position:absolute;left:50%;float:left;} #loading { position:relative; margin-left:-50%; float:left; width:200px; margin-top:250px; font-size:20px; color:white; text-align:center; } </style> </head> <body> <div id=\"loading-wrapper\"> <div id=\"loading\">Connecting to server...</div> </div> </body> </html>\0"

QString ToString(QSize val) {
    QString str;
    str.sprintf("(%d,%d)", val.width(), val.height());
    return str;
}

QString ToString(QPoint val) {
    QString str;
    str.sprintf("(%d,%d)", val.x(), val.y());
    return str;
}

void ShowHideTaskBar(bool bHide) {

#if defined(Q_OS_WIN)	// Windows-specific code
    // find taskbar window
    HWND taskbar = ::FindWindow(L"Shell_TrayWnd", L"");
    HWND start = ::FindWindow(L"Button", L"Start");
    if( bHide ) {
        if(taskbar!=NULL)
            ::ShowWindow(taskbar, SW_HIDE);
        if(start!=NULL)
            ::ShowWindow(start, SW_HIDE);
    } else {
        if(taskbar!=NULL)
            ::ShowWindow(taskbar, SW_SHOW);
        if(start!=NULL)
            ::ShowWindow(start, SW_SHOW);
    }
#endif

}


/******************************************************************************/
/**
 * Constructor; initializes a new main window object.
 *
 * @return
 *		None
 *
 ******************************************************************************/
MainWindow::MainWindow(
    QWidget *parent,		///< Parent widget (NULL for top-level window)
    QString overrideUrl,	///< Override initial URL to load with this one
    bool delayLoading)		///< Whether we should delay main web app loading
    : QWebView(parent)		// Parent class
    , _eoNebula(*this)		// Nebula API object
    , _rtspClient()			// RTSP client object
    , _page(NULL)			// Make us the parent of the main page
    , _progress(NULL)		// Page load progress dialog
    , _tvplayer(this)		// TV player
    , _osd(this)			// OSD display layer
    , _mainAppDelayTimer()	// Timer to delay main app loading
    , _mainAppRetryTimer()	// Timer to control main app retry if not connected
    , _isLocalAppRunning(false) {

    // Enable Javascript and NPAPI Plugins (Mozilla compatible plugins)
    QWebSettings::globalSettings()->setAttribute(QWebSettings::PluginsEnabled, true);
    QWebSettings::globalSettings()->setAttribute(QWebSettings::JavascriptEnabled, true);
    QWebSettings::globalSettings()->setAttribute(QWebSettings::AutoLoadImages, true);

    // Private mode ensures we don't cache anything on harddrive, or leave history behind
    QWebSettings::globalSettings()->setAttribute(QWebSettings::PrivateBrowsingEnabled, true);

    //QWebSettings::globalSettings()->setAttribute(QWebSettings::DeveloperExtrasEnabled, true);

    // disable page caching
    QWebSettings::globalSettings()->setObjectCacheCapacities(0, 0, 0);
    QWebSettings::globalSettings()->setMaximumPagesInCache(0);

    // Inject CSS to change global web page scrollbar
    QResource css(":/nebula/Resources/useragent.css");
    QByteArray cssBytes(reinterpret_cast<const char*>(css.data()), css.size());
    QString cssBase64(cssBytes.toBase64());
    QWebSettings::globalSettings()->setUserStyleSheetUrl(QUrl("data:text/css;charset=utf-8;base64," + cssBase64));

#if 0	// Connect up signals from the Nebula object
    connect(&_eoNebula, SIGNAL(addJavaScriptObject(QString, QObject *)),
            this,		SLOT(addJavaScriptObject(QString, QObject *)));
    connect(&_eoNebula,	SIGNAL(reload()),
            this,		SLOT(doReload()));
    connect(&_eoNebula,	SIGNAL(enableDebugger(bool)),
            this,		SLOT(enableDebugger(bool)));
#endif

    // set window icon
    //setWindowIcon(QIcon(":/userbrowser/userbrowser.png"));

    // Make the main window be full-screen (i.e. no window decoration or frame)
    setWindowFlags(
        Qt::MSWindowsFixedSizeDialogHint	// Thin border; also disables resize
        |	Qt::CustomizeWindowHint				// Turn off default window decoration
        |	Qt::FramelessWindowHint				// Don't want any window frame at all
        //| Qt::Tool // no taskbar icon
#if !defined(QT_DEBUG)
        |	Qt::WindowStaysOnTopHint			// Always keep the window on top
#endif
    );

#if !defined(QT_DEBUG)
    ShowHideTaskBar(true);
#endif

    // Normally, we just run full-screen; that can be overridden in the settings
    // file by specifying a rectangle (handy for debugging on a large monitor)
    QSettings s;
    QRect winRect = s.value("WindowRect").toRect();
    if (winRect.isValid()) {
        logger()->info("Setting window pos=%1 size=%2", ToString(winRect.topLeft()), ToString(winRect.size()));
        setFixedSize(winRect.size());
        move(winRect.topLeft());
    } else {
        showFullScreen();
    }

#if defined(Q_OS_WIN)	// Windows-specific code
    // In Qt, there is a way to remove the System Menu icon from the window,
    // but the user can still hit ALT+space to bring it up.  Unfortunately,
    // the ALT+space key does not show up in the keyPressEvent handler, so
    // it can't be blocked that way.  So we must use some Windows-specific
    // code to remove the system menu altogether.
    HWND hWnd = (HWND)winId();
    if (hWnd) {
        // Retrieve the current window style, clear the system menu flag,
        // and set the new style.  Note that this also removes the title
        // bar and min/max/close buttons, which is fine for our main
        // window.
        ULONG style = ::GetWindowLong(hWnd, GWL_STYLE);
        style &= ~WS_SYSMENU;
        ::SetWindowLong(hWnd, GWL_STYLE, style);
    }
#endif // Q_OS_WIN

#if 0
    // For debug builds, enable the web debugger by default
    _eoNebula.setDebug(true);
#endif

    // Start polling for hardware button presses.  Send events to this object.
    _hardware.setEventTarget(this);
    _hardware.StartButtonPolling();

    /// @TODO Need to disable CTRL-ALT-DEL -- maybe by building our own WES7 image?

    /// @TODO Need to disable ALT-Tab -- maybe by having nothing else running?

    // Initialize main app timers
    // wait a random time between 1000 ms and the DelayMax (by default 5 minutes)
    QTime time = QTime::currentTime();
    qsrand((uint)time.msec());	// random seed
    int waitMax = s.value("MainPageLoadDelayMax", 300000).toInt();
    int waitMs = (qrand()*1000) % ((waitMax + 1) - 1000) + 1000;
    _mainAppDelayTimer.setSingleShot(true);
    _mainAppDelayTimer.setInterval(waitMs);
    connect (&_mainAppDelayTimer, SIGNAL(timeout()), this, SLOT(mainAppDelayTimeout()));

    // If server not responding, retry in RetryDelay milliseconds (by default 1 minute)
    int retryMs = s.value("MainPageLoadRetryDelay", 60000).toInt();
    _mainAppRetryTimer.setSingleShot(true);
    _mainAppRetryTimer.setInterval(retryMs);
    connect (&_mainAppRetryTimer, SIGNAL(timeout()), this, SLOT(mainAppRetryTimeout()));

    // Load the initial URL
    //QUrl url;
    if (!overrideUrl.isEmpty()) {
        // URL provided on command line--use that
        logger()->info("Override URL by command line: %1", overrideUrl);
        _mainAppUrl.setUrl(overrideUrl, QUrl::TolerantMode);
    } else {
        // Get the initial URL as configured
        _mainAppUrl.setUrl(Config::GetInitialUrl(), QUrl::TolerantMode);
    }

    // Delay random time before connecting to server
    // During the time if any keypress or touch is detected, fire the reload right away
    loadMainApplication(delayLoading);

    // initialize TV tuner
    QString player = s.value("TVTunerPlayer", "VLC").toString();
    QString device = s.value("TVTunerHardware", "WinTV HVR-950 Capture").toString();
    QString options = s.value("TVTunerOptions", "").toString();
    _tvplayer.setEventTarget(this);
    if(!_tvplayer.Init(player, device, options)) {
        logger()->error("Failed to initialize TV player!");
        //TODO: Need some sort of fallback plan, maybe continue without any TV feature, or report back to server.
        QMessageBox::critical(this, "Error", "Failed to initialize TV player!");
    }

    return;
}


/******************************************************************************/
/**
 * Destructor; clean up when the window goes away.
 *
 * @return
 *		None
 *
 ******************************************************************************/
MainWindow::~MainWindow() {
    _tvplayer.Stop(false);
    delete _progress;
    ShowHideTaskBar(false);
}


/******************************************************************************/
/**
 * Add our objects to the Javascript frame (called each time a new frame
 * is cleared, prior to being loaded).
 *
 * @return
 *		None
 *
 ******************************************************************************/
void MainWindow::addJavascriptObjects() {
    // Bridge Nebula API to Javascript
    page()->mainFrame()->addToJavaScriptWindowObject(QString("EONebula"), &_eoNebula, QScriptEngine::QtOwnership);

    // Bridge the RTSP Client to Javascript
    page()->mainFrame()->addToJavaScriptWindowObject(QString("EORtspClient"), &_rtspClient, QScriptEngine::QtOwnership);

    // Bridge the TV player to Javascript
    page()->mainFrame()->addToJavaScriptWindowObject(QString("EOTVPlayer"), &_tvplayer, QScriptEngine::QtOwnership);
}


/******************************************************************************/
/**
 * Add the specified object to the Javascript frame (ownership is given to the
 * JavaScript window).
 *
 * @return
 *		None
 *
 ******************************************************************************/
void MainWindow::addJavaScriptObject(QString name, QObject *object) {
    // After this call, the script controls the lifetime of the object
    page()->mainFrame()->addToJavaScriptWindowObject(name, object, QScriptEngine::ScriptOwnership);
}


/******************************************************************************/
/**
 * Called to indicate loading of a new page has started
 *
 * @return
 *		None
 *
 ******************************************************************************/
void MainWindow::onLoadStarted(void) {
    if (!_progress)
        _progress = new QProgressDialog("Loading", "Cancel", 0, 100, this, Qt::WindowStaysOnTopHint);
    if (_progress)
        _progress->setValue(0);
}


/******************************************************************************/
/**
 * Called to indicate progress loading a page
 *
 * @return
 *		None
 *
 ******************************************************************************/
void MainWindow::onLoadProgress(int progress) {	///< Percentage of page loaded (0-100)
    if (_progress)
        _progress->setValue(progress);
}


/******************************************************************************/
/**
 * Called to indicate page load has completed
 *
 * @return
 *		None
 *
 ******************************************************************************/
void MainWindow::onLoadFinished(bool ok) {	///< True for success, false if an error occurred
    if (!ok)
        logger()->warn("Page load failed");
    if (_progress) {
        _progress->reset();
        delete _progress;
        _progress = NULL;
    }
}


/******************************************************************************/
/**
 * Called to display text on the status bar
 *
 * @return
 *		None
 *
 ******************************************************************************/
void MainWindow::onStatusBarMessage(const QString &text) {	///< text to display
    logger()->info("Status: %1", text);
}

void MainWindow::handleSslErrors(QNetworkReply* reply, const QList<QSslError> &errors) {
    logger()->debug("Handle SSL Errors: ");
    foreach (QSslError e, errors) {
        logger()->debug("    Error: %1", e.errorString());
    }
    reply->ignoreSslErrors();
}


/******************************************************************************/
/**
 * This event handler is called with the given event when Qt receives a window
 * close request for a top-level widget from the window system.
 *
 * We override this function so we can prevent the window from being closed
 * (in release builds only; in debug builds, we allow the close, but also
 * notify the Nebula API so it can clean up).
 *
 * @return
 *		None
 *
 ******************************************************************************/
void MainWindow::closeEvent(QCloseEvent *event) {
#if QT_DEBUG
    // In debug builds, turn off the debugger and accept the close event
    _eoNebula.setDebug(false);
    event->accept();
#else
    // Ignore the event in release build
    event->ignore();
#endif
}


/******************************************************************************/
/**
 * This event handler is called with the given event when a key is pressed on
 * the keyboard (AND when we have the focus).
 *
 * We override this function so we can generate Nebula events for "interesting"
 * keys.
 *
 * @return
 *		None
 *
 ******************************************************************************/
void MainWindow::keyPressEvent(QKeyEvent *event) {
    logger()->trace("keyPressEvent: key=%1, code=%2, mod=%3",
                    QKeySequence(event->key()).toString(),
                    "0x"+QString::number(event->key(), 16),
                    "0x"+QString::number(event->modifiers(), 16));

    //TODO: remove the following test code
    //test
#if 0
    if(_tvplayer.TestKey(event->key())) {
        return;
    }
    //end test
#endif

    // Handle key sequences
    processKeySequences(event);

    // Handle most common keys like Vol+, Vol-...etc
    processCommonKeys(event);

    // Check for the "escape to Windows" key sequence
    //checkEscapeToWindows(event);

    // Also check for the "enable debugger" key sequence
    //checkEnableDebugger(event);

    // Fire an event back through Nebula
    /// @TODO	Right now we send everything, but we may want to change this to
    ///			only send "interesting" key presses.
    if(event->key() > 0x01000060) {
        _eoNebula.FireCommandEvent(event->key(), event->text(), event->modifiers());
    } else {
        // Pass the key up to the parent to handle in the usual way
        QWebView::keyPressEvent(event);
    }
}

/******************************************************************************/
/**
 * This event handler is called with the context menu is about to show up.
 *
 * We override this function so we can disable right-click menu.
 *
 * @return
 *		None
 *
 ******************************************************************************/
void MainWindow::contextMenuEvent(QContextMenuEvent *event) {
    // do nothing
}

#if 0
/******************************************************************************/
/**
 * Check for the "escape to Windows" key sequence.
 *
 * @return
 *		True if sequence activated; false if not.
 *
 ******************************************************************************/
bool MainWindow::checkEscapeToWindows(QKeyEvent *event) {
    bool result = false;

    // Modifiers which must be present (uninterrupted!)
    const Qt::KeyboardModifier MODIFIERS = Qt::AltModifier;

    // Sequence of keys which must be seen (also uninterrupted)
    const int SEQUENCE[] = {
        Qt::Key_N, Qt::Key_E, Qt::Key_B, Qt::Key_U, Qt::Key_L, Qt::Key_A,
        0
    };

    // Current offset in the sequence
    static int offset = 0;

    // Range check the current offset (just defensive programming)
    if ((offset < 0) || (offset > sizeof(SEQUENCE))) {
        logger()->error("Escape sequence offset out of bounds: %1 (resetting to 0)", offset);
        offset = 0;
    }

    // Check to see if the current keypress is the next key in the escape sequence
    if ((event->modifiers() == MODIFIERS) && (event->key() == SEQUENCE[offset])) {
        // Advance to the next key in the sequence
        offset++;

        // If we've hit the end, escape to Windows!
        if (SEQUENCE[offset] == 0) {
            // For now, just minimize ourselves.  We may want to do something
            // different, especially under WES7, like start up Task Manager
            // or Explorer.
            offset = 0;
            result = true;
            showMinimized();
        }
    } else {
        // This key press is NOT part of the escape sequence; reset to the beginning
        offset = 0;
    }
    return result;
}


/******************************************************************************/
/**
 * Check for the "enable debugger" key sequence.
 *
 * @return
 *		True if sequence activated; false if not.
 *
 ******************************************************************************/
bool MainWindow::checkEnableDebugger(QKeyEvent *event) {
    bool result = false;

    // Modifiers which must be present (uninterrupted!)
    const Qt::KeyboardModifier MODIFIERS = Qt::AltModifier;

    // Sequence of keys which must be seen (also uninterrupted)
    const int SEQUENCE[] = {
        Qt::Key_D, Qt::Key_E, Qt::Key_B, Qt::Key_U, Qt::Key_G,
        0
    };

    // Current offset in the sequence
    static int offset = 0;

    // Range check the current offset (just defensive programming)
    if ((offset < 0) || (offset > sizeof(SEQUENCE))) {
        logger()->error("Debug sequence offset out of bounds: %1 (resetting to 0)", offset);
        offset = 0;
    }

    // Check to see if the current keypress is the next key in the escape sequence
    if ((event->modifiers() == MODIFIERS) && (event->key() == SEQUENCE[offset])) {
        // Advance to the next key in the sequence
        offset++;

        // If we've hit the end, escape to Windows!
        if (SEQUENCE[offset] == 0) {
            // Enable the internal debugger window
            offset = 0;
            result = true;
            _eoNebula.setDebug(true);
        }
    } else {
        // This key press is NOT part of the escape sequence; reset to the beginning
        offset = 0;
    }
    return result;
}

#endif

bool MainWindow::checkKeySequence(QKeyEvent *event, int& offset, const int sequence[], const int length, Qt::KeyboardModifier modifiers) {
    bool result = false;

    // Range check the current offset (just defensive programming)
    if ((offset < 0) || (offset >= length)) {
        logger()->debug("Sequence offset out of bounds: %1 (resetting to 0)", offset);
        offset = 0;
    }

    // Check to see if the current keypress is the next key in the escape sequence
    if ((event->modifiers() == modifiers) && (event->key() == sequence[offset])) {
        // Advance to the next key in the sequence
        offset++;

        // If we've hit the end, escape to Windows!
        if (sequence[offset] == 0) {
            offset = 0;
            result = true;
        }
    } else {
        // This key press is NOT part of the escape sequence; reset to the beginning
        offset = 0;
    }
    return result;
}

bool MainWindow::processKeySequences(QKeyEvent *event) {
    // Escape to windows desktop (Alt + N E B U L A)
    const int ESCAPE[] = { Qt::Key_N, Qt::Key_E, Qt::Key_B, Qt::Key_U, Qt::Key_L, Qt::Key_A, 0};
    static int OFFSET_ESCAPE = 0;
    if(checkKeySequence(event, OFFSET_ESCAPE, ESCAPE, sizeof(ESCAPE)/sizeof(int))) {
        logger()->info("NEBULA key sequence detected, escape to windows...");
        showMinimized();
        return true;
    }

    // Reload UI/web (Alt + R E L O A D)
    const int RELOAD[] = { Qt::Key_R, Qt::Key_E, Qt::Key_L, Qt::Key_O, Qt::Key_A, Qt::Key_D, 0};
    static int OFFSET_RELOAD = 0;
    if(checkKeySequence(event, OFFSET_RELOAD, RELOAD, sizeof(RELOAD)/sizeof(int))) {
        logger()->info("RELOAD key sequence detected, reloading web app...");
        _eoNebula.Reload(false);
        return true;
    }

    // Exit Nebula (Alt + E X I T A C E S O)
    const int EXIT[] = { Qt::Key_E, Qt::Key_X, Qt::Key_I, Qt::Key_T, Qt::Key_A, Qt::Key_C, Qt::Key_E, Qt::Key_S, Qt::Key_O, 0};
    static int OFFSET_EXIT = 0;
    if(checkKeySequence(event, OFFSET_EXIT, EXIT, sizeof(EXIT)/sizeof(int))) {
        logger()->info("EXITACESO key sequence detected, shutting down Nebula...");
        QCoreApplication::exit(0);
        return true;
    }

    // Restart Nebula (Alt + R E S T A R T)
    const int RESTART[] = { Qt::Key_R, Qt::Key_E, Qt::Key_S, Qt::Key_T, Qt::Key_A, Qt::Key_R, Qt::Key_T, 0};
    static int OFFSET_RESTART = 0;
    if(checkKeySequence(event, OFFSET_RESTART, RESTART, sizeof(RESTART)/sizeof(int))) {
        logger()->info("RESTART key sequence detected, restarting Nebula...");
        _eoNebula.Restart(false);
        return true;
    }

    // Enable web inspector (Alt + D E B U G)
    const int DEBUG[] = { Qt::Key_D, Qt::Key_E, Qt::Key_B, Qt::Key_U, Qt::Key_G, 0};
    static int OFFSET_DEBUG = 0;
    if(checkKeySequence(event, OFFSET_DEBUG, DEBUG, sizeof(DEBUG)/sizeof(int))) {
        logger()->info("DEBUG key sequence detected, launching web inspector...");
        _eoNebula.setDebug(true);
        return true;
    }

    // Start Auto Update Now
    const int UPDATE[] = { Qt::Key_U, Qt::Key_P, Qt::Key_D, Qt::Key_A, Qt::Key_T, Qt::Key_E, 0};
    static int OFFSET_UPDATE = 0;
    if(checkKeySequence(event, OFFSET_UPDATE, UPDATE, sizeof(UPDATE)/sizeof(int))) {
        logger()->info("UPDATE key sequence detected, starting update...");
        _eoNebula.AutoUpdate();
        return true;
    }

    return false;
}

bool MainWindow::processCommonKeys(QKeyEvent *event) {
    bool handled = true;
    int vol = 0;
    int bright = -1;
    bool on = _hardware.GetScreenStatus();
    bool mute = false;
    switch(event->key()) {
    case Qt::Key_VolumeUp:
        if(on) {
            vol = _eoNebula.GetVolume();
            _eoNebula.SetVolume(vol + 4);
        }
        break;
    case Qt::Key_VolumeDown:
        if(on) {
            vol = _eoNebula.GetVolume();
            _eoNebula.SetVolume(vol - 4);
        }
        break;
    case Qt::Key_VolumeMute:
        if(on) {
            mute = _eoNebula.GetMute();
            _eoNebula.SetMute(!mute);
        }
        break;
    case Qt::Key_F7:
        bright = _hardware.GetScreenBrightness();
        if(bright>=0 && bright<15)
            _hardware.SetScreenBrightness(bright+1);
        break;
    case Qt::Key_F8:
        bright = _hardware.GetScreenBrightness();
        if(bright>0 && bright<=15)
            _hardware.SetScreenBrightness(bright-1);
        break;
    case Qt::Key_PowerOff:
        if(on) {
            _hardware.SetScreenStatus(false);
            _eoNebula.SetMute(true);
        } else {
            _hardware.SetScreenStatus(true);
            _eoNebula.SetMute(false);
            // force reload if we are waiting
            if(_mainAppDelayTimer.isActive()) {
                loadMainApplication(false);
            }
        }
        break;
    default:
        handled = false;
        break;
    }
    return handled;
}

/******************************************************************************/
/**
 * Timeout slot for main app delay timer.
 * In some cases such as a mass power cycle in building, all clients get rebooted.
 * When they come back up they rush to server for HTTP pages and eventually crash the server.
 * We have a random timer to wait before connecting to avoid this issue.
 * @return
 *		None
 *
 ******************************************************************************/
void MainWindow::mainAppDelayTimeout() {
    loadMainApplication(false);
}

// When main app server is not available, we use this timer to control the retry rate
void MainWindow::mainAppRetryTimeout() {
    loadMainApplication(false);
}

// Try load the main application
// If server is not available, fall back to local application.
void MainWindow::loadMainApplication(bool bWait) {
    _mainAppDelayTimer.stop();
    _mainAppRetryTimer.stop();

    if (!_mainAppUrl.isValid()) {
        logger()->warn("*** Invalid initial URL '%1' ***", _mainAppUrl.toString());
        loadLocalApplication();
        return;
    }

    if(bWait) {
        _mainAppDelayTimer.start();
        logger()->info("*** Delay main page loading for %1 milliseconds ***", _mainAppDelayTimer.interval());
        if(!_isLocalAppRunning)
            loadLocalApplication();
        return;
    }

    if(_mainAppUrl.scheme()=="http" || _mainAppUrl.scheme()=="https") {
        // Now check server connection first, if good, then go ahead, otherwise retry in a while
        QTcpSocket socket;
        socket.connectToHost(_mainAppUrl.host(), _mainAppUrl.port(80));
        bool connected = socket.waitForConnected(2000);
        socket.abort();
        if(!connected) {
            logger()->warn("*** Server %1:%2 not responding, retry in %3 milliseconds ***", _mainAppUrl.host(), _mainAppUrl.port(80), _mainAppRetryTimer.interval());
            _mainAppRetryTimer.start();
            if(!_isLocalAppRunning)
                loadLocalApplication();
            return;
        }
    }

    resetApplication();

    logger()->info("****************************************************************************");
    logger()->info("*** Loading main page '%1' ***", _mainAppUrl.toString());
    logger()->info("****************************************************************************");

    load(_mainAppUrl);
    _isLocalAppRunning = false;
}

// Load a local HTML application.
// This is useful when there is network issue or server down time
// when the user can still access basic local functions such as
// watching TV channels.
// Also it helps delay the remote app loading to avoid rushing the server
// when there is a huge power cycle.
void MainWindow::loadLocalApplication() {
    QString localPath = QCoreApplication::applicationDirPath() + "/html";
    QString localApp = localPath + "/startup.html";
    logger()->info("****** Loading local app %1 ******", localApp);
    QFile localFile(localApp);

    resetApplication();
    if(!localFile.exists()) {
        logger()->info("****** Local app does not exist! Display connecting screen ******");
        setHtml(QString::fromUtf8(STATIC_HTML));
    } else {
        QUrl url;
        url.setUrl("file:///" + localApp, QUrl::TolerantMode);
        load(url);
    }
    _isLocalAppRunning = true;
}

void MainWindow::resetApplication() {
    logger()->info("Cleaning up web application leftovers...");
    _rtspClient.Stop();
    _tvplayer.SetTVLayerEnable(false);
    _tvplayer.Stop(false);

    // clear webkit page cache
    settings()->clearMemoryCaches();

    // deleting the old page.  Hopefully(?) all slots and JS bridges are destroyed as well
    if(_page)
        _page->deleteLater();

    _page = new MainPage(this);
    setPage(_page);

    // Connect up to get notified when the Javascript environment is cleared (i.e.
    // before starting a new load).  The docs says this:
    //
    //		This signal is emitted whenever the global window object of the
    //		JavaScript environment is cleared, e.g., before starting a new load.
    //
    //		If you intend to add QObjects to a QWebFrame using addToJavaScriptWindowObject(),
    //		you should add them in a slot connected to this signal. This ensures that
    //		your objects remain accessible when loading new URLs.
    connect(page()->mainFrame(), SIGNAL(javaScriptWindowObjectCleared()), this, SLOT(addJavascriptObjects()));

    // Connect page load signals on main frame only.  Otherwise all subsequent iframe loading will trigger this as well
    connect(page()->mainFrame(), SIGNAL(loadStarted()),	this, SLOT(onLoadStarted()));
    connect(page(), SIGNAL(loadProgress(int)),	this, SLOT(onLoadProgress(int)));
    connect(page()->mainFrame(), SIGNAL(loadFinished(bool)), this, SLOT(onLoadFinished(bool)));

    connect(page()->networkAccessManager(), SIGNAL(sslErrors(QNetworkReply*, const QList<QSslError> & )), this, SLOT(handleSslErrors(QNetworkReply*, const QList<QSslError> & )));
}

/******************************************************************************/
/**
 * Constructor; initializes a new main page object.
 *
 * @return
 *		None
 *
 ******************************************************************************/
MainPage::MainPage(QObject *parent)	///< Parent object (NULL for top-level window)
    : QWebPage(parent)
    ,_enabled(false) {
    QSettings s;
    int enabled = s.value("EnableJavascriptLogging").toInt();
    _enabled = (bool)enabled;

    // Set default background color to something other than white to distinguish from HTML background
    QPalette palette = this->palette();
    palette.setBrush(QPalette::Base, Qt::black);
    setPalette(palette);
}


/******************************************************************************/
/**
 * Destructor; clean up when the window goes away.
 *
 * @return
 *		None
 *
 ******************************************************************************/
MainPage::~MainPage() {
    // Nothing to do (yet, anyway)
}


/******************************************************************************/
/**
 * This function is called whenever a JavaScript program tries to print a message
 * to the web browser's console.  For example in case of evaluation errors the
 * source URL may be provided in sourceID as well as the lineNumber.
 *
 * @return
 *		None
 *
 ******************************************************************************/
void MainPage::javaScriptConsoleMessage(
    const QString &message,		///< Message to display
    int lineNumber,				///< Line number in script
    const QString &sourceID) {	///< Source URL
    if(!_enabled)
        return;

    // Format the string to print out
    QString log = "JS Console >>>    ";

    // Add the message to display
    log += message;

    // If there's a line number, combine it with the source URL
    if (lineNumber) {
        // The source ID is an encoded URL; we want to display the "normal" version
        QUrl url = QUrl::fromEncoded(sourceID.toLatin1());
        QStringList urlParts = url.toString().split('/');
        log += "                              " + urlParts.value(urlParts.length() - 1);
        log += ':' + QString::number(lineNumber);
    }

    logger()->trace(log);
}

QString MainPage::userAgentForUrl (const QUrl & url) const {
    QString str = QWebPage::userAgentForUrl(url);
    QString appVersion = QCoreApplication::applicationName() + QString("/") + QCoreApplication::applicationVersion();
    str.replace(appVersion, QString("Version/5.0"));
    return str;
}
