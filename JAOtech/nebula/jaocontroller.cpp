/******************************************************************************/
/** @file
 *
 * @brief	Declarations for JaoTech controller
 *
 *		This file contains the implementation for the JaoTech controller.  This
 *		object is used to interface to the GPIO functions in the JaoTech terminal.
 *
 * @author
 *		Michael K. Jones\n
 *		Stone Hill Consulting\n
 *		http://www.stonehill.com\n
 *
 * @par Environment
 *		Windows Embedded Standard 7
 *		Microsoft Visual Studio 2010
 *		Qt Framework 4.8.3
 *****************************************************************************/

#include "jaocontroller.h"

#if defined(Q_OS_WIN)	// Windows-specific code
#include <qt_windows.h>
#endif

#include <QCoreApplication>
#include <QKeyEvent>
#include <QSettings>

/// I/O port number to write hardware status
const quint16 OUTPUT1_PORT = 0x220;

/// I/O port number to write hardware status
const quint16 OUTPUT2_PORT = 0x221;
enum OUTPUT2 {
    SPK_OVR			= 0x80,	// override headphone jack sensing
    GPO2			= 0x40,	// output pins available from a box header.
    GPO1			= 0x20, // output pins available from a box header.
    INVERTER		= 0x10, // enables/disables the LCD display.
    IPS_ENABLE		= 0x08,	// enables/disables the Sonitor IPS module circuit
    IPS_MIC_MUTE	= 0x04, // sets the stereo microphone used for the Sonitor IPS and voice command/speakerphone mute on/off.
    REM_HANDSET_GPO1= 0x02, // these are connected to the remote device via the 26-pin MDR and can be used for custom functions.
    BAR_SCAN		= 0x01, // enables/disables the barcode scanner module.
};

/// I/O port number to write hardware status
const quint16 OUTPUT3_PORT = 0x222;
enum OUTPUT3 {
    BUZ_RING_SEL_B3	= 0x80, // the combined value (4 bit) of these bits selects the preprogrammed buzzer ring pattern.
    BUZ_RING_SEL_B2 = 0x40,
    BUZ_RING_SEL_B1 = 0x20,
    BUZ_RING_SEL_B0	= 0x10,
    LCD_BRIGHT_B3	= 0x08, // the combined value (4 bit) of these bits sets the brightness level of
    LCD_BRIGHT_B2	= 0x04, // the LCD display. 0x00 corresponds to the minimum brightness level supported by the display whereas
    LCD_BRIGHT_B1	= 0x02, // 0x0F corresponds to the maximum brightness. These signals control the LCD brightness either via analog
    LCD_BRIGHT_B0	= 0x01, // or PWM methods depending on the LCD display capabilities.
};

/// I/O port number to read membrane key status
const quint16 BUTTON_PORT = 0x223;

/// Membrane key status bits
enum BUTTONS {
    CH_PLUS		= 0x80,	// Set if Channel + is pressed
    CH_MINUS	= 0x40,	// Set if Channel - is pressed
    PLAY_PAUSE	= 0x20,	// Set if Play/Pause is pressed
    STOP		= 0x10,	// Set if Stop is pressed
    FORWARD		= 0x08,	// Set if Forward is pressed
    REWIND		= 0x04,	// Set if Rewind is pressed
    VOL_PLUS	= 0x02,	// Set if Volume + is pressed
    VOL_MINUS	= 0x01,	// Set if Volume - is pressed

    BUTTON_MASK	= 0xFF,	// We're interested in all buttons
};

/// I/O port number to read handset (and other) status
const quint16 HANDSET_PORT = 0x224;

/// Handset (and other) status bits
enum HANDSET {
    HOOK			= 0x80,	// Set when phone is on-hook
    REMOTE_HOOK		= 0x40,	// Set when remote phone is on-hook
    AUD_JACK_SENSE	= 0x20,	// Set when something plugged into audio out jack
    MIC_JACK_SENSE	= 0x10,	// Set when something plugged into microphone jack
    IPS_PRESENT		= 0x08,	// Set if Sonitor IPS module is present
    ATTN			= 0x04,	// Connected to JSMx ATTN pin, for external devices w/GPIO
    MEMBRANE_KEY	= 0x02,	// Set if membrane keypad is present
    POWER			= 0x01,	// Set if power button is pressed

    HANDSET_MASK	= 0x81,	// We're interested in the phone on/off hook and power button
};


/******************************************************************************/
/**
 * Constructor; initialize a new instance of the object
 *
 * @return
 *		None
 *
 ******************************************************************************/
JaoController::JaoController(QObject *parent)
    : QObject(parent)
#if defined(Q_OS_WIN)	// Windows-specific code
    , _winio("WinIo32")		// Assume 32-bit version for now
    , InitializeWinIo(NULL)
    , ShutdownWinIo(NULL)
    , GetPortVal(NULL)
    , SetPortVal(NULL)
#endif
    , _eventTarget(NULL) {	// No target object for events
    // Connect the timer to the polling routine.
    _timer.setInterval(20);
    _timer.setSingleShot(false);
    connect (&_timer, SIGNAL(timeout()), this, SLOT(PollHardware()));
}


/******************************************************************************/
/**
 * Destructor; clean up the object as it is going away
 *
 * @return
 *		None
 *
 ******************************************************************************/
JaoController::~JaoController() {
    _timer.stop();
#if defined(Q_OS_WIN)	// Windows-specific code
    if (ShutdownWinIo) {
        ShutdownWinIo();
        InitializeWinIo = NULL;
        ShutdownWinIo = NULL;
        GetPortVal = NULL;
        SetPortVal = NULL;
        _winio.unload();
    }
#endif
}


/******************************************************************************/
/**
 * Start polling for button events.  When events occur, they are posted to the
 * current event target.
 *
 * @return
 *		None
 *
 ******************************************************************************/
void JaoController::StartButtonPolling(void) {
    // Consult the application settings to see if we should enable or disable
    // the hardware polling (since it does direct access to hard-coded I/O ports,
    // we don't want to do it unless we're sure we're on the correct JaoTech
    // model).
    QSettings settings;
    int enable = settings.value("EnableHardwarePolling").toInt();

    /// @TODO	The value is 0 to disable polling, positive to enable, or
    ///			negative to automatically determine if we're running on the
    ///			appropriate hardware.  We're currently waiting for info from
    ///			Barco on how to determine that, so for now, "auto" also means
    ///			"disabled".
    if (enable > 0) {
#if defined(Q_OS_WIN)	// Windows-specific code
        if (LoadWinIo()) {
            // Establish the initial state of the buttons
            _buttons = 0;
            _handset = 0;

            if(_winio.isLoaded()) {
                // Start a timer to poll for changes in state.
                logger()->info("Hardware polling starting...");
                _timer.start();
            } else {
                logger()->error("Hardware polling canceled because WinIo is not loaded properly");
            }

        }
#endif
    }
}


/******************************************************************************/
/**
 * Initialize WinIo DLL.  Used for reading and writing I/O ports to access
 * GPIO on JaoTech terminal.
 *
 * @return
 *		None
 *
 ******************************************************************************/
bool JaoController::LoadWinIo(void) {
#if defined(Q_OS_WIN)	// Windows-specific code
    // Check if already loaded
    if (!_winio.isLoaded() || !InitializeWinIo || !ShutdownWinIo || !GetPortVal || !SetPortVal) {
        // Load the DLL
        if (!_winio.load())
            logger()->error("Unable to load %1: %2", _winio.fileName(), _winio.errorString());

        // Resolve the entry points we need to call
        else if (!(InitializeWinIo = (InitializeWinIo_t)_winio.resolve("InitializeWinIo")))
            logger()->error("Unable to resolve InitializeWinIo in %1: %2", _winio.fileName(), _winio.errorString());

        else if (!(ShutdownWinIo = (ShutdownWinIo_t)_winio.resolve("ShutdownWinIo")))
            logger()->error("Unable to resolve ShutdownWinIo in %1: %2", _winio.fileName(), _winio.errorString());

        else if (!(GetPortVal = (GetPortVal_t)_winio.resolve("GetPortVal")))
            logger()->error("Unable to resolve GetPortVal in %1: %2", _winio.fileName(), _winio.errorString());

        else if (!(SetPortVal = (SetPortVal_t)_winio.resolve("SetPortVal")))
            logger()->error("Unable to resolve SetPortVal in %1: %2", _winio.fileName(), _winio.errorString());

        // Initialize the WinIo library
        else if (!InitializeWinIo()) {
            logger()->error("Unable to Initialize WinIo: error=%1.  Unload now!", GetLastError());
            _winio.unload();
        }
    }

    // Return true if we resolved all of the entry points
    return InitializeWinIo && ShutdownWinIo && GetPortVal && SetPortVal;
#else
    return true;
#endif
}


/******************************************************************************/
/**
 * Poll the hardware to see if the button state has changed.  If so, post events
 * for the changed state to the specified target object.
 *
 * @return
 *		None
 *
 ******************************************************************************/
void JaoController::PollHardware() {
#if defined(Q_OS_WIN)	// Windows-specific code
    quint32 newButtons, newHandset;

    // Fetch the current state of the buttons and the handset
    if (!GetPortVal(BUTTON_PORT, &newButtons, 1)) {
        logger()->debug("*** Failed to read button port value (error=%1)",
                        GetLastError());
    } else if (!GetPortVal(HANDSET_PORT, &newHandset, 1)) {
        logger()->debug("*** Failed to read handset port value (error=%1)",
                        GetLastError());
    } else {
        // If any interesting bits have changed, post events to the target object
        newButtons &= BUTTON_MASK;
        newHandset &= HANDSET_MASK;

        // See if any buttons have changed state
        /// @TODO	The sample code from Barco de-bounced the state by reading it
        ///			three times in a row (sleeping 50ms in between), and AND-ing
        ///			the results.  Here I'm assuming that the hardware has
        ///			already de-bounced the switch status that's read in the GPIO
        ///			register.  If this turns out not to be the case, we'll need
        ///			to implement our own de-bounce logic.
        quint32 val = (newButtons ^ _buttons) & BUTTON_MASK;
        if (val) {
            logger()->trace("PollHardware, changed=%1 state=%2",
                            QString::number(val,16), QString::number(newButtons,16));

            // Post an event for each button which has changed state.  Note that
            // for channel plus/minus, we're posting the same event sent by the
            // "Ch+" and "Ch-" keys on the keyboard.
            if (val & CH_PLUS)
                PostEvent(Qt::Key_X, newButtons & CH_PLUS, Qt::ControlModifier);

            if (val & CH_MINUS)
                PostEvent(Qt::Key_C, newButtons & CH_MINUS, Qt::ControlModifier);

            if (val & PLAY_PAUSE)
                PostEvent(Qt::Key_MediaTogglePlayPause, newButtons & PLAY_PAUSE);

            if (val & STOP)
                PostEvent(Qt::Key_MediaStop, newButtons & STOP);

            if (val & FORWARD)
                PostEvent(Qt::Key_MediaNext, newButtons & FORWARD);

            if (val & REWIND)
                PostEvent(Qt::Key_MediaPrevious, newButtons & REWIND);

            if (val & VOL_PLUS)
                PostEvent(Qt::Key_VolumeUp, newButtons & VOL_PLUS);

            if (val & VOL_MINUS)
                PostEvent(Qt::Key_VolumeDown, newButtons & VOL_MINUS);

            // Save the new current state
            _buttons = newButtons;
        }

        // See if the handset has changed state
        /// @TODO	As with the membrane buttons, we're assuming the hardware
        ///			has de-bounced the hook switch in the GPIO register.  The
        ///			sample code from Barco did NOT de-bounce this either.  If
        ///			the switch status is not de-bounced in hardware, we'll
        ///			need to add our own de-bounce logic.
        val = (newHandset ^ _handset) & HANDSET_MASK;
        if (val) {
            logger()->trace("PollHardware, newHandset=0x%1", QString::number(newHandset, 16));

            // For now, we're using a key press event to indicate on-hook (hang
            // up) or off-hook (make or answer a call)
            if (val & HOOK) {
                if (newHandset & HOOK)
                    PostEvent(Qt::Key_Hangup, true);
                else
                    PostEvent(Qt::Key_Call, true);
            }

            // power button
            if (val & POWER) {
                if (newHandset & POWER)
                    PostEvent(Qt::Key_PowerOff, true);
            }

            // Save the new current state
            _handset = newHandset;
        }
    }
#endif // Q_OS_WIN
}


/******************************************************************************/
/**
 * Post a new key event to the target object
 *
 * @return
 *		None
 *
 ******************************************************************************/
void JaoController::PostEvent(int key, bool press, quint32 modifiers) {
    if (_eventTarget) {
        // Determine type of event (key press or release)
        QKeyEvent::Type type = press ? QEvent::KeyPress :QEvent::KeyRelease;

        // Create a new key event (it eventually gets deleted when it is processed)
        QKeyEvent *event = new QKeyEvent(type, key, (Qt::KeyboardModifier)modifiers);

        // Post the event to the target object (which might actually be running
        // in a different thread than us, so we rely on postEvent being thread-safe)
        QCoreApplication::postEvent(_eventTarget, event);
    }
    return;
}


/******************************************************************************/
/**
 * Get LCD screen status
 *
 * @return
 *		True if enabled
 *
 ******************************************************************************/
bool JaoController::GetScreenStatus() {
#if defined(Q_OS_WIN)	// Windows-specific code
    if (!_winio.isLoaded())
        return true;

    quint32 newOutput2;

    // Fetch the current state of the hardware output
    if (!GetPortVal(OUTPUT2_PORT, &newOutput2, 1)) {
        logger()->debug("*** Failed to read output2 port value (error=%1)", GetLastError());
        return true;
    }

    logger()->debug("GetScreenStatus enabled=%1", (newOutput2 & INVERTER)?1:0);

    if(newOutput2 & INVERTER)
        return true;
    else
        return false;
#endif
}

/******************************************************************************/
/**
 * Enable/Disable LCD screen
 *
 * @enabled
 *		True to enable LCD
 *
 * @return
 *		None
 *
 ******************************************************************************/
void JaoController::SetScreenStatus(bool enabled) {
#if defined(Q_OS_WIN)	// Windows-specific code
    if (!_winio.isLoaded())
        return;

    quint32 newOutput2;

    logger()->debug("SetScreenStatus enabled=%1", enabled?1:0);

    // Fetch the current state of the hardware output
    if (!GetPortVal(OUTPUT2_PORT, &newOutput2, 1)) {
        logger()->debug("*** Failed to read output2 port value (error=%1)", GetLastError());
        return;
    }

    // Set state
    if(enabled)
        newOutput2 = newOutput2 | INVERTER;
    else
        newOutput2 = newOutput2 ^ INVERTER;

    if (!SetPortVal(OUTPUT2_PORT, newOutput2, 1)) {
        logger()->debug("*** Failed to write output2 port value (error=%1)", GetLastError());
        return;
    }
#endif
}

/******************************************************************************/
/**
 * Get LCD screen brightness
 *
 * @return
 *		0-15. The terminal has 16 levels of brightness, 0 darkest and 15 brightest.
 *
 ******************************************************************************/
int JaoController::GetScreenBrightness() {
#if defined(Q_OS_WIN)	// Windows-specific code
    if (!_winio.isLoaded())
        return -1;

    quint32 newOutput3;

    // Fetch the current state of the hardware output
    if (!GetPortVal(OUTPUT3_PORT, &newOutput3, 1)) {
        logger()->debug("*** Failed to read output3 port value (error=%1)", GetLastError());
        return -1;
    }

    // Set state
    int val = newOutput3 & 0x0F;

    logger()->debug("GetScreenBrightness brightness=%1", val);

    return val;
#endif
}

/******************************************************************************/
/**
 * Set LCD screen brightness
 *
 * @bright
 *		0-15. The terminal has 16 levels of brightness, 0 darkest and 15 brightest.
 *
 * @return
 *		None
 *
 ******************************************************************************/
void JaoController::SetScreenBrightness(int bright) {
#if defined(Q_OS_WIN)	// Windows-specific code
    if (!_winio.isLoaded())
        return;

    logger()->debug("SetScreenBrightness brightness=%1", bright);

    int val = bright;
    if(val < 0)
        return;


    else if(val > 15)
        val = 15;

    quint32 newOutput3;

    // Fetch the current state of the hardware output
    if (!GetPortVal(OUTPUT3_PORT, &newOutput3, 1)) {
        logger()->debug("*** Failed to read output3 port value (error=%1)", GetLastError());
        return;
    }

    // Set state
    newOutput3 = (newOutput3 & 0xF0) | val;

    if (!SetPortVal(OUTPUT3_PORT, newOutput3, 1)) {
        logger()->debug("*** Failed to write output3 port value (error=%1)", GetLastError());
        return;
    }
#endif
}