/******************************************************************************/
/** @file
 *
 * @brief	Declarations for JaoTech controller
 *
 *		This file contains the declarations and definitions for the JaoTech
 *		controller.  This object is used to interface to the GPIO functions
 *		in the JaoTech terminal.
 *
 * @author
 *		Michael K. Jones\n
 *		Stone Hill Consulting\n
 *		http://www.stonehill.com\n
 *
 * @par Environment
 *		Windows Embedded Standard 7
 *		Microsoft Visual Studio 2010
 *		Qt Framework 4.8.3
 *****************************************************************************/

#ifndef JAOCONTROLLER_H
#define JAOCONTROLLER_H

#include <QObject>
#include <QLibrary>
#include <QTimer>
#include "logger.h"


#if defined(Q_OS_WIN)	// Windows-specific code
/// Define prototypes for dynamically linking to the WinIo DLL
typedef bool (__stdcall *InitializeWinIo_t)();
typedef bool (__stdcall *ShutdownWinIo_t)();
typedef bool (__stdcall *GetPortVal_t)(quint16 PortAddr, quint32* pPortVal, quint8 Size);
typedef bool (__stdcall *SetPortVal_t)(quint16 PortAddr, quint32 PortVal, quint8 Size);
#endif // Q_OS_WIN


/******************************************************************************/
/**
 * Class for accessing JaoTech-specific GPIO
 *
 ******************************************************************************/
class JaoController : public QObject {
    Q_OBJECT
    LOG4QT_DECLARE_QCLASS_LOGGER

    Q_PROPERTY(QObject * eventTarget READ eventTarget WRITE setEventTarget)

  public:
    JaoController(QObject *parent = NULL);
    ~JaoController();

    // Get/set the object pointer to send button events to
    QObject *eventTarget() const		{
        return _eventTarget;
    }
    void setEventTarget(QObject *obj)	{
        _eventTarget = obj;
    }

    // Public hardware control functions
  public:
    bool GetScreenStatus();
    void SetScreenStatus(bool enabled);
    int GetScreenBrightness();
    void SetScreenBrightness(int bright);
    //-------------
    // Public Slots
    //-------------
  public slots:

    void StartButtonPolling();


    //--------------
    // Private Slots
    //--------------
  private slots:
    void PollHardware();


    //-------------------------
    // Private member functions
    //-------------------------
  private:

    bool LoadWinIo(void);
    void PostEvent(int key, bool press, quint32 modifiers=0);


    //-------------------------
    // Private member variables
    //-------------------------
  private:

#if defined(Q_OS_WIN)	// Windows-specific code
    /// For dynamically loading the WinIO DLL to access the GPIO ports
    QLibrary			_winio;
    InitializeWinIo_t	InitializeWinIo;
    ShutdownWinIo_t		ShutdownWinIo;
    GetPortVal_t		GetPortVal;
    SetPortVal_t		SetPortVal;
#endif // Q_OS_WIN

    /// Object to send button events to
    QObject *_eventTarget;

    /// Current button state
    quint8 _buttons;

    /// Current state of handset
    quint8 _handset;

    /// Timer used to poll buttons for state changes
    QTimer _timer;
};

#endif // JAOCONTROLLER_H
