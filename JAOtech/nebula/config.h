/******************************************************************************/
/** @file 
 *
 * @brief	Definitions and declarations for Nebula configuration.
 *
 * @author
 *		Michael K. Jones\n
 *		Stone Hill Consulting\n
 *		http://www.stonehill.com\n
 *
 * @par Environment
 *		Windows Embedded Standard 7
 *		Microsoft Visual Studio 2010
 *		Qt Framework 4.8.3
 *****************************************************************************/
#ifndef CONFIG_H
#define CONFIG_H

#include <QString>


/******************************************************************************/
/**
 * Class which wraps application configuration information
 *
 ******************************************************************************/
class Config
{
public:
	// Return the initial URL to load
	static QString GetInitialUrl();
};

#endif // CONFIG_H