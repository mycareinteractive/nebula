#include <QNetworkReply>
#include <QtDebug>
#include <QSslError>
#include "nebulawebview.h"

NebulaWebView::NebulaWebView(QWidget *parent)
	: QWebView(parent)
{
	connect(page()->networkAccessManager(),
        SIGNAL(sslErrors(QNetworkReply*, const QList<QSslError> & )),
        this,
        SLOT(handleSslErrors(QNetworkReply*, const QList<QSslError> & )));
}

NebulaWebView::~NebulaWebView()
{

}

void NebulaWebView::handleSslErrors(QNetworkReply* reply, const QList<QSslError> &errors)
{
	logger()->debug("Handle SSL Errors: ");
    foreach (QSslError e, errors)
    {
		logger()->debug("    Error: %1", e.errorString());
    }
    reply->ignoreSslErrors();
}

