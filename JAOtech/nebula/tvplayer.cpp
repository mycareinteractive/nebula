#include <QMessageBox>
#include <QTextstream>
#include <QCoreApplication>
#include <QKeyEvent>
#include <QSettings>
#include <QApplication>
#include <QDesktopWidget>
#include "tvplayer.h"
#include "osdwidget.h"

TVPlayer::TVPlayer(QWidget *parent)
	: QFrame(parent)
, _pTunerFactory(NULL)
, _pTuner(NULL)
, _currCh(0)
, _eventTarget(NULL)
, _enabled(false)
, _rect(0,0,1,1)
{
	setFrameStyle(QFrame::NoFrame);
	QPalette plt = palette();
	plt.setColor( QPalette::Window, Qt::black);
    setPalette(plt);
	setAutoFillBackground(true);
	_pTunerFactory = TVTuner::TunerFactory::Get();
}

TVPlayer::~TVPlayer()
{
	if(_pTuner) {
		_pTuner->Stop(true);
		_pTuner->Terminate();
	}
}

bool TVPlayer::Init(QString player, QString device, QString options)
{
	try {
		_player = player;
		_device = device;
		_options = options;

		_pTuner = _pTunerFactory->CreateTuner(player.toStdString());
		if(!_pTuner)
		{
			logger()->error("Failed to creating TV tuner!");
			return false;
		}
		if(0 != _pTuner->Initialize(device.toStdString(), options.toStdString()))
		{
			logger()->error("Failed to initialize TV tuner!");
			return false;
		}
	}
	catch(...) {
		logger()->error("Unkown error when initializing TV tuner!");
		return false;
	}
	hide();
	return true;
}

void TVPlayer::Play(QString type, QString param, QString name)
{
	logger()->trace("Play %1, %2, %3", type, param, name);
	int channel;

	_pTuner->SetWindow((int)winId());
	QString mrl = FormatPlayerMrl(type, param);
	int ret = _pTuner->Play(mrl.toStdWString(),channel);
	if(0 != ret)
	{
		logger()->error("Failed to play TV tuner!");
		return;
	}
	
	channel = param.toInt();
	if(type=="Analog") {
		if(channel < TV_CH_ANALOG_MIN || channel > TV_CH_ANALOG_MAX) {
			return;
		}
		_currCh = channel;

		OSDWidget* osd = OSDWidget::GetInstance();
		if(osd) {
			osd->ShowText(param+" "+name, QPoint(20,20), 3000);
		}
	}
	else {
		OSDWidget* osd = OSDWidget::GetInstance();
		if(osd) {
			osd->ShowText(name, QPoint(20,20), 3000);
		}
	}
}

void TVPlayer::Stop(bool standby)
{
	logger()->trace("Stop standby=%1", standby);
	// Workaround for stupid USB tuner initialization delay.
	// When using VLC to play TV, we don't really stop the tuner.
	// Instead we tune to an invalid channel and hide the player.
	if(standby && _player == "VLC") {
		Play("Analog", QString::number(TV_CH_ANALOG_IDLE));
		SetTVLayerEnable(false);
		return;
	}

	if(_pTuner) {
		_pTuner->Stop(true);
	}
	logger()->trace("Stop completed");
}

QVariantMap TVPlayer::GetTVLayerRect()
{
	QString str = QString("GetTVLayerRect x:%1 y:%2 w:%3 h:%4").arg(QString::number( _rect.x()), QString::number( _rect.y()), QString::number(_rect.width()), QString::number(_rect.height()));
	logger()->trace(str);
	QVariantMap obj;
	obj.insert("x", _rect.x());
	obj.insert("y", _rect.y());
	obj.insert("width", _rect.width());
	obj.insert("height", _rect.height());
	return obj;
}

void TVPlayer::SetTVLayerRect(int x, int y, int w, int h)
{
	QString str = QString("SetTVLayerRect x:%1 y:%2 w:%3 h:%4").arg(QString::number(x), QString::number(y), QString::number(w), QString::number(h));
	logger()->trace(str);
	_rect = QRect(x, y, w, h);
	setGeometry(_rect);
}

bool TVPlayer::GetTVLayerEnable()
{
	logger()->trace("GetTVLayerEnabled enabled=%1", _enabled);
	return _enabled;
}

void TVPlayer::SetTVLayerEnable(bool enabled)
{
	logger()->trace("SetTVLayerEnable enabled=%1", enabled);
	_enabled = enabled;
	if(_enabled) {
		show();
		setGeometry(_rect);
	}
	else {
		setGeometry(0,0,1,1);
		hide();
	}
}

/// TODO: REMOVE ME, TEST function
bool TVPlayer::TestKey(int keyval)
{
	bool processed = false;

#if QT_DEBUG
	QSize fullscreen = QApplication::desktop()->size();
	static bool isFullScreen = false;
	
	switch(keyval)
	{
	case Qt::Key_F1:
		Play("Custom", "udp://@:1234");
		SetTVLayerRect(985, 21, 328, 185);
		SetTVLayerEnable(true);
		break;
	case Qt::Key_F2:
		Play("Analog", "3", "Test Channel");
		SetTVLayerRect(985, 21, 328, 185);
		SetTVLayerEnable(true);
		break;
	case Qt::Key_F3:
		Play("Custom", "http://localhost/video/Sesame_Street.mp4");
		SetTVLayerRect(985, 21, 328, 185);
		SetTVLayerEnable(true);
		break;
	case Qt::Key_F4:
		Stop(true);
		break;
	case Qt::Key_F10:
		SetTVLayerEnable(!GetTVLayerEnable());
		break;
	case Qt::Key_F11:
		isFullScreen = !isFullScreen;
		if(isFullScreen)
			SetTVLayerRect(0, 0, fullscreen.width(), fullscreen.height());
		else
			SetTVLayerRect(985, 21, 328, 185);
		break;
	case Qt::Key_F12:
		Stop(false);
		break;
	case Qt::Key_PageUp:
		_currCh++;
		if(_currCh > TV_CH_ANALOG_MAX)	_currCh = TV_CH_ANALOG_MIN;
		Play("Analog", QString::number(_currCh));
		break;
	case Qt::Key_PageDown:
		_currCh--;
		if(_currCh < TV_CH_ANALOG_MIN)	_currCh = TV_CH_ANALOG_MAX;
		Play("Analog", QString::number(_currCh));
		break;
	default:
		break;
	}
#endif

	return processed;
}

QString TVPlayer::FormatPlayerMrl(QString type, QString param)
{
	QString mrl = param;
	if(type == "Analog") {
		// analog TV
		if(_player == "VLC") {
			mrl = QString("dshow:// :dshow-tuner-channel=%1")\
				.arg(param);
		}
	}
	else if(type == "Digital") {
		// digital TV
	}
	else {
		// custom stuff
		mrl = param;
	}

	return mrl;
}

void TVPlayer::mousePressEvent(QMouseEvent* event)
{
	if (event->button() == Qt::LeftButton) {
        logger()->trace("Detect Mouse Press event, posting to Javascript");
		// Let Javascript decide what to do
		PostEvent(Qt::Key_Video, true);
    }
}

void TVPlayer::PostEvent(int key, bool press, quint32 modifiers)
{
	if (_eventTarget)
	{
		// Determine type of event (key press or release)
		QKeyEvent::Type type = press ? QEvent::KeyPress :QEvent::KeyRelease;

		// Create a new key event (it eventually gets deleted when it is processed)
		QKeyEvent *event = new QKeyEvent(type, key, (Qt::KeyboardModifier)modifiers);

		// Post the event to the target object (which might actually be running
		// in a different thread than us, so we rely on postEvent being thread-safe)
		QCoreApplication::postEvent(_eventTarget, event);
	}
	return;
}