/******************************************************************************/
/** @file
 *
 * @brief	Implementation for utility functions
 *
 * @author
 *		Michael K. Jones\n
 *		Stone Hill Consulting\n
 *		http://www.stonehill.com\n
 *
 * @par Environment
 *		Windows Embedded Standard 7
 *		Microsoft Visual Studio 2010
 *		Qt Framework 4.8.3
 *****************************************************************************/
#include <qt_windows.h>
#include <QCoreApplication>
#include "util.h"
#include "logger.h"

// Declare a static logger function to log messages for this module
LOG4QT_DECLARE_STATIC_LOGGER(PermitLogger, Permit)
LOG4QT_DECLARE_STATIC_LOGGER(GetVersionLogger, GetVersion)


/******************************************************************************/
/**
 * Add or remove the specified run-time permission permission.
 *
 * @return
 *		True for success, false if an error occurred
 *
 ******************************************************************************/
bool Permit(const wchar_t * privName, bool bEnabled)
{
	bool result = false;
	HANDLE hToken;
	TOKEN_PRIVILEGES tkp;

	// Open the current process token
	if (!OpenProcessToken(GetCurrentProcess(), TOKEN_ADJUST_PRIVILEGES|TOKEN_QUERY, &hToken))
		PermitLogger()->error("OpenProcessToken failed, error=%1", GetLastError());
	else
	{
		// Get the current attribute value for the specified privilege
		if (!LookupPrivilegeValue(NULL, privName, &tkp.Privileges[0].Luid))
			PermitLogger()->error("LookupPrivilegeValue %1 failed, error=%2", QString::fromWCharArray(privName), GetLastError());
		else
		{
			// Enable/disable the privilege, as requested
			tkp.PrivilegeCount = 1;
			tkp.Privileges[0].Attributes = bEnabled ? SE_PRIVILEGE_ENABLED : 0;

			// Adjust the priv in the token
			if (!AdjustTokenPrivileges(hToken, FALSE, &tkp, 0, (PTOKEN_PRIVILEGES)NULL, 0))
				PermitLogger()->error("AdjustTokenPrivileges %1 failed, error=%2", QString::fromWCharArray(privName), GetLastError());

			// Success!
			else
				result = true;
		}

		// Close the token
		CloseHandle(hToken);
	}
	return result;
}


/******************************************************************************/
/**
 * Get the version number of the current EXE file
 *
 * @return
 *		Version number, or empty string if it can't be read for any reason
 *
 ******************************************************************************/
QString GetAppVersion()
{
#pragma comment(lib, "version.lib")
	QString ver;

	// Get size of the version info
	DWORD dwHandle = 0;
	DWORD verSize = GetFileVersionInfoSize(QCoreApplication::applicationFilePath().utf16(), &dwHandle);
	if (verSize == 0)
	{
		GetVersionLogger()->error("GetFileVersionInfoSize failed, error %1", GetLastError());
	}
	else
	{
		// Allocate a buffer for the info, and read it in
		PBYTE buf = new BYTE[verSize];
		if (!GetFileVersionInfo(QCoreApplication::applicationFilePath().utf16(), NULL, verSize, buf))
		{
			GetVersionLogger()->error("GetFileVersionInfo failed, error %1", GetLastError());
		}
		else
		{
			// Query to get the fixed part of the version info
			VS_FIXEDFILEINFO *info = NULL;
			UINT len = 0;

			if (!VerQueryValue(buf, TEXT("\\"), (LPVOID *)&info, &len))
			{
				GetVersionLogger()->error("VerQueryValue failed, error %1", GetLastError());
			}
			else
			{
				// Defensive programming: make sure we got what we expected
				Q_ASSERT(info != NULL);
				Q_ASSERT(len = sizeof(*info));
				Q_ASSERT(info->dwSignature == VS_FFI_SIGNATURE);

				// Grab all 64-bits of the version info
				ver = QString::number(HIWORD(info->dwProductVersionMS));
				ver += '.';
				ver += QString::number(LOWORD(info->dwProductVersionMS));
				ver += '.';
				ver += QString::number(HIWORD(info->dwProductVersionLS));
				ver += '.';
				ver += QString::number(LOWORD(info->dwProductVersionLS));
			}
		}

		// Delete the buffer containing the version info
		delete [] buf;
	}
	return ver;
}


void NBSleep(int ms)
{
#ifdef Q_OS_WIN
    Sleep(uint(ms));
#else
    struct timespec ts = { ms / 1000, (ms % 1000) * 1000 * 1000 };
    nanosleep(&ts, NULL);
#endif
}

void NBReboot()
{
	if (Permit(SE_SHUTDOWN_NAME, true))
	{
		// Must have the required permission to restart; event log will say, "Application: Hung (Planned)"
		ExitWindowsEx(EWX_REBOOT | EWX_FORCEIFHUNG, SHTDN_REASON_MAJOR_APPLICATION | SHTDN_REASON_MINOR_HUNG | SHTDN_REASON_FLAG_PLANNED);
	}
}