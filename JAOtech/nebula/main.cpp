#include <QtGui/QApplication>
#include <QSettings>
#include <QDir>
#include <QDebug>
#include <QTcpServer>
#include <QMessageBox>
#include <QSplashScreen>

#include "MainWindow.h"
#include "util.h"
#include "logger.h"
#include "logmanager.h"
#include "ttcclayout.h"
#include "varia/debugappender.h"
#include "helpers/configuratorhelper.h"
#include "../version.info"


// Set to 1 to use the full-screen MainWindow class;
// Set to 0 to use the old Nebula window.
#define USE_MAIN_WINDOW	1

int main(int argc, char *argv[]) {
    // Set the following attributes to allow for persisting certain settings
    // (e.g. the web debugger settings).  Note: the settings will by default
    // be stored in the registry, in HKCU\Software\Aceso\Nebula
    QCoreApplication::setOrganizationName(QString::fromAscii("Aceso"));
    QCoreApplication::setApplicationName(QString::fromAscii("Nebula"));
    QCoreApplication::setApplicationVersion(QString::fromAscii(STRVERSION));
    QCoreApplication::setOrganizationDomain(QString::fromAscii("http://www.aceso.com/"));

    // This is the application object
    QApplication a(argc, argv);

    // Display a splash screen
    QPixmap pixmap(QString::fromUtf8(":/nebula/Resources/splash.jpg"));
    QSplashScreen splash(pixmap);
    splash.show();

    // Check if an instance of Nebula is already running.
    // We create a local tcp socket and listen on port 27275.
    // If the port is occupied then one instance is already running.
    QTcpServer server;
    for(int i=0; i<20; i++) {
        if(!server.listen(QHostAddress::Any, 27275)) {
            splash.showMessage("An instance on Nebula is still running\nattempt " + QString::number(i)
                               ,Qt::AlignBottom|Qt::AlignHCenter, Qt::white);
            a.processEvents();
            NBSleep(5000);
        } else {
            break;
        }
    }

    if(!server.isListening()) {
#if defined Q_OS_WIN
        QMessageBox::critical(NULL, QObject::tr("Oops!"), QObject::tr("Unable to start Nebula Framework!\nPress OK to reset"));
        NBReboot();
#endif
        return -1;
    }

    // Check if nebula.ini exists or not.  If not look for nebula.default.ini and copy it to nebula.ini
    // If neither exists or copy failed, by default QT will create a new nebula.ini file.
    QString iniFile = QCoreApplication::applicationDirPath() + "/conf/nebula.ini";
    QString iniDefaultFile = QCoreApplication::applicationDirPath() + "/conf/nebula.default.ini";
    if(!QFile::exists(iniFile)) {
        if(QFile::exists(iniDefaultFile)) {
            QFile::copy(iniDefaultFile, iniFile);
        }
    }

    // Change the default settings format to be an INI file, and the default path
    // for the INI file to the directory where the EXE resides.
    QCoreApplication::setOrganizationName(QString::fromAscii("conf"));	//override setting place
    QSettings::setDefaultFormat(QSettings::IniFormat);
    QSettings::setPath(QSettings::IniFormat, QSettings::UserScope, QCoreApplication::applicationDirPath());

    // Check to see if Log4Qt configuration exists, and if not, set up a default
    // configuration.  IMPORTANT: MUST DO THIS BEFORE CREATING OR ACCESSING A
    // LOGGER THE FIRST TIME!
    QSettings settings;
    QStringList keys = settings.childGroups();
    if (!keys.contains("Log4Qt", Qt::CaseInsensitive)) {
        qDebug() << "** Log4Qt settings not present; writing default settings";

        // Create Log4Qt subkey, and then the Properties sub-subkey
        settings.beginGroup("Log4Qt");
        settings.beginGroup("Properties");

        // Write the values to set up a rolling file appender
        settings.setValue("log4j.appender.File", "Log4Qt::RollingFileAppender");
        settings.setValue("log4j.appender.File.file", QCoreApplication::applicationDirPath() + "/logs/nebula.log");
        settings.setValue("log4j.appender.File.maxBackupIndex", "10");
        settings.setValue("log4j.appender.File.maxFileSize", "50000000");
        settings.setValue("log4j.appender.File.appendFile", "true");

        // Create an output formatter for the appender
        settings.setValue("log4j.appender.File.layout",	"Log4Qt::TTCCLayout");
        settings.setValue("log4j.appender.File.layout.DateFormat", "ISO8601");

        // Finally, configure the root logger to use the appender (and set the
        // output level to DEBUG_INT
        settings.setValue("log4j.rootLogger", "DEBUG, File");

        // End the groups (to hopefully force the values to be written out)
        settings.endGroup();	// Properties
        settings.endGroup();	// Log4Qt

        // Flush to disk
        settings.sync();
    }

    // Instantiate the "root" logger, which is the catch-all for all loggers.
    // When it is instantiated (actually, when the LogManager singleton is
    // instantiated), the root logger is configured by reading the properties
    // from the QSettings for the application (which is normally the registry).
    //
    // In debug builds, we will also add an appender to output ALL messages to
    // the debugger.
    // whole logging subsystem.
    Log4Qt::Logger *rootLogger = Log4Qt::LogManager::rootLogger();

    // Create the standard Time/Thread/Class/Context layout (since we're not
    // currently using multiple threads, turn off the thread display, just
    // to reduce the noise in the output).
    Log4Qt::TTCCLayout *p_layout = new Log4Qt::TTCCLayout(Log4Qt::TTCCLayout::ABSOLUTE);
    p_layout->setName(QLatin1String("rootLogger Debug Layout"));
    p_layout->setThreadPrinting(false);
    p_layout->activateOptions();

#if QT_DEBUG
    // Create an appender to write system debug message (i.e. OutputDebugString)
    Log4Qt::DebugAppender *p_appender = new Log4Qt::DebugAppender(p_layout);
    p_appender->setName(QLatin1String("rootLogger Debug Appender"));
    p_appender->setThreshold(Log4Qt::Level::ALL_INT);
    p_appender->activateOptions();

    // Set appender on root logger (so it will output to debug, as well as whatever
    // it was configured to do from the registry).  We also set the level to ALL,
    // which causes it to output everything.
    rootLogger->addAppender(p_appender);
    rootLogger->setLevel(Log4Qt::Level::ALL_INT);
    rootLogger->trace("Debug appender added");
#endif

    // Optional command-line argument is URL to load (overriding URL specified
    // by DHCP config)
    QStringList  args = a.arguments();

    // Get a logger (using the EXE name as the class)
    Log4Qt::Logger *logger = Log4Qt::Logger::logger(args[0]);

    // Check for configuration errors
    QList<Log4Qt::LoggingEvent> configErr = Log4Qt::ConfiguratorHelper::configureError();
    if (configErr.count() != 0)
        qDebug() << "*** Unable to load Log4Qt configuration:\n" << configErr;

    // Log a message that we've started up
    logger->info("============================================================================");

#if QT_DEBUG
    logger->info("Starting, version = %1 (Debug)", GetAppVersion());
#else
    logger->info("Starting, version = %1 (Release)", GetAppVersion());
#endif
    logger->info("Settings from %1", settings.fileName());

    logger->info("============================================================================");

#if 0
    QString url;
    if (args.count() > 1) {	// First arg is path to EXE
        // Second arg (if present) is URL to open initially
        url = args[1];
        logger->info("URL specified on command line: '%1'", url);
    }
#endif
    QString url;
    bool delay = true;
    args.removeFirst();
    foreach(QString arg, args) {
        if(arg.startsWith('-')) {
            if(arg.compare("-nodelay", Qt::CaseInsensitive)==0)
                delay = false;
        } else if(url.isEmpty()) {
            url = arg;
        }
    }

#if USE_MAIN_WINDOW
    // Instantiate the main window.  Parent is NULL since we're the top-level
    // window.  Also pass in the override URL (if specified on command line)
    //MainWindow w(NULL, url, delay);
    MainWindow w(NULL, url, delay);
#else
    nebula w; // MainWindow w;
#endif

    // Show the window
    w.show();

    // Splash will close once main window is shown
    splash.finish(&w);

    logger->info("****************************** To the Moon! *******************************");
    // Off to the races!
    int result = a.exec();

    // Log a message that we've ended
    logger->info("Exiting");

    // close the Tcp server
    server.close();

    return result;
}

