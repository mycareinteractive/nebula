#pragma once

#include <string>
#include <sstream>

#define TV_SUCCESS		0
#define TV_EGENERIC		-1


#define TV_CH_ANALOG_MIN	2
#define TV_CH_ANALOG_MAX	82
#define TV_CH_ANALOG_IDLE	83

#define SSTR( x ) dynamic_cast< std::ostringstream & >( \
            ( std::ostringstream() << std::dec << x ) ).str()

namespace TVTuner
{
	/// We should abstract it to a base class/interface.
	/// The idea is to expose interface to outside without knowing implementations.
	/// That way we can replace tuner relatively easier.
	class ITuner
	{
	public:
		virtual int Initialize(std::string device, std::string options)=0;
		virtual int Terminate()=0;

		virtual int GetChannelCount()=0;
		virtual std::wstring GetChannels()=0;
		virtual int SetChannels()=0;

		virtual int SetWindow(int handle)=0;
		virtual int GetWindow()=0;

		/// Every channel has a unique identifier and aach tuner manufacture uses different format.
		/// Uid is a string based identifier to map a channel from our database to the tuner channel ring.
		virtual int Play(std::wstring uid, int& idx)=0;

		/// Stop the channel but don't kill the tuner
		virtual int Stop(bool shutdownHardware)=0;

		virtual int SetMute(bool mute)=0;

		virtual int SetCC(bool cc)=0;
	};

	typedef ITuner* (__stdcall *CreateTunerFunc)(void); 

	/// The only entry to obtain a tuner is via TunerFactory
	class TunerFactory
	{
	private:
		TunerFactory();
		TunerFactory(const TunerFactory &) {}
		TunerFactory &operator=(const TunerFactory &) { return *this; }

	public:
		static TunerFactory *Get()
		{
			static TunerFactory instance;
			return &instance;
		}

		ITuner *CreateTuner(std::string player);
	};
}

