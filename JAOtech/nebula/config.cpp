/******************************************************************************/
/** @file
 *
 * @brief	Implementation for Nebula configuration.
 *
 * @author
 *		Michael K. Jones\n
 *		Stone Hill Consulting\n
 *		http://www.stonehill.com\n
 *
 * @par Environment
 *		Windows Embedded Standard 7
 *		Microsoft Visual Studio 2010
 *		Qt Framework 4.8.3
 *****************************************************************************/
#include <Qt>
#include "config.h"
#include "EONetworkDevice.h"

#include <QStringList>
#include <QSettings>
#include <QVariant>
#include <qt_windows.h>
#include "logger.h"

#if defined(Q_OS_WIN)	// Windows-specific code
#include <dhcpcsdk.h>		// Windows DHCP Client API
#pragma comment( lib, "dhcpcsvc.lib" )
#endif

// Declare a static logger function to log messages for this module
LOG4QT_DECLARE_STATIC_LOGGER(logger, Config)


/******************************************************************************/
/**
 * Return the initial URL to load
 *
 ******************************************************************************/
QString Config::GetInitialUrl() {
    QString url;

    // First, see if the initial URL is specified in the application settings
    QSettings s;
    if (s.contains("InitialUrl")) {
        url = s.value("InitialUrl").toString();
        logger()->info("InitialUrl from settings file: '%1'", url);
    }

#if defined(Q_OS_WIN)	// Windows-specific code to query DHCP server
    else {
        // Get the list of (connected) network adapter names.
        QStringList nicList = GetNetworkDeviceList();

        /// @TODO:	for now, we just use the first active NIC.  What should we do if
        //			there's more than one?  Or if none of the interfaces is active?
        QString nic;
        EONebulaNetworkDevice dev;
        foreach(nic, nicList) {
            if (dev.assignInterface(nic) && dev.isActive())
                break;
        }
        if (!dev.isValid()) {
            logger()->error("No valid, active interface was found");
        } else {
            logger()->debug("Found adapter %1, '%2'", nic, dev.getHumanReadableName());

            // The DHCP option we're requesting
            DHCPCAPI_PARAMS DhcpApiBootFileNameParams = {
                0,							// Flags
#if 0
                OPTION_NETBIOS_SCOPE_OPTION,// OptionId (FOR DEBUGGING ONLY!)
#else
                OPTION_BOOTFILE_NAME,		// OptionId
#endif
                FALSE,						// vendor specific?
                NULL,						// data filled in on return
                0							// nBytes
            };

            // Array of DHCP data the caller is interested in receiving.
            DHCPCAPI_PARAMS_ARRAY RequestParams = {
                1,						// only one option to request
                &DhcpApiBootFileNameParams
            };

            // Optional data to be requested, in addition to the data requested in the
            // RecdParams array. The SendParams parameter cannot contain any of the
            // standard options that the DHCP client sends by default.
            DHCPCAPI_PARAMS_ARRAY SendParams = {
                0,		// We're not requesting any additional parameters
                NULL
            };

            // We specify a user class ID so the DHCP server can provide a config
            // specifically for us (in case they use the Boot File Name option
            // for something else)
            const char USER_CLASS_ID[] = "Aceso Nebula Browser";
            const ULONG USER_CLASS_ID_SIZE = sizeof(USER_CLASS_ID);
            DHCPCAPI_CLASSID ClassId = {
                0,						// Flags (must be 0 currently)
                (LPBYTE) &USER_CLASS_ID,// Class ID
                USER_CLASS_ID_SIZE		// Size in bytes
            };

            // Buffer for receiving the data
            BYTE buf[1024];
            DWORD dwSize = sizeof(buf);
            DWORD dwError = DhcpRequestParams(
                                DHCPCAPI_REQUEST_SYNCHRONOUS,	// Flags
                                NULL,							// Reserved
                                (LPWSTR)nic.utf16(),			// Adapter Name
                                &ClassId,						// not using class id
                                SendParams,		                // sent parameters
                                RequestParams,					// requesting params
                                buf,							// buffer
                                &dwSize,						// buffer size
                                NULL							// Request ID
                            );

            if(ERROR_MORE_DATA == dwError) {
                logger()->error("DhcpRequestParams buffer is not large enough; %1 bytes requested, %2 bytes required",
                                sizeof(buf), dwSize);
            } else if(NO_ERROR == dwError) {
                // Check if the requested option was obtained.
                if(DhcpApiBootFileNameParams.nBytesData) {
                    // Return the string
                    url = QString::fromAscii((char *)DhcpApiBootFileNameParams.Data,
                                             DhcpApiBootFileNameParams.nBytesData);
                    logger()->info("Initial URL read from DHCP: '%1'", url);
                } else {
                    logger()->info("DhcpRequestParams request returned empty string; adapter='%1'",
                                   dev.getHumanReadableName());
                }
            } else {
                logger()->error("DhcpRequestParams failed; adapter='%1', error=%2",
                                dev.getHumanReadableName(), dwError);
            }
        }
    }
#endif // Q_OS_WIN

    return url;
}
