/******************************************************************************/
/** @file
 *
 * @brief	Declarations for main UI window
 *
 *		This file contains the declarations and definitions for the main UI
 *		window.  It's a full-screen browser window without any decoration -- no
 *		window frame, title bar, menu bar, status line, tool bar, etc. (just
 *		the browser pane).
 *
 * @author
 *		Michael K. Jones\n
 *		Stone Hill Consulting\n
 *		http://www.stonehill.com\n
 *
 * @par Environment
 *		Windows Embedded Standard 7
 *		Microsoft Visual Studio 2010
 *		Qt Framework 4.8.3
 *****************************************************************************/

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QWebView>
#include <QProgressDialog>
#include "eonebula.h"
#include "RtspClient.h"
#include "jaocontroller.h"
#include "logger.h"
#include "tvplayer.h"
#include "osdwidget.h"


/******************************************************************************/
/**
 * Class which represents the web page (inside the window).  We must implement
 * our own subclass so we can override one of its methods in order to capture
 * Javascript console messages.
 *
 ******************************************************************************/
class MainPage: public QWebPage {
    Q_OBJECT
    LOG4QT_DECLARE_QCLASS_LOGGER

  public:
    // Construction/destruction
    MainPage(QObject *parent=NULL);
    ~MainPage();

  protected:

    // Override so we can capture Javascript console messages
    void javaScriptConsoleMessage(const QString& message, int lineNumber, const QString& sourceID);

    // Override the user agent to prevent those annoying jReject browser detector
    QString userAgentForUrl(const QUrl & url) const;

  private:
    bool _enabled;
};


/******************************************************************************/
/**
 * Class which represents the main UI window.
 *
 ******************************************************************************/
class MainWindow : public QWebView {
    Q_OBJECT
    LOG4QT_DECLARE_QCLASS_LOGGER

  public:
    // Construction/destruction
    MainWindow(QWidget *parent=NULL, QString overrideUrl="", bool delayLoading=false);
    ~MainWindow();

  public:
    // Access functions to get sub-objects
    EONebula* getEONebula() {
        return &_eoNebula;
    }

    RtspClient* getRtspClient() {
        return &_rtspClient;
    }

    JaoController* getJaoController() {
        return &_hardware;
    }

    TVPlayer* getTVPlayer() {
        return &_tvplayer;
    }

    OSDWidget* getOSDWidget() {
        return &_osd;
    }

    // load main web application
    void loadMainApplication(bool bWait);

    // Load local html application
    void loadLocalApplication();

    // clean up application leftovers such as open players

    void resetApplication();

    // --------------------
    // Private Slot Methods
    // --------------------
  private slots:
    // Add Nebula object for JavaScript access
    void addJavascriptObjects();

    // Add the specified object for Javascript access
    void addJavaScriptObject(QString name, QObject *object);

    // Called for page load progress
    void onLoadStarted(void);
    void onLoadProgress(int progress);
    void onLoadFinished(bool ok);

    // Called to display text on the status bar
    void onStatusBarMessage(const QString &text);

    // Called when SSL error happens
    void handleSslErrors(QNetworkReply* reply, const QList<QSslError> &errors);

    // Handle a timeout of random wait before connecting to HTTP server
    void mainAppDelayTimeout();

    // Handle a timeout to retry connecting to main app
    void mainAppRetryTimeout();

    // --------------------------
    // Protected Member Functions
    // --------------------------
  protected:

    // Override the close event
    virtual void closeEvent(QCloseEvent *event);

    // Handle key press events
    virtual void keyPressEvent(QKeyEvent *event);

    // Handle right click menu
    virtual void contextMenuEvent(QContextMenuEvent *event);

    // Check for the "escape-to-Windows" key sequence
    //virtual bool checkEscapeToWindows(QKeyEvent *event);

    // Check for the "enable debugger window" key sequence
    //virtual bool checkEnableDebugger(QKeyEvent *event);

    // Check for key sequence
    virtual bool checkKeySequence(QKeyEvent *event, int& offset, const int sequence[], const int length, Qt::KeyboardModifier modifiers=Qt::AltModifier);

    // Check for all key sequence combinations
    virtual bool processKeySequences(QKeyEvent *event);

    // Process common button logic
    virtual bool processCommonKeys(QKeyEvent * event);

    // ------------------------
    // Private Member Variables
    // ------------------------
  private:
    /// Nebula API object (for providing local functionality to Javascript)
    EONebula _eoNebula;

    /// RTSP Client object (for providing functionality to Javascript)
    RtspClient _rtspClient;

    /// The main page object for the web view
    MainPage* _page;

    /// Page load progress dialog
    QProgressDialog *_progress;

    /// For polling JaoTech buttons
    JaoController _hardware;

    /// For Hauppauge TV tuner
    TVPlayer _tvplayer;

    /// For On-Screen-Display
    OSDWidget _osd;

    /// Main web app URL
    QUrl _mainAppUrl;

    /// Timer that delay random time to avoid rushing to server
    QTimer _mainAppDelayTimer;

    /// Timer that controls the main app retry if not connected
    QTimer _mainAppRetryTimer;

    /// Whether we are in local app
    bool _isLocalAppRunning;

};

#endif // MAINWINDOW_H
