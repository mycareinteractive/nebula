/******************************************************************************/
/** @file
 *
 * @brief	Declarations and definitions for various utility functions
 *
 * @author
 *		Michael K. Jones\n
 *		Stone Hill Consulting\n
 *		http://www.stonehill.com\n
 *
 * @par Environment
 *		Windows Embedded Standard 7
 *		Microsoft Visual Studio 2010
 *		Qt Framework 4.8.3
 *****************************************************************************/
#ifndef UTIL_H
#define UTIL_H


/// Add or remove the specified run-time permission permission.
bool Permit(const wchar_t * privName, bool bEnabled);

/// Get version number of current EXE file
QString GetAppVersion();


void NBSleep(int ms);

void NBReboot();

#endif UTIL_H
