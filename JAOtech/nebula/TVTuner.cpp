#include "TVTuner.h"

#include "..\VLCTuner\VLCTuner.h"

#ifdef TUNER_HAUPPAUGESDK
#include "..\HauppaugeTuner\HauppaugeTuner.h"
#endif

using namespace TVTuner;

TunerFactory::TunerFactory()
{
}

ITuner *TunerFactory::CreateTuner(std::string player)
{
	ITuner* pTuner = NULL;
	const char* name = player.c_str();

	if(_stricmp(name, "VLC") == 0) {
		pTuner = VLCTuner::Create();
	}
	else if(_stricmp(name, "HauppaugeSDK") == 0) {
#ifdef TUNER_HAUPPAUGESDK
		pTuner = HauppaugeTuner::Create();
#else
		pTuner = NULL;
#endif
	}
	else {
		pTuner = NULL;
	}

	return pTuner;
}