#ifndef NEBULAWEBVIEW_H
#define NEBULAWEBVIEW_H

#include <QWebView>
#include "logger.h"

class NebulaWebView : public QWebView
{
	Q_OBJECT
	LOG4QT_DECLARE_QCLASS_LOGGER

public:
	NebulaWebView(QWidget *parent);
	~NebulaWebView();

private slots:
    void handleSslErrors(QNetworkReply* reply, const QList<QSslError> &errors);

};

#endif // NEBULAWEBVIEW_H
