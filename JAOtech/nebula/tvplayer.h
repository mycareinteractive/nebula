#ifndef TVPLAYER_H
#define TVPLAYER_H

#include <QRect>
#include <QWidget>
#include <qFrame>
#include <QString>
#include "logger.h"
#include "TVTuner.h"

class TVPlayer : public QFrame
{
	Q_OBJECT
	LOG4QT_DECLARE_QCLASS_LOGGER

	Q_PROPERTY(QObject * eventTarget READ eventTarget WRITE setEventTarget)

public:
	QObject *eventTarget() const		{ return _eventTarget; }
	void setEventTarget(QObject *obj)	{ _eventTarget = obj; }

public:
	TVPlayer(QWidget *parent = 0);
	~TVPlayer();
	bool Init(QString player, QString device, QString options);
	bool TestKey(int keyval);

public:
	// functions exposed to javascript
	Q_INVOKABLE void	Play(QString type, QString param, QString name="");
	Q_INVOKABLE void	Stop(bool standby);
	Q_INVOKABLE QVariantMap	GetTVLayerRect();
	Q_INVOKABLE void	SetTVLayerRect(int x, int y, int w, int h);
	Q_INVOKABLE bool	GetTVLayerEnable();
	Q_INVOKABLE void	SetTVLayerEnable(bool enabled);
	
public:
	// QT overwritten functions
	virtual void mousePressEvent(QMouseEvent* event);

protected:
	QString FormatPlayerMrl(QString type, QString param);
	void PostEvent(int key, bool press, quint32 modifiers=0);

private:
	/// Object to send button events to
	QObject *_eventTarget;

private:
	TVTuner::TunerFactory* _pTunerFactory;
	TVTuner::ITuner* _pTuner;
	QString _player;
	QString _device;
	QString _options;
	int _currCh;

	bool _enabled;
	QRect _rect;
};

#endif // TVPLAYER_H
