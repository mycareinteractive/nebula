#ifndef OSDWIDGET_H
#define OSDWIDGET_H

#include <QWidget>
#include <QLabel>
#include <QString>
#include <QTimer>
#include "logger.h"

/**
 *	On Screen Display items.
 *  This layer allows us to display any non-web content such as volume bar, notification...etc.
 */
class OSDWidget :public QWidget
{
	Q_OBJECT
	LOG4QT_DECLARE_QCLASS_LOGGER 

public:
	OSDWidget(QWidget *parent = 0);
	~OSDWidget();

public:
	static OSDWidget* GetInstance();

	void ShowVolumeBar(int volume, int delay);
	void ShowText(QString text, QPoint point, int delay);

protected:
	void Init();
	void Clear();
	void AdjustLayer(int x, int y, int w, int h);

protected slots:
	void OSDTimeout();

private:
	static OSDWidget* _instance;

private:
	QLabel*	_volumeBg;
	QLabel*	_volumeBar;
	int		_volumeBarWidth;
	QLabel* _textLabel;
	QTimer* _timer;
};

#endif //OSDWIDGET_H

