#include "StdAfx.h"
#include <codecvt>
#include <sstream>
#include "VLCTuner.h"

using namespace TVTuner;

VLCTuner::VLCTuner(void)
	:vlcPlayer(NULL)
	,vlcObject(NULL)
	,_renderWindow(NULL)
{
	_mrl = "";
	_lastATrack = _lastVTrack = -1;
}


VLCTuner::~VLCTuner(void)
{
	Stop(true);
	Terminate();
	libvlc_release(vlcObject);
	vlcObject = NULL;
}

int VLCTuner::Initialize(std::string device, std::string options)
{
	/* Init libVLC */
	char const *vlc_argv[] =
	{
		"--no-ignore-config",	// Use VLC's config files
		"--no-video-title-show",	// Don't display title
		"--dshow-vdev", device.c_str()
	};
	int vlc_argc = sizeof(vlc_argv)/sizeof(*vlc_argv);
    if((vlcObject = libvlc_new(vlc_argc, vlc_argv)) == NULL) {
        //TODO: logging
        return TV_EGENERIC;
    }
	_device = device;
	_options = options;
	
	return TV_SUCCESS;
}

int VLCTuner::Terminate()
{
	if(vlcPlayer) {
        libvlc_media_player_stop(vlcPlayer);
        libvlc_media_player_release(vlcPlayer);
    }
    vlcPlayer = NULL;

	return TV_SUCCESS;
}

int VLCTuner::GetChannelCount()
{
	return 81;
}

std::wstring VLCTuner::GetChannels()
{
	return std::wstring(L"2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,64,65,66,67,68,69,70,71,72,73,74,75,76,77,78,79,80,81,82");
}

int VLCTuner::SetChannels()
{
	return TV_SUCCESS;
}


int VLCTuner::SetWindow(int handle)
{
	_renderWindow = (void*)handle;
	return TV_SUCCESS;
}

int VLCTuner::GetWindow()
{
	return (int)_renderWindow;
}

int VLCTuner::Play(std::wstring mrl, int& idx)
{
	/* New Media */
	std::wstring_convert<std::codecvt_utf8<wchar_t>> myconv;
	std::string mrlstr = myconv.to_bytes(mrl);
	
	std::stringstream ss(mrlstr);
    std::string option;

	libvlc_media_t *vlcMedia = NULL;

	int i_channel = 0;
	bool is_channel_change = false;
	char delim = ' ';
	while (std::getline(ss, option, delim)) {
        if(NULL == vlcMedia) {	// first part of string is MRL
			if(option == "dshow://") {
				sscanf_s(mrlstr.c_str(), "dshow:// :dshow-tuner-channel=%d", &i_channel);
			}
			if(option == _mrl && option == "dshow://") { // just channel change, not playing new content
				// we pad 0.5 channel just to prevent float number being unprecise, eg: 0.0399999999 instead of 0.04
				float f_channel = (float) ((i_channel + 0.5) / 100.0);
				if(i_channel >=TV_CH_ANALOG_MIN || i_channel <= TV_CH_ANALOG_MAX || i_channel == TV_CH_ANALOG_IDLE) {
					is_channel_change = true;
					libvlc_media_player_set_position(vlcPlayer, f_channel);
					break;
				}
			}
			_mrl = option;
			vlcMedia = libvlc_media_new_location(vlcObject,option.c_str());
			continue;
		}
		libvlc_media_add_option(vlcMedia, option.c_str());
    }

	if(!is_channel_change) {	// new media
		if( !vlcMedia ) {
			//TODO: logging
			return TV_EGENERIC;
		}

		/* Stop if something is playing */
		if( vlcPlayer && libvlc_media_player_is_playing(vlcPlayer) )
			Stop(true);
    
		vlcPlayer = libvlc_media_player_new_from_media (vlcMedia);
		libvlc_media_release(vlcMedia);

		/* Integrate the video in the interface */
#if defined(TARGET_OS_MAC)
		libvlc_media_player_set_nsobject(vlcPlayer, _renderWindow);
#elif defined(__linux__)
		libvlc_media_player_set_xwindow(vlcPlayer, _renderWindow);
#elif defined(WIN32) || defined(_WIN32) || defined(__WIN64)
		libvlc_media_player_set_hwnd(vlcPlayer, _renderWindow);
#endif

		/* And play */
		libvlc_media_player_play (vlcPlayer);
	}

	// disable mouse and keyboard
	libvlc_video_set_mouse_input(vlcPlayer, 0);
	libvlc_video_set_key_input(vlcPlayer, 0);

	if(i_channel == TV_CH_ANALOG_IDLE) {
		// hack. when playing the IDLE channel we disable audio and video to save system resource.
		_lastATrack = libvlc_audio_get_track(vlcPlayer);
		libvlc_audio_set_track(vlcPlayer, -1);
		_lastVTrack = libvlc_video_get_track(vlcPlayer);
		libvlc_video_set_track(vlcPlayer, -1);
		libvlc_audio_set_volume(vlcPlayer, 0);
	}
	else {
		libvlc_track_description_t* pAT = libvlc_audio_get_track_description(vlcPlayer);
		if(_lastATrack>-1)
			libvlc_audio_set_track(vlcPlayer, _lastATrack);
		if(_lastVTrack>-1)
			libvlc_video_set_track(vlcPlayer, _lastVTrack);
		libvlc_audio_set_volume(vlcPlayer, 100);
	}
	/*
	if(i_channel >=TV_CH_ANALOG_MIN || i_channel <= TV_CH_ANALOG_MAX) {
		libvlc_video_set_marquee_string(vlcPlayer, libvlc_marquee_Text, SSTR(i_channel).c_str());
		libvlc_video_set_marquee_int(vlcPlayer, libvlc_marquee_Color, 0x00FFFFFF);	//color
		libvlc_video_set_marquee_int(vlcPlayer, libvlc_marquee_Opacity, 255);	//opacity
		libvlc_video_set_marquee_int(vlcPlayer, libvlc_marquee_Size, 30);	//size
		libvlc_video_set_marquee_int(vlcPlayer, libvlc_marquee_Timeout,3000); //timeout
		libvlc_video_set_marquee_int(vlcPlayer, libvlc_marquee_X,20);  //x-coordinate
		libvlc_video_set_marquee_int(vlcPlayer, libvlc_marquee_Y,20);  //y-coordinate
		libvlc_video_set_marquee_int(vlcPlayer, libvlc_marquee_Enable, 1);
	}
	*/

	/* we don't track channel position */
	idx = 0;
	return TV_SUCCESS;
}

int VLCTuner::Stop(bool shutdownHardware)
{
	if(!vlcPlayer)
		return TV_EGENERIC;

	if(shutdownHardware) {
		if(vlcPlayer) {
			libvlc_media_player_stop(vlcPlayer);
			libvlc_media_player_release(vlcPlayer);
		}
		vlcPlayer = NULL;
	}
	else {
		if(vlcPlayer) {
			libvlc_media_player_stop(vlcPlayer);
		}
	}
	_lastATrack = _lastVTrack = -1;
	_mrl = "";
	return TV_SUCCESS;
}

int VLCTuner::SetMute(bool mute)
{
	if(vlcPlayer) {
		//libvlc_audio_set_mute(vlcPlayer, mute? 1 : 0);
		libvlc_audio_set_track(vlcPlayer, mute? -1 : 0);
	}
	return TV_SUCCESS;
}

int VLCTuner::SetCC(bool cc)
{
	if(vlcPlayer) {
		libvlc_video_set_spu(vlcPlayer, cc? 0 : -1);
	}
	return TV_SUCCESS;
}
