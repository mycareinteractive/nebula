#pragma once
#include <vlc\vlc.h>
#include "..\nebula\TVTuner.h"

namespace TVTuner
{
	class VLCTuner : public ITuner
	{
	public:
		VLCTuner(void);
		~VLCTuner(void);

		int Initialize(std::string device, std::string options);
		int Terminate();

		int GetChannelCount();
		std::wstring GetChannels();
		int SetChannels();

		int SetWindow(int handle);
		int GetWindow();

		int Play(std::wstring uid, int& idx);
		int Stop(bool shutdownHardware);

		int SetMute(bool mute);
		int SetCC(bool cc);

		static ITuner * __stdcall Create() { return new VLCTuner(); }

	private:
		void* _renderWindow;
		std::string _device;
		std::string _options;
		std::string _mrl;
		libvlc_instance_t *vlcObject;
        libvlc_media_player_t *vlcPlayer;
		int	_lastATrack;
		int _lastVTrack;
	};
}

