#ifndef USERBROWSER_H
#define USERBROWSER_H

#include <qt_windows.h>
#include <QtGui/QMainWindow>
#include <QLineEdit>
#include <QCloseEvent>
#include <QLocalSocket>
#include "ui_userbrowser.h"
#include "UserNetworkAccessManager.h"
#include "UserWebPage.h"

class QProgressBar;

class UserBrowser : public QMainWindow {
    Q_OBJECT

    // -----------------------
    // Public member functions
    // -----------------------
  public:
    // Construction/Destruction
    UserBrowser(QWidget *parent = 0, Qt::WFlags flags = 0);
    ~UserBrowser();

    // Load the specified URL in the web browser
    void load(const QUrl &url, bool isStartUrl = false);

    // Connect to event channel
    void connectToChannel(const QString &channelName);

    static UserBrowser* instance() {
        return _instance;
    }

    static LRESULT CALLBACK windowReturnCallback(INT code, WPARAM wParam, LPARAM lParam);

  protected:
    // Override the close event
    virtual void closeEvent(QCloseEvent *event);


  public slots:
    // Called when closing
    void lastWindowClosed();

    // -------------
    // Private slots
    // -------------
  private slots:
    // Called for page load progress
    void webView_loadStarted(void);
    void webView_loadProgress(int progress);
    void webView_loadFinished(bool ok);

    // Called to display text on the status bar
    void webView_statusBarMessage(const QString &text);

    // Called when the URL of the view changes
    void webView_urlChanged(const QUrl &url);

    // Called when the frame is laid out the first time.
    void webView_initialLayoutCompleted();

    // Called when the "Go" button is pressed
    void buttonGo_clicked();

    // Called when the "Reload/Stop" button is pressed
    void buttonReloadStop_clicked(bool reloading);

    // Called when the "Back" button is pressed
    void buttonBack_clicked();

    // Called when the "Forward" button is pressed
    void buttonForward_clicked();

    // Called when the "Exit" button is pressed
    void buttonExit_clicked();

    // Called when the "Home" button is pressed
    void buttonHome_clicked();

    // Called when connected to Nebula
    void channel_connected();

    // Called when received message from Nebula
    void channel_read();

    // Called when IPC channel has error
    void channel_error(QLocalSocket::LocalSocketError error);

    // Called when sending message to Nebula
    void channel_write(QString type, QString category, QString action, QString payload="");

    // ------------------------
    // Private member functions
    // ------------------------
  private:
    void init();


    // ------------------------
    // Private member variables
    // ------------------------
  private:
    Ui::UserBrowserClass ui;
    UserNetworkAccessManager *_accessManager;
    UserWebPage *_webPage;
    QProgressBar *_progress;
    QLocalSocket *_messageChannel;
    QString _channelName;
    HHOOK _hook;

    QUrl _startUrl;

    static UserBrowser* _instance;
};

#endif // USERBROWSER_H
