#pragma once
#include <QNetworkReply>

class UserNetworkReplyErrorImpl: public QNetworkReply {

    Q_OBJECT
  public:
    enum UserError {
        NoError = 0,
        ResourceNotAllowed,
        DomainNotAllowed
    };

  public:
    UserNetworkReplyErrorImpl(QObject *parent, const QNetworkRequest &req, const QNetworkAccessManager::Operation op, UserError code = NoError);
    ~UserNetworkReplyErrorImpl(void);

    virtual void abort();
    virtual void close();
    virtual qint64 bytesAvailable() const;
    virtual bool isSequential () const;
    virtual qint64 readData(char *data, qint64 maxlen);
};

