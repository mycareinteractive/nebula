#include <QSettings>
#include <QCoreApplication>
#include "userbrowser.h"
#include "UserWebPage.h"
#include "UserNetworkAccessManager.h"

UserWebPage::UserWebPage(QObject *parent)	///< Parent object (NULL for top-level window)
    : QWebPage(parent) {
    // set domain access manager
    this->setNetworkAccessManager(UserNetworkAccessManager::instance());
}


/******************************************************************************/
/**
 * Destructor; clean up when the window goes away.
 *
 * @return
 *		None
 *
 ******************************************************************************/
UserWebPage::~UserWebPage() {
    // Nothing to do (yet, anyway)
}

bool UserWebPage::extension(Extension extension, const ExtensionOption *option, ExtensionReturn * output) {
    if(!option || !output) {
        return false;
    }
    if(extension == QWebPage::ErrorPageExtension) {
        const ErrorPageExtensionOption *errOption =
            static_cast<const ErrorPageExtensionOption*>(option);
        QString errPage;
        errPage = "<html><body style=\"background:#e6e6e6; font-family:Arial; text-align:center\">";
        errPage += "<div style=\"margin:auto; padding:10px; max-width: 540px; min-width: 200px; background-color: #FBFBFB; border: 1px solid #AAA; border-bottom: 1px solid #888; border-radius: 3px; color: #000; box-shadow: 0px 2px 2px #AAA;\">";
        errPage += "<h3>This webpage is not available</h3><p>";
        errPage += errOption->url.toString();
        errPage += "</p><p>Error occurred in the ";
        switch (errOption->domain) {
        case QWebPage::QtNetwork:
            errPage += "Network error (";
            break;
        case QWebPage::Http:
            errPage += "Web Server error (";
            break;
        case QWebPage::WebKit:
            errPage += "Browser error (";
            break;
        default:
            errPage += "Error (";
        }
        errPage += QString::number(errOption->error);
        errPage += ")</p><p><br>Details: ";
        errPage += errOption->errorString;
        errPage += "</p></div></body></html>";
        ErrorPageExtensionReturn *errReturn =
            static_cast<ErrorPageExtensionReturn*>(output);
        errReturn->baseUrl = errOption->url;
        errReturn->content = errPage.toUtf8();
        // errReturn->contentType = "text/html"
        // errReturn->encoding = "UTF-8"; // these values are defaults
        return true;
    }
    return false;
}

bool UserWebPage::supportsExtension(Extension extension) const {
    if(extension == QWebPage::ErrorPageExtension) {
        return true;
    }
    return QWebPage::supportsExtension(extension);
}


void UserWebPage::javaScriptAlert(QWebFrame *frame, const QString &msg) {
    return;
}

bool UserWebPage::javaScriptConfirm(QWebFrame *frame, const QString &msg) {
    return QWebPage::javaScriptConfirm(frame, msg);
}

void UserWebPage::javaScriptConsoleMessage(
    const QString &message,		///< Message to display
    int lineNumber,				///< Line number in script
    const QString &sourceID) {	///< Source URL
    return;
}

QString UserWebPage::userAgentForUrl (const QUrl & url) const {
    QString str = QWebPage::userAgentForUrl(url);
    QString appVersion = QCoreApplication::applicationName() + QString("/") + QCoreApplication::applicationVersion();
    //str.replace(appVersion, QString("Chrome/13.0.766.0"));
    str.replace(appVersion, QString(""));
    return str;
}

QString UserWebPage::chooseFile(QWebFrame *parentFrame, const QString& suggestedFile) {
    return QString::null;
}
