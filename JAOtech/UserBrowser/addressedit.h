#pragma once

#include <QLineEdit>

class AddressEdit : public QLineEdit {
    Q_OBJECT
  public:
    AddressEdit(QWidget *parent);
    virtual ~AddressEdit() {}

  protected:
    void focusInEvent(QFocusEvent *e);
    void mousePressEvent(QMouseEvent *me);

    bool _selectOnMousePress;
};
