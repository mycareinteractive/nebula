#include "userbrowser.h"
#include <qt_windows.h>
#include <QtGui/QApplication>
#include <QDesktopWidget>
#include "../version.info"

int main(int argc, char *argv[]) {

    QCoreApplication::setOrganizationName(QString::fromAscii("Aceso"));
    QCoreApplication::setApplicationName(QString::fromAscii("UserBrowser"));
    QCoreApplication::setApplicationVersion(QString::fromAscii(STRVERSION));
    QCoreApplication::setOrganizationDomain(QString::fromAscii("http://www.aceso.com/"));

    QApplication a(argc, argv);

    // Process any command line arguments
    QString channelName = ""; // Event channel name
    QUrl	url;		// URL to load initially
    QPoint	pos(0,0);	// Position for window
    QSize	size(0,0);	// Size for window
    QStringList  argList = a.arguments();
    if (!argList.isEmpty()) {
        // Skip first arg (it is name of EXE)
        argList.removeFirst();
        foreach(QString arg, argList) {
            // Check for event channel name
            if(arg.startsWith("-channel=", Qt::CaseInsensitive)) {
                arg.remove(0,9);
                channelName = arg;
            }

            // Check for window position argument
            if (arg.startsWith("-pos=", Qt::CaseInsensitive)) {
                // Strip off the "-pos=" from front of string
                arg.remove(0,5);

                // Split the two values apart
                QStringList valList = arg.split(',');
                if (valList.size() == 2) {
                    // Convert to integer (invalid numbers will return 0, which
                    // will cause us to default that value)
                    pos.setX(valList[0].toInt());
                    pos.setY(valList[1].toInt());
                }
            }

            // Check for window size argument
            else if (arg.startsWith("-size=", Qt::CaseInsensitive)) {
                // Strip off the "-size=" from front of string
                arg.remove(0,6);

                // Split the two values apart
                QStringList valList = arg.split(',');
                if (valList.size() == 2) {
                    // Convert to integer (invalid numbers will return 0, which
                    // will cause us to default that value)
                    size.setWidth(valList[0].toInt());
                    size.setHeight(valList[1].toInt());
                }
            }

            // Any other non-switch argument is assumed to be the URL to
            // load.
            else if (!arg.startsWith('-')) {
                // Second arg (if present) is URL to open initially
                url.setUrl(arg);

                // If protocol not specified, assume http
                if (url.scheme().isEmpty())
                    url.setUrl("http://" + arg);
            }
        }
    }

    // Create the user browser window at the requested position,
    // with the requested size
    UserBrowser w(NULL,						// This is a top-level window
                  Qt::MSWindowsFixedSizeDialogHint // thin border turn off resize
                  | Qt::FramelessWindowHint	// Thin border; also disables resize
                  | Qt::CustomizeWindowHint			// Allows us to customize window decoration
                  //| Qt::Tool // no taskbar icon
#if !defined(QT_DEBUG)
                  |	Qt::WindowStaysOnTopHint			// Always keep the window on top
#endif
                 );

#if defined(Q_OS_WIN)	// Windows-specific code
    // In Qt, there is a way to remove the System Menu icon from the window,
    // but the user can still hit ALT+space to bring it up.  Unfortunately,
    // the ALT+space key does not show up in the keyPressEvent handler, so
    // it can't be blocked that way.  Since all we want to do is prevent
    // the user from moving or sizing the window, we'll just remove those
    // two items from the window's system menu.  Easy-peasy!
    HWND hWnd = (HWND)w.winId();
    if (hWnd) {
        // Retrieve the current window style, clear the system menu flag,
        // and set the new style.  Note that this also removes the title
        // bar and min/max/close buttons, which is fine for our main
        // window.
        ULONG style = ::GetWindowLong(hWnd, GWL_STYLE);
        style &= ~WS_SYSMENU;
        ::SetWindowLong(hWnd, GWL_STYLE, style);
    }
#endif

    // Somehow when we terminate() the process from Nebula the application
    // will not quit even after the UserBrowser window is destroyed.
    // Here is the work around by connect lastWindowClosed to a UserBrowser
    // and explicitly quit the application
    QObject::connect(&a, SIGNAL(lastWindowClosed()), &w, SLOT(lastWindowClosed()));

    // Default the size and position to fullscreen
    QRect r = QApplication::desktop()->screenGeometry();

    // If no pos/size supplied, use defaults
    if (!pos.x())		pos.setX(r.left());
    if (!pos.y())		pos.setY(r.top());
    if (!size.width())	size.setWidth(r.width());
    if (!size.height())	size.setHeight(r.height());

    // Calculate space required by window decoration
    QRect frame = w.frameGeometry();
    QRect client = w.geometry();
    int left = client.left() - frame.left();
    int top = client.top() - frame.top();
    int right = frame.right() - client.right();
    int bottom = frame.bottom() - client.bottom();

    // Adjust position and size to allow for window decoration
    pos.rx() -= left;
    pos.ry() += bottom;
    size.rheight() -= top + bottom*2;
    size.rwidth() -= left + right;

    // Move the window and set it's size
    w.move(pos.x(), pos.y());
    w.setFixedSize(size.width(), size.height());

    // Connect to event channel if needed
    if(!channelName.isEmpty())
        w.connectToChannel(channelName);

    // Load the initial URL
    w.load(url, true);

    // Show the window now
    w.show();

    // By default the Qt::Tool window will not exit when receiving WM_CLOSE.
    // Setting WA_QuitOnClose can correct this behavior.
    w.setAttribute(Qt::WA_QuitOnClose);

    // Start the main event loop
    a.exec();
}
