#pragma once
#include <QWebView>
class UserWebView :
    public QWebView {
  public:
    UserWebView(void);
    UserWebView(QWidget* parent);
    ~UserWebView(void);

  protected:
    // Handle right click menu
    virtual void contextMenuEvent(QContextMenuEvent *event);

    // Override new window creation
    QWebView* createWindow(QWebPage::WebWindowType type);
};

