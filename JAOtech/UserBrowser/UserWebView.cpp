#include "UserWebView.h"


UserWebView::UserWebView(void) {
}

UserWebView::UserWebView(QWidget* parent)
    :QWebView(parent) {
}


UserWebView::~UserWebView(void) {
}

void UserWebView::contextMenuEvent(QContextMenuEvent *event) {
    // do nothing
}

QWebView* UserWebView::createWindow(QWebPage::WebWindowType type) {
    if(type == QWebPage::WebWindowType::WebBrowserWindow) {
        return this;
    }
    return QWebView::createWindow(type);
}