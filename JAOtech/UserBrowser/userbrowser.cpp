/******************************************************************************/
/** @file
 *
 * @brief	Implementation of user browser window
 *
 * @par Environment
 *		Windows Embedded Standard 7
 *		Microsoft Visual Studio 2010
 *		Qt Framework 4.8.3
*****************************************************************************/
#include <QApplication>
#include <QWebView>
#include <QProgressBar>
#include <QFile>
#include <QMessageBox>
#include <QWebFrame>
#include <QResource>
#include "../commonlib/commonlib.h"
#include "userbrowser.h"

UserBrowser* UserBrowser::_instance = NULL;

/******************************************************************************/
/**
 * Constructor; initializes a new user browser window.
 *
 * @return
 *		None
 *
 ******************************************************************************/
UserBrowser::UserBrowser(
    QWidget *	parent,		///< Parent window (NULL if this is top level)
    Qt::WFlags	flags)		///< Flags for various window properties & hints
    : QMainWindow(parent, flags)
    , _progress(NULL)
    , _accessManager(NULL)
    , _webPage(NULL)
    , _hook(NULL) {

    _instance = this;
    // Set up the UI
    ui.setupUi(this);
    init();
    return;
}


/******************************************************************************/
/**
 * Destructor; clean up when the window goes away.
 *
 * @return
 *		None
 *
 ******************************************************************************/
UserBrowser::~UserBrowser() {
    if(_progress)
        delete _progress;
    if(_webPage)
        delete _webPage;
    if(_accessManager)
        delete _accessManager;
    if(_hook) {
        ::UnhookWindowsHookEx(_hook);
        _hook = NULL;
    }
    return;
}

/******************************************************************************/
/**
 * Initialize components and signal/slots
 *
 * @return
 *		None
 *
 ******************************************************************************/
void UserBrowser::init() {
    // Initialize domain access manager
    _accessManager = new UserNetworkAccessManager();
    _accessManager->setIsWhiteList(false);
    _accessManager->addDomain("*.getwellnetwork.com");
    _accessManager->addDomain("*.redtube.com");
    _accessManager->addDomain("*.comcast.com");
    _accessManager->addDomain("*.comcast.net");

    _webPage = new UserWebPage(this); // access manager is automatically added in UserWebPage constructor
    ui.webView->setPage(_webPage);

    // Web settings
    QWebSettings *settings = QWebSettings::globalSettings();

    settings->setAttribute(QWebSettings::PluginsEnabled, true); // Enable plugins
    settings->setAttribute(QWebSettings::DnsPrefetchEnabled, true); // Enable DNS speed up
    settings->setAttribute(QWebSettings::LocalContentCanAccessFileUrls, false); // Disable local file access
    settings->setAttribute(QWebSettings::JavascriptCanOpenWindows, true); // JS can open window

    // Inject CSS to change global web page scrollbar
    QResource css(":/userbrowser/Resources/useragent.css");
    QByteArray cssBytes(reinterpret_cast<const char*>(css.data()), css.size());
    QString cssBase64(cssBytes.toBase64());
    settings->setUserStyleSheetUrl(QUrl("data:text/css;charset=utf-8;base64," + cssBase64));

    // Create a progress bar, and add it to the status bar area (used for
    // displaying page load progress)
    _progress = new QProgressBar(ui.statusBar);
    _progress->setRange(0, 100);
    _progress->setValue(0);
    _progress->setOrientation(Qt::Horizontal);
    _progress->setAlignment(Qt::AlignCenter);
    _progress->setMaximumWidth(300);
    ui.statusBar->addPermanentWidget(_progress, 1);
    _progress->hide();

    // Connect page load signals
    connect(ui.webView->page()->mainFrame(), SIGNAL(loadStarted()), this, SLOT(webView_loadStarted()));
    connect(ui.webView,	SIGNAL(loadProgress(int)), this, SLOT(webView_loadProgress(int)));
    connect(ui.webView->page()->mainFrame(), SIGNAL(loadFinished(bool)), this, SLOT(webView_loadFinished(bool)));
    connect(ui.webView->page()->mainFrame(), SIGNAL(initialLayoutCompleted()), this, SLOT(webView_initialLayoutCompleted()));

    // Connect signal to display text on the status bar
    connect(ui.webView,	SIGNAL(statusBarMessage(const QString &)), this, SLOT(webView_statusBarMessage(const QString &)));

    // When user hovers over a link, show its URL on the status bar
    connect(ui.webView->page(),	SIGNAL(linkHovered(const QString &, const QString &, const QString &)),	this, SLOT(webView_statusBarMessage(const QString &)));

    // When the URL changes (e.g. when user clicks on a link, or hits Back or
    // Forward button), update the address line
    connect(ui.webView,	SIGNAL(urlChanged(const QUrl &)), this, SLOT(webView_urlChanged(const QUrl &)));

    // toolbar signals
    connect(ui.buttonBack, SIGNAL(clicked()), this, SLOT(buttonBack_clicked()));
    connect(ui.buttonForward, SIGNAL(clicked()), this, SLOT(buttonForward_clicked()));
    connect(ui.buttonReloadStop, SIGNAL(clicked(bool)),	this, SLOT(buttonReloadStop_clicked(bool)));
    connect(ui.buttonGo, SIGNAL(clicked()),	this, SLOT(buttonGo_clicked()));
    connect(ui.lineAddress, SIGNAL(returnPressed()), this, SLOT(buttonGo_clicked()));
    connect(ui.buttonExit, SIGNAL(clicked()), this, SLOT(buttonExit_clicked()));
    connect(ui.buttonHome, SIGNAL(clicked()), this, SLOT(buttonHome_clicked()));

    // Connect event channel signals for communication between Nebula and User Browser
    _messageChannel = new QLocalSocket(this);
    connect(_messageChannel, SIGNAL(connected()), this, SLOT(channel_connected()));
    connect(_messageChannel, SIGNAL(readyRead()), this, SLOT(channel_read()));
    connect(_messageChannel, SIGNAL(error(QLocalSocket::LocalSocketError)), this, SLOT(channel_error(QLocalSocket::LocalSocketError)));

    // Setup a windows hook to get new top-level window creation event
#if !defined(QT_DEBUG)
    _hook = ::SetWindowsHookEx(WH_SHELL, windowReturnCallback, NULL, ::GetCurrentThreadId());
    if(_hook == NULL) {
        // TODO log error or do something.
    }
#endif
}

/************************************************************************/
/* A windows hook callback to handle Adobe Flash full screen popup
/************************************************************************/
LRESULT CALLBACK UserBrowser::windowReturnCallback(INT code, WPARAM wParam, LPARAM lParam) {
    // We only care about window created event
    if (code == HSHELL_WINDOWCREATED) {
        HWND handle = (HWND)wParam;
        WCHAR buffer[256];
        // Get window name see if it's "Adobe Flash Player"
        if(::GetWindowText(handle, buffer, sizeof(buffer)/sizeof(WCHAR))) {
            QString name = QString::fromWCharArray(buffer);
            if(name.contains("Adobe Flash")) {
                ::SetWindowPos(handle, HWND_TOPMOST, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE);
            }
        }
    }
    return ::CallNextHookEx(NULL, code, wParam, lParam);
}

void UserBrowser::connectToChannel(const QString &channelName) {
    // Connect to the event channel
    _channelName = channelName;
    _messageChannel->abort();
    _messageChannel->connectToServer(channelName);
    ui.statusBar->showMessage(tr("Connecting to Nebula on channel ") + _messageChannel->serverName());
}

/******************************************************************************/
/**
* This event handler is called with the given event when Qt receives a window
* close request for a top-level widget from the window system.
*
******************************************************************************/
void UserBrowser::closeEvent(QCloseEvent *event) {
    ui.webView->stop();

    delete _progress;
    _progress = NULL;

    delete _webPage;
    _webPage = NULL;

    delete _accessManager;
    _accessManager = NULL;
    event->accept();
}

void UserBrowser::lastWindowClosed () {
    QApplication::exit();
}

/******************************************************************************/
/**
* Called to indicate loading of a new page has started
*
* @return
*		None
*
******************************************************************************/
void UserBrowser::webView_loadStarted(void) {
    // clear webkit page cache
    ui.webView->settings()->clearMemoryCaches();

    // Check the "reload/stop" button so it shows a stop icon
    ui.buttonReloadStop->setChecked(true);

    // Reset the progress bar, and make sure it is showing
    _progress->reset();
    _progress->show();

    QUrl url = ui.webView->url();
//     // Send notification back to Nebula
//     QUrl url = ui.webView->page()->mainFrame()->url();
//     channel_write(NBIPC_TYPE_NOTIFICATION, NBIPC_CATEGORY_BROWSER, NBIPC_ACTION_OPENING, url.toString());
}



/******************************************************************************/
/**
* Called to indicate progress loading a page
*
* @return
*		None
*
******************************************************************************/
void UserBrowser::webView_loadProgress(int progress) {	///< Percentage of page loaded (0-100)
    // Set the current progress
    if (_progress)
        _progress->setValue(progress);
}


/******************************************************************************/
/**
* Called to indicate page load has completed
*
* @return
*		None
*
******************************************************************************/
void UserBrowser::webView_loadFinished(bool ok) {	///< True for success, false if an error occurred
    // Hide the progress bar
    _progress->hide();

    // Uncheck the "reload/stop" button so it shows a reload icon
    ui.buttonReloadStop->setChecked(false);
}

void UserBrowser::webView_initialLayoutCompleted() {
    // When this signal happens, the entire page might be still loading but the basic layout is available.
    // We take this as the user has visited one valid URL and we will trigger event back to Nebula.
    QUrl url = ui.webView->page()->mainFrame()->url();
    channel_write(NBIPC_TYPE_NOTIFICATION, NBIPC_CATEGORY_BROWSER, NBIPC_ACTION_OPENED, url.toString());
}


/******************************************************************************/
/**
* Called to display text on the status bar
*
* @return
*		None
*
******************************************************************************/
void UserBrowser::webView_statusBarMessage(const QString &text) {
    // Send the string to the status bar (as a temporary message)
    ui.statusBar->showMessage(text);
}

void UserBrowser::load(const QUrl &url, bool isStartUrl /* = false */) {
    if(!url.isEmpty()) {
        if(isStartUrl) {
            _startUrl = url;
        }
        ui.lineAddress->setText(url.toString());
        ui.webView->load(url);
    }
    return;
}

void UserBrowser::buttonGo_clicked() {
    QString text = ui.lineAddress->text();

    // We try best to guess the URL.
    QUrl url = QUrl::fromUserInput(text);

    // If it's not valid we search on Google
    if(!url.isValid() || !url.host().contains(".")) {
        text = text.replace(QRegExp("\\s"),"+");
        url = QUrl::fromUserInput("http://www.google.com/search?q=" + text);
    }

    ui.webView->load(url);
    ui.webView->setFocus();
    return;
}

void UserBrowser::buttonExit_clicked() {
    close();
}

void UserBrowser::webView_urlChanged(const QUrl &url) {
    ui.lineAddress->setText(url.toString());
}

void UserBrowser::buttonBack_clicked() {
    ui.webView->back();
}

void UserBrowser::buttonForward_clicked() {
    ui.webView->forward();
}

void UserBrowser::buttonReloadStop_clicked(bool reloading) {
    if(reloading) {
        ui.webView->reload();
    } else {
        ui.webView->stop();
    }
}

void UserBrowser::buttonHome_clicked() {
    if(!_startUrl.isEmpty()) {
        load(_startUrl);
    }
}

void UserBrowser::channel_connected() {
    ui.statusBar->showMessage(tr("Message channel established with Nebula."));
}

void UserBrowser::channel_read() {
    if(!_messageChannel) {
        return;
    }

    QDataStream in(_messageChannel);
    in.setVersion(QDataStream::Qt_4_8);

    if (_messageChannel->bytesAvailable() < 2) {
        ui.statusBar->showMessage(tr("Invalid message received from Nebula"));
        return;
    }

    QString jsonString;
    in >> jsonString;

    QMap<QString, QVariant> message = Json::decode(jsonString);
    if(message.isEmpty() || message[NBIPC_CATEGORY] != "browser") {
        ui.statusBar->showMessage(tr("Ignored message received from Nebula"));
        return;
    }

    if(message[NBIPC_TYPE] == NBIPC_TYPE_COMMAND) {
        ui.statusBar->showMessage(tr("Command received from Nebula:") + message[NBIPC_ACTION].toString());
        if(message[NBIPC_ACTION] == NBIPC_ACTION_EXIT) {
            ui.statusBar->showMessage(tr("Exit request from Nebula!!"));
            qApp->exit();
        }
    }
}

void UserBrowser::channel_write(QString type, QString category, QString action, QString payload/* ="" */) {
    if(!_messageChannel || !_messageChannel->isOpen()) {
        return;
    }

    QMap<QString, QVariant> message;
    message[NBIPC_TYPE] = type;
    message[NBIPC_CATEGORY] = category;
    message[NBIPC_ACTION] = action;
    if(!payload.isEmpty())
        message[NBIPC_PAYLOAD] = payload;

    QByteArray block;
    QDataStream out(&block, QIODevice::WriteOnly);
    out.setVersion(QDataStream::Qt_4_8);
    out << Json::encode(message);
    _messageChannel->write(block);
}

void UserBrowser::channel_error(QLocalSocket::LocalSocketError error) {
    QMessageBox::warning(NULL, "oops", tr("Failed to connect to Nebula channel ") + _channelName + " \n" + _messageChannel->errorString());
}

