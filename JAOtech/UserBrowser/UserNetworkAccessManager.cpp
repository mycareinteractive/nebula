#include <QNetworkRequest>
#include "UserNetworkReplyErrorImpl.h"
#include "UserNetworkAccessManager.h"

UserNetworkAccessManager* UserNetworkAccessManager::_instance = NULL;
UserNetworkAccessManager* UserNetworkAccessManager::instance() {
    return _instance;
}

UserNetworkAccessManager::UserNetworkAccessManager( QObject *parent ) :
    QNetworkAccessManager(parent) {
    _isWhiteList = false;
    _instance = this;

    connect(this, SIGNAL(sslErrors(QNetworkReply*, const QList<QSslError> & )),
            this, SLOT(handleSslErrors(QNetworkReply*, const QList<QSslError> & )));
}

UserNetworkAccessManager::~UserNetworkAccessManager() {
    removeAllDomains();
}

void UserNetworkAccessManager::addDomain(const QString &domain) {
    _domainList.insert(domain, 1);
}

void UserNetworkAccessManager::removeDomain( const QString &domain ) {
    _domainList.remove(domain);
}

void UserNetworkAccessManager::removeAllDomains() {
    _domainList.clear();
}

bool UserNetworkAccessManager::isAllowed( const QString &domain ) {
    // If the domain is in the list, it should be either
    // allowed if we are operating in whitelist mode;
    // or
    // blocked if we are operating in blacklist mode.

    QMap<QString, int>::iterator i;
    for (i = _domainList.begin(); i != _domainList.end(); ++i) {
        if(isMatchingHostname(i.key(), domain)) {
            return isWhiteList()? true: false;
        }
    }
    return isWhiteList()? false: true;
}

QNetworkReply* UserNetworkAccessManager::createRequest(Operation op, const QNetworkRequest &req, QIODevice *outgoingData) {
    QNetworkRequest myReq( req );

    // For now we only accept http, https and data (base64) protocols
    if ( req.url().scheme()!= "http" && req.url().scheme()!= "https" && req.url().scheme()!= "data") {
        return new UserNetworkReplyErrorImpl(this, req, op, UserNetworkReplyErrorImpl::ResourceNotAllowed);
    }

    // Check if the domain is allowed.
    // If we are using white list, it needs to be in it;
    // If we are using black list, it must not be in it.
    if ( !isAllowed( req.url().host() ) ) {
        return new UserNetworkReplyErrorImpl(this, req, op, UserNetworkReplyErrorImpl::DomainNotAllowed);
    }

    QNetworkReply *reply = QNetworkAccessManager::createRequest( op, myReq, outgoingData );

    return reply;
}


// Here we borrowed the SSL wildcard host matching logic to perform the wildcard based hostname checking
bool UserNetworkAccessManager::isMatchingHostname(const QString &cn, const QString &hostname) {
    int wildcard = cn.indexOf(QLatin1Char('*'));

    // Check this is a wildcard cert, if not then just compare the strings
    if (wildcard < 0)
        return cn == hostname;

    int firstCnDot = cn.indexOf(QLatin1Char('.'));
    int secondCnDot = cn.indexOf(QLatin1Char('.'), firstCnDot+1);

    // Check at least 3 components
    if ((-1 == secondCnDot) || (secondCnDot+1 >= cn.length()))
        return false;

    // Check * is last character of 1st component (ie. there's a following .)
    if (wildcard+1 != firstCnDot)
        return false;

    // Check only one star
    if (cn.lastIndexOf(QLatin1Char('*')) != wildcard)
        return false;

    // Check characters preceding * (if any) match
    if (wildcard && (hostname.leftRef(wildcard) != cn.leftRef(wildcard)))
        return false;

    // Check characters following first . match
    if (hostname.midRef(hostname.indexOf(QLatin1Char('.'))) != cn.midRef(firstCnDot))
        return false;

    // Ok, I guess this was a wildcard CN and the hostname matches.
    return true;
}

void UserNetworkAccessManager::handleSslErrors(QNetworkReply* reply, const QList<QSslError> &errors) {
    // Workaround to solve the stupid kp.org SSL/DNS issue inside kp northwest building
    reply->ignoreSslErrors();
}