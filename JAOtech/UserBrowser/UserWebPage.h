#pragma once
#include <QWebPage>

/******************************************************************************/
/**
 * Class which represents the web page (inside the window).  We must implement
 * our own subclass so we can override one of its methods in order to capture
 * Javascript console messages.
 *
 ******************************************************************************/
class UserWebPage: public QWebPage {
    Q_OBJECT

  public:
    // Construction/destruction
    UserWebPage(QObject *parent=NULL);
    ~UserWebPage();

    bool extension(Extension extension, const ExtensionOption *option, ExtensionReturn * output);
    bool supportsExtension(Extension extension) const;

  protected:
    // Override javascript popups
    bool javaScriptConfirm(QWebFrame *frame, const QString &msg);
    void javaScriptAlert(QWebFrame *frame, const QString &msg);
    void javaScriptConsoleMessage(const QString& message, int lineNumber, const QString& sourceID);

    // Override the user agent to prevent those annoying jReject browser detector
    QString userAgentForUrl(const QUrl & url) const;

    // Override the file picker dialog behavior so patient can't hack into file system
    QString chooseFile(QWebFrame *parentFrame, const QString& suggestedFile);
};

