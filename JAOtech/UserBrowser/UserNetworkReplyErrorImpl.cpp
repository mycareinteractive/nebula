#include "UserNetworkReplyErrorImpl.h"


UserNetworkReplyErrorImpl::UserNetworkReplyErrorImpl(QObject *parent, const QNetworkRequest &req, const QNetworkAccessManager::Operation op, UserError code)
    : QNetworkReply(parent) {

    setRequest(req);
    setUrl(req.url());
    setOperation(op);
    setFinished(true);
    QNetworkReply::open(QIODevice::ReadOnly);

    QString msg = "";
    switch(code) {
    case ResourceNotAllowed:
        msg = "Requested resource is not allowed";
        break;
    case DomainNotAllowed:
        msg = "This webpage is not allowed";
        break;
    default:
        msg = "Network error";
        break;
    }

    setError(QNetworkReply::ContentAccessDenied, msg);
    QMetaObject::invokeMethod(this, "error", Qt::QueuedConnection,
                              Q_ARG(QNetworkReply::NetworkError, QNetworkReply::ContentAccessDenied));
    QMetaObject::invokeMethod(this, "finished", Qt::QueuedConnection);
    return;

}


UserNetworkReplyErrorImpl::~UserNetworkReplyErrorImpl(void) {
}

void UserNetworkReplyErrorImpl::abort() {
}

void UserNetworkReplyErrorImpl::close() {
}

qint64 UserNetworkReplyErrorImpl::bytesAvailable() const {
    return 0;
}

bool UserNetworkReplyErrorImpl::isSequential () const {
    return true;
}

qint64 UserNetworkReplyErrorImpl::readData(char *data, qint64 maxlen) {
    return true;
}