#pragma once
#include <QString>
#include <QMap>
#include <QNetworkAccessManager>

class UserNetworkAccessManager :
    public QNetworkAccessManager {
    Q_OBJECT

  public:
    UserNetworkAccessManager( QObject *parent = 0 );
    ~UserNetworkAccessManager();

    bool isAllowed( const QString &domain );

    Q_PROPERTY( bool isWhiteList READ isWhiteList WRITE setIsWhiteList );

    static UserNetworkAccessManager* instance();

  public slots:
    bool isWhiteList() {
        return _isWhiteList;
    }

    void setIsWhiteList(bool enable) {
        _isWhiteList = enable;
    }

    void addDomain( const QString &domain );

    void removeDomain( const QString &domain );

    void removeAllDomains();

  protected:
    QNetworkReply *createRequest( Operation op,
                                  const QNetworkRequest &req,
                                  QIODevice *outgoingData );

  private:
    bool isMatchingHostname(const QString &cn, const QString &hostname);

  private slots:
    void handleSslErrors(QNetworkReply* reply, const QList<QSslError> &errors);

  private:
    static UserNetworkAccessManager* _instance;
    bool _isWhiteList;
    QMap<QString, int> _domainList;
};

