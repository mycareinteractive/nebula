#include <QKeyEvent>
#include <QMessageBox>
#include "AddressEdit.h"


AddressEdit::AddressEdit(QWidget *parent)
    : QLineEdit(parent) {
    _selectOnMousePress = false;
}


void AddressEdit::focusInEvent(QFocusEvent *e) {
    QLineEdit::focusInEvent(e);
    selectAll();
    _selectOnMousePress = true;
}

void AddressEdit::mousePressEvent(QMouseEvent *me) {
    QLineEdit::mousePressEvent(me);
    if(_selectOnMousePress) {
        selectAll();
        _selectOnMousePress = false;
    }
}
