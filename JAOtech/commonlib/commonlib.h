#ifndef COMMONLIB_H
#define COMMONLIB_H

#include "Json.h"

#define NBIPC_NAME					"nebula"

// Inter-Process-Communication messages
#define NBIPC_TYPE					"type"
#define NBIPC_CATEGORY				"category"
#define NBIPC_ACTION				"action"
#define NBIPC_PAYLOAD				"payload"

// Message types
#define NBIPC_TYPE_COMMAND			"command"
#define NBIPC_TYPE_NOTIFICATION		"notification"

// Message categories
#define NBIPC_CATEGORY_BROWSER		"browser"

// Message actions
#define NBIPC_ACTION_EXIT			"exit"		// Nebula -> Browser, exit the browser
#define NBIPC_ACTION_OPENING		"opening"	// Browser -> Nebula, navigating to a new page
#define NBIPC_ACTION_OPENED			"opened"	// Browser -> Nebula, a new page opened
#define NBIPC_ACTION_FAILED			"failed"	// Browser -> Nebula, page failed to open


#endif // COMMONLIB_H
