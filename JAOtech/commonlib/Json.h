#pragma once

#include <QMap>
#include <QVariant>
#include <QString>
#include <QScriptValue>

class Json {
  public:
    Json(void);
    ~Json(void);

    static QString encode(const QMap<QString,QVariant> &map);
    static QMap<QString, QVariant> decode(const QString &jsonStr);

  private:
    static QScriptValue encodeInner(const QMap<QString,QVariant> &map, QScriptEngine* engine);
    static QMap<QString, QVariant> decodeInner(QScriptValue object);
    static QList<QVariant> decodeInnerToList(QScriptValue arrayValue);
};
